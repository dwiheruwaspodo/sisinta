<?php
$form = array(
    'nilai_bimb1' => array(
        'name'=>'nilai_bimb1',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('nilai_bimb1', isset($form_value['nilai_bimb1']) ? $form_value['nilai_bimb1'] : '')
    ),
	'nilai_bimb2' => array(
		'name' =>'nilai_bimb2',
		'size' => '30',
		'class'=>'form_field',
		'value' =>set_value('nilai_bimb2',isset($form_value['nilai_bimb2']) ? $form_value['nilai_bimb2'] : '')
	),
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
<p>
    <?php echo form_label('Nilai Pembimbing 1', 'nilai_bimb1'); ?>
    <?php echo form_input($form['nilai_bimb1']); ?>
</p>
<?php echo form_error('nilai_bimb1', '<p class="field_error">', '</p>');?>

<p>
    <?php echo form_label('Nilai Pembimbing 2', 'nilai_bimb2'); ?>
    <?php echo form_input($form['nilai_bimb2']); ?>
</p>
<?php echo form_error('nilai_bimb2', '<p class="field_error">', '</p>');?>


<p>
    <?php echo form_submit($form['submit']); ?>
    <?php echo anchor('adm_nilaiseminar','Batal', array('class' => 'cancel')) ?>
</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_nilaiseminar.php */
/* Location: ./application/views/adm_nilaiseminar/adm_nilaiseminar_form.php */
?>