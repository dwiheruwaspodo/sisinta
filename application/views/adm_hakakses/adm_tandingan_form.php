<script src="<?php echo base_url();?>asset/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>asset/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url();?>asset/js/jquery-ui.css" type="text/css" media="all"/>
<?php
$form = array(
    
	'users_name' => array(
			'name' => 'users_name',
			'rows' => '3',
			'cols' => '50',
			'class' => 'form_field',
			'value' => set_value('users_name',isset($form_value['users_name']) ? $form_value['users_name']:'')
	),
	'user_nama' => array(
			'name' => 'user_nama',
			'size' => '30',
			'class' => 'form_field',
			'value' => set_value('user_nama',isset($form_value['user_nama']) ? $form_value['user_nama'] : '')
	),
	'submit'   	=> array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
		<?php echo form_label('No. Pegawai','users_name');?>
		<?php echo form_input($form['users_name']);?>
	<p>
	<?php form_error('users_name','<p class="field_error">','</p>');?>
	
	<p>
		<?php echo form_label('Nama Admin','user_nama'); ?>
		<?php echo form_input($form['user_nama']);?>
	</p><?php form_error('user_nama','<p class="field_error">','</p>');?>
	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('adm_admintandingan','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>