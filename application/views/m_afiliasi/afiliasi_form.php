<?php
$form = array(
    'mAfiliasi_nama'    => array(
                        'name'=>'mAfiliasi_nama',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('mAfiliasi_nama', isset($form_value['mAfiliasi_nama']) ? $form_value['mAfiliasi_nama'] : '')
                  ),
	'afiliasi_alamat' => array(
				  		'name' => 'afiliasi_alamat',
						'size' => '50',
						'class' => 'form_field',
						'value' => set_value('afiliasi_alamat', isset($form_value['afiliasi_alamat'])? $form_value['afiliasi_alamat'] : '')
				  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Nama Instansi', 'mAfiliasi_nama'); ?>
        <?php echo form_input($form['mAfiliasi_nama']); ?>
	</p>
	<p><?php echo form_error('mAfiliasi_nama', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Alamat', 'afiliasi_alamat'); ?>
        <?php echo form_input($form['afiliasi_alamat']); ?>
	</p>
	<p><?php echo form_error('prodi', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_afiliasi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file afiliasi_form.php */
/* Location: ./application/views/prodi/afiliasi_form.php */
?>