<!DOCTYPE>
<html>

<head>
	<h2><?php echo $breadcrumb; ?></h2>
</head>
<body>
	<!-- pesan flash message start -->
	<?php $flash_pesan = $this->session->flashdata('pesan')?>
	<?php if (! empty($flash_pesan)) : ?>
		<div class="pesan">
			<?php echo $flash_pesan; ?>
		</div>
	<?php endif ?>
	<!-- pesan flash message end -->
	
	<!-- tabel data start -->
	<?php if (! empty($pesan)) : ?>
		<div class="pesan">
			<?php echo $pesan; ?>
		</div>
	<?php endif ?>
	<!-- tabel data end -->
	
	<div id="bottom_link">
	<?php echo anchor('koor_download','Berkas Masuk',array('class'=> 'add'));?>
	<?php echo anchor('koor_download/yg_sudahdidonlod','Sudah di download',array('class'=> 'add'));?>
	<?php echo anchor('koor_download/yg_sudahvalid/','Berkas Valid',array('class'=> 'add'));?>
	</div>
	
	<!-- tabel data start -->
	<?php if (! empty($tabel_data)) : ?>
		<?php echo $tabel_data; ?>
	<?php endif ?>
	<!-- tabel data end -->
	

</body>
</html>