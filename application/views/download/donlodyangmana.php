<!DOCTYPE>
<html>

<head>
	<h2><?php echo $breadcrumb; ?></h2>
</head>
<body>
	<!-- pesan flash message start -->
	<?php $flash_pesan = $this->session->flashdata('pesan')?>
	<?php if (! empty($flash_pesan)) : ?>
		<div class="pesan">
			<?php echo $flash_pesan; ?>
		</div>
	<?php endif ?>
	<!-- pesan flash message end -->
	
	<!-- tabel data start -->
	<?php if (! empty($pesan)) : ?>
		<div class="pesan">
			<?php echo $pesan; ?>
		</div>
	<?php endif ?>
	<!-- tabel data end -->
	
	<ul id="menu_tab">	
		<li><?php echo anchor('koor_download', 'Syarat Seminar');?></li>
		<li><?php echo anchor('koor_download_sidang', 'Syarat Sidang');?></li>
	</ul>
		

	<!-- tabel data start -->
	<?php if (! empty($tabel_data)) : ?>
		<?php echo $tabel_data; ?>
	<?php endif ?>
	<!-- tabel data end -->
	

</body>
</html>