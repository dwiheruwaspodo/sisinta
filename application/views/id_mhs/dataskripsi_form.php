<script src="<?php echo base_url();?>asset/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>asset/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url();?>asset/js/jquery-ui.css" type="text/css" media="all"/>
<?php
$form = array(
    
	'judul'	=> array(
			'name' => 'judul',
			'rows'=>'3',
			'cols' =>'50',
			'class' => 'form_field',
			'value' => set_value('judul',isset($form_value['skrip_judul']) ? $form_value['skrip_judul']:'')
	),
	'abstrak' => array(
			'name' => 'abstrak',
			'rows' => '10',
			'cols' => '120',
			'class' => 'form_field',
			'value' => set_value('abstrak',isset($form_value['skrip_abstrak']) ? $form_value['skrip_abstrak'] : '')
	),
	'submit'   	=> array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
		<?php echo form_label('Judul Skripsi','judul'); ?>
		<?php echo form_textarea($form['judul']);?>
	</p><?php form_error('judul','<p class="field_error">','</p>');?>
	
	<p class = "fg-white">
        <?php echo form_label('Pembimbing 1', 'idmDosen_Bi1'); ?>
        <?php echo form_dropdown('idmDosen_Bi1', $opsi_p1, set_value('idmDosen_Bi1',isset($form_value['idmDosen_Bi1']) ? $form_value['idmDosen_Bi1'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi1', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Pembimbing 2', 'idmDosen_Bi2'); ?>
        <?php echo form_dropdown('idmDosen_Bi2', $opsi_p2, set_value('idmDosen_Bi2',isset($form_value['idmDosen_Bi2']) ? $form_value['idmDosen_Bi2'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi1', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p>
		<?php echo form_label('Abstrak Skripsi','abstrak'); ?>
		<?php echo form_textarea($form['abstrak']);?>
	</p><?php form_error('abstrak','<p class="field_error">','</p>');?>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('mhs_id','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>