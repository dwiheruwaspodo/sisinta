<?php
$form = array(
    'semsid_tgl' => array(
						'name'	=> 'semsid_tgl',
						'size' 	=> '30',
						'class' => 'form_field',
						'value' => set_value('semsid_tgl',isset($form_value['semsid_tgl'])? $form_value['semsid_tgl']:''),
						'onclick' =>"displayDatePicker('semsid_tgl')"
				  ),
	'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  ),
	);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>

	<p>
        <?php echo form_label('Mahasiswa', 'idmSkripsi'); ?>
        <?php echo form_dropdown('idmSkripsi', $opsi_mhs, set_value('idmSkripsi',isset($form_value['idmSkripsi']) ? $form_value['idmSkripsi'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSkripsi', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
    	<?php echo form_label('Tanggal','semsid_tgl');?>
        <?php echo form_input($form['semsid_tgl']);?>
        <a href="javascript:void(0);" onclick="displayDatePicker('semsid_tgl');"><img src="<?php echo base_url('asset/images/icon_calendar.png'); ?>" alt="calendar" border="0"></a>
    </p>
    <p><?php echo form_error('semsid_tgl','<p class="field_error">', '</p>');?></p>

	<p>
		<?php echo form_label('Penguji 1', 'idmdosen_uji');?>
		<?php echo form_dropdown('idmdosen_uji', $opsi_dos, set_value('idmdosen_uji',isset($form_value['idmdosen_uji'])? $form_value['idmdosen_uji'] : ''));?>
	</p>
	<p><?php echo form_error('idmdosen_uji', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p>
		<?php echo form_label('Penguji 2', 'idmdosen_uji2');?>
		<?php echo form_dropdown('idmdosen_uji2', $opsi_dos2, set_value('idmdosen_uji2',isset($form_value['idmdosen_uji2'])? $form_value['idmdosen_uji2'] : ''));?>
		<br/>
		<a style="font-size:12px; color:red; margin-left:150px;">*Bila tidak ada dosen penguji ke 2, pilih Tidak/Belum ditentukan!</a>
	</p>
	<p>
	<?php echo form_error('idmdosen_uji2', '<p class="field_error fg-red">', '</p>');?>
	</p>
	
	<p>
        <?php echo form_label('Nama Ruangan', 'idmRuang'); ?>
        <?php echo form_dropdown('idmRuang', $opsi_ruang, set_value('idmRuang',isset($form_value['idmRuang']) ? $form_value['idmRuang'] : '')); ?>
	</p>
	<p><?php echo form_error('idmRuang', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
		<?php echo form_label('Jam Mulai','idmSesi');?>
		<?php echo form_dropdown('idmSesi',$jam, set_value('idmSesi',isset($form_value['idmSesi']) ? $form_value['idmSesi'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSesi', '<p class="field_error fg-red">', '</p>');?></p>
	
   <p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('koor_penjadwalansidang','Batal',array('class' => 'cancel')); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file jadwal_form.php */
/* Location: ./application/views/adm_penjadwalanseminar/jadwal_form.php */
?>