<!DOCTYPE html>
<html>

<head><h2><?php echo $breadcrumb; ?></h2></head>
<body>
	
	<p>
		Syarat Seminar : </br>
		1. Fotocopy KRS</br>
		2. Fotocopy Abstrak</br>
		3. Fotocopy Kartu Hadir Seminar</br>
		4. Lembar Persetujuan</br>
		5. Lembar Kesediaan Hadir Uji</br>
		6. Berkas Skripsi 3 eks</br>
		7. Fotocopy KHS Semua Semester</br>
		8. Undangan Seminar Proposal</br>
		9. Berita Acara Seminar Proposal</br>
		<div>
			<b>Dijadikan satu dalam bentuk RAR atau ZIP!</b>
		</div>
		<br/>
		<div>Ukuran file maksimal 2 Mb</div>
	</p>
	
	
	<!-- pesan flash message start -->
	<?php $flash_pesan = $this->session->flashdata('pesan')?>
	<?php if (! empty($flash_pesan)) : ?>
		<div class="pesan">
			<?php echo $flash_pesan; ?>
		</div>
	<?php endif ?>
	<!-- pesan flash message end -->

	<!-- pesan start -->
	<?php if (! empty($pesan)) : ?>
		<div class="pesan">
			<?php echo $pesan; ?>
		</div>
	<?php endif ?>
	<!-- pesan end -->

	
	<!-- tabel data start -->
	<?php if (! empty($tabel_data)) : ?>
		<?php echo $tabel_data; ?>
		<?php else:?>
		<?php 
		
		echo form_open_multipart($scriptaksi);
		echo form_label('Pilih file untuk diupload : ');
		echo form_upload('userfile');
		echo br();
		echo form_submit('btnSimpan','Upload');
		echo form_close();
	
	?>
	<?php endif ?>
	<!-- tabel data end -->
	
	
	
</body>
</html>