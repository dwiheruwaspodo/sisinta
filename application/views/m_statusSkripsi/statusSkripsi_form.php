<?php
$form = array(
    'idmStatusSkrip' => array(
        'name'=>'idmStatusSkrip',
        'size'=>'5',
        'class'=>'form_field',
        'value'=>set_value('idmStatusSkrip', isset($form_value['idmStatusSkrip']) ? $form_value['idmStatusSkrip'] : '')
    ),
    'StatusSkrip_tipe'    => array(
        'name'=>'StatusSkrip_tipe',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('StatusSkrip_tipe', isset($form_value['StatusSkrip_tipe']) ? $form_value['StatusSkrip_tipe'] : '')
    ),    
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class="fg-white">
        <?php echo form_label('ID', 'idmStatusSkrip'); ?>
        <?php echo form_input($form['idmStatusSkrip']); ?>
	</p>
	<p><?php echo form_error('idmStatusSkrip', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class="fg-white">
        <?php echo form_label('Status Skripsi', 'StatusSkrip_tipe'); ?>
        <?php echo form_input($form['StatusSkrip_tipe']); ?>
	</p>
	<?php echo form_error('StatusSkrip_tipe', '<p class="field_error fg-red">', '</p>');?>	

	<p>
		<?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_statusskripsi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form start -->

<?php
/* End of file siswa_form.php */
/* Location: ./application/views/kelas/siswa_form.php */
?>