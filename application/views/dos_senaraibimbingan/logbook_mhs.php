<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<?php
	foreach($mahasiswa as $row):
?>
<?php
	$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->logbook_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->logbook_tgl));
            $hr_tgl = "$hari, $tgl";
?>
<div id="chatkiri"></div>
	
<div id="chat_mhs">
	<div style="float:left; font-size:14px; color:#004d40;">
		<?php echo $row->mhsw_nama;?>
	</div>
	<br/>
	<div id="tanggal">
		<?php echo $hr_tgl;?>
	</div>
	<div id="tajud">
		<div id="konsultasi">
			<?php echo $row->logbook_mhs;?>
		</div>
	</div>
		<br>
	<div id="status">
	<?php	
		if($row->log_verif == '1')
		{
			echo "Sudah terverifikasi";
		}
		else
		{
			echo "Belum Terverifikasi";
		}
	?>
	</div>
</div>
<div style="clear:both"></div>

<div id="chatkanan"></div>
<div id="chat_dos">
	<div id="status">
		<?php echo $row->dos_nama;?>
	</div>
	<br/>
	<div id="tanggaldos">
		<?php echo $hr_tgl;?>
	</div>
	<div id="tajudd">
		<div id="konsultasi">
			<?php echo $row->komen_dos;?>
		</div>
	</div>
</div>
<div style="clear:both"></div>
<?php
	endforeach;
?>

<?php
/* End of file adm_afiliasi.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi.php */
?>