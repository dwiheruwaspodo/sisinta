<!DOCTYPE>
<body>
<?php echo form_open($form_action); ?>

<div id="katalog">
	<?php echo anchor('doskoor_senaraibimbingan/p1','Lihat Semua');?>
</div>

Kata Kunci Pencarian : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan nama" />
<button type="submit">Cari</button>

<?php echo form_close(); ?>

<table id="zebra">
<?php
	$banyak = count($nama_nama->result_array());
	echo '<tr><td>Ditemukan <b>'.$banyak.'</b> hasil pencarian dengan kata <b>"'.$nama.'"</b></td></tr>';
	echo '<tr> <th>NIM</th><th>Nama</th><th>Prodi</th><th>Judul</th><th>Tahapan</th><th>Status Skripsi</th></tr>';
	
	if(count($nama_nama->result_array())>0)
	{
		foreach($nama_nama->result_array() as $nd)
		{
			echo '<tr class="zebra"><td>'.$nd['nim'].'</td>';
			echo '<td>'.$nd['nama'].'</td>';
			echo '<td>'.$nd['prodi'].'</td>';
			echo '<td>'.$nd['skrip_judul'].'</td>';
			echo '<td>'.$nd['sproses'].'</td>';
			echo '<td>'.$nd['sskripsi'].'</td>';
			echo '<td>'.anchor('doskoor_senaraibimbingan/lihat_catatan_anak/'.$nd['id'],'Logbook',array('class'=> 'buku')).'</td>';
			
		}
	}
	else
	{
		echo '<tr><td>Tidak ditemukan mahasiswa <b>"'.$nama.'"</b> sebagai mahasiswa bimbingan 1</td></tr>';
	}
?>

</table>
<div id="pagination"><?php echo $paginator; ?>
</div>
</body>