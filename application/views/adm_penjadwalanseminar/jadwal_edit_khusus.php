<?php
$form = array(
    'semsid_tgl' => array(
						'name'	=> 'semsid_tgl',
						'size' 	=> '30',
						'class' => 'form_field',
						'value' => set_value('semsid_tgl',isset($form_value['semsid_tgl'])? $form_value['semsid_tgl']:''),
						'onclick' =>"displayDatePicker('semsid_tgl')"
				  ),
	'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  ),
	);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
    	<?php echo form_label('Tanggal','semsid_tgl');?>
        <?php echo form_input($form['semsid_tgl']);?>
        <a href="javascript:void(0);" onclick="displayDatePicker('semsid_tgl');"><img src="<?php echo base_url('asset/images/icon_calendar.png'); ?>" alt="calendar" border="0"></a>
    </p>
    <p><?php echo form_error('semsid_tgl','<p class="field_error">', '</p>');?></p>

	<p>
        <?php echo form_label('Nama Ruangan', 'idmRuang'); ?>
        <?php echo form_dropdown('idmRuang', $opsi_ruang, set_value('idmRuang',isset($form_value['idmRuang']) ? $form_value['idmRuang'] : '')); ?>
	</p>
	<p><?php echo form_error('idmRuang', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
		<?php echo form_label('Jam Mulai','idmSesi');?>
		<?php echo form_dropdown('idmSesi',$jam, set_value('idmSesi',isset($form_value['idmSesi']) ? $form_value['idmSesi'] : '')); ?>
	</p>
	
   <p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('adm_penjadwalanseminar','Batal'); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file jadwal_form.php */
/* Location: ./application/views/adm_penjadwalanseminar/jadwal_form.php */
?>