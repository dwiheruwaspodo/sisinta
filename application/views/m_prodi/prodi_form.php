<?php
$form = array(
    'idmProdi' => array(
                        'name'=>'idmProdi',
                        'size'=>'5',
                        'class'=>'form_field',
                        'value'=>set_value('idmProdi', isset($form_value['idmProdi']) ? $form_value['idmProdi'] : '')
                  ),
    'prodi_nama'    => array(
                        'name'=>'prodi_nama',
                        'size'=>'35',
                        'class'=>'form_field',
                        'value'=>set_value('prodi_nama', isset($form_value['prodi_nama']) ? $form_value['prodi_nama'] : '')
                  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Kode Prodi', 'idmProdi'); ?>
        <?php echo form_input($form['idmProdi']); ?>
	</p>
	<p><?php echo form_error('idmProdi', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Prodi', 'prodi_nama'); ?>
        <?php echo form_input($form['prodi_nama']); ?>
	</p>
	<p><?php echo form_error('prodi', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_prodi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>