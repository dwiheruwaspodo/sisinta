<!DOCTYPE HTML>
<head>
<title>SIS : Sistem Informasi Skripsi</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href="<?php echo base_url();?>asset/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>asset/css/polesan.css" rel="stylesheet" type="text/css"/> 


</head>
<body>
	<div id="warna_atas"></div>
	
	<div id="judul">
		<h1 align="center">Sistem Informasi 
		Skripsi dan Tugas Akhir</h1>
	</div>
	
	<div id="ttg-wrapper">
		<div id="about">
			<p align="center"><b>Sistem Informasi ini dibuat oleh :</b></p>
			
			<div id="kiri">
				<img  width="150px" height="120px" src="<?php echo base_url();?>asset/images/bu.jpg"/>
				<br/>
				<b>Triyanna Widiyaningtyas, M.T.</b><br/>
				Dosen aktif di Jurusan Teknik Elektro Universitas Negeri Malang. Berperan sebagai dosen pembimbing 1.
			</div>
				
			<div id="kiri">
				<img width="100px" height="120px" src="<?php echo base_url();?>asset/images/budi.jpg"/>
				<br/><b>Dwi Heru Budi Waspodo</b><br/>
				Mahasiswa Jurusan Teknik Elektro ,Prodi Pendidikan Teknik Informatika. Angkatan 2011. Berperan sebagai mahasiswa bimbingan.
			</div>
			<div id="kiri">
				<img width="100px" height="120px" src="<?php echo base_url();?>asset/images/pak.jpg"/>
				<br/>
				<b>Utomo Pujianto, S.Kom, M.Kom</b><br/>
				Dosen aktif di Jurusan Teknik Elektro Universitas Negeri Malang. Berperan sebagai dosen pembimbing 2.
			</div>
			<div class="clear"></div>
		</div>
		<div id="katalog">
			<?php echo anchor('login','Kembali'); ?>
		</div>
		<div id="ngisor">
			<img src="<?php echo base_url();?>asset/images/logo.png"/>
		</div>
	</div>
	
</body>
</html>

