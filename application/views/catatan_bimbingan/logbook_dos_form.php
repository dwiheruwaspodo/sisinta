<?php
$form = array(
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Kirim'
					),
	'komen_dos' => array(
						'name' => 'komen_dos',
						'size' => '50',
						'class' => 'form_field',
						'value' => set_value('komen_dos',isset($form_value['komen_dos'])? $form_value['komen_dos'] : '')
					),
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<?php
	$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($form_value['logbook_tgl']));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($form_value['logbook_tgl']));
            $hr_tgl = "$hari, $tgl";
?>

<!-- form start -->
<?php echo form_open($form_action); ?>
	<div id="chatkiri"></div>
		<div id="chat_mhs">
			<div id="tanggal">
				<?php echo $hr_tgl;?>
			</div>
			<div id="tajud">
				<div id="konsultasi">
					<?php echo $form_value['logbook_mhs'];?>
				</div>
			</div>
		</div>
	<div style="clear:both"></div>

	<div id="chatkanan"></div>
		<div id="chat_dos">
			<div id="tanggaldos">
				<?php echo form_label('Masukan untuk mahasiswa : ', 'komen_dos'); ?>
			</div>
			<div id="tajudd">
				<div id="konsultasi">
					<?php echo form_textarea($form['komen_dos']); ?>
					<?php echo form_error('komen_dos');?>
				</div>
			</div>
			
			<div id="status">
				<?php echo form_label('Verifikasi Status : ','log_verif');?>
				<select name="log_verif">
					<option value = "1">Terverifikasi
					<option value = "0">Tidak terverifikasi
				</select>
			</div>
			<br>
			<br>
			<br>
			<div id="status">
				<?php echo form_submit($form['submit']); ?>
				<?php echo anchor('catatan_bimb_dos','Batal',array('class'=>'cancel')); ?>	
			</div>
		</div>
	<div style="clear:both"></div>
	

<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_afiliasi_form.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi_form.php */
?>
