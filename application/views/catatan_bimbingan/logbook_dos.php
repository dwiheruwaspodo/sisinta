<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->


<div id="bottom_link">
	<?php echo anchor('catatan_bimb_dos/ins1/','Konsultasi sebagai Pembimbing 1',array('class'=> 'add'));?>
	<?php echo anchor('catatan_bimb_dos/ins2/','Konsultasi sebagai Pembimbing 2',array('class'=> 'add'));?>
</div>

<!-- tabel data start -->
<?php if (! empty($tabel_data)) : ?>
    <?php echo $tabel_data; ?>
<?php endif ?>
<!-- tabel data end -->

<!-- pagination start -->
<?php if (! empty($pagination)) : ?>
    <div id="pagination" class="fg-white">
        <?php echo $pagination; ?>
    </div>
<?php endif ?>
<!-- paginatin end -->


<?php
/* End of file adm_afiliasi.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi.php */
?>