<?php
$form = array(
	'logbook_mhs' => array(
						'name' =>'logbook_mhs',
						'rows'=>'3',
						'cols' =>'50',
						'class' => 'form_textarea',
						'value'=> set_value('logbook_mhs', isset($form_value['logbook_mhs']) ? $form_value['logbook_mhs'] : '')
					),
	'logbook_tgl' => array(
						'name'=> 'logbook_tgl',
						'size'=> '30',
						'class'=>'form_field',
						'value'=> set_value('logbook_tgl', isset($form_value['logbook_tgl'])? $form_value['logbook_tgl']:''),
						'onclick' =>"displayDatePicker('srt_tgl')"
					),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
        <?php echo form_label('Konsultasi', 'logbook_mhs'); ?>
		<?php echo form_textarea($form['logbook_mhs']); ?>
	</p>
	<p><?php echo form_error('logbook_mhs', '<p class="field_error fg-red">', '</p>');?></p>
    
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('catatan_bimb_mhs','Batal',array('class'=>'cancel')); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file logbook_mhs_form.php */
/* Location: ./application/views/adm_afiliasi/logbook_mhs_form.php */
?>