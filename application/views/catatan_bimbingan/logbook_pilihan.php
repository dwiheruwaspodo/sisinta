<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->
<div  id="bottom_link">
	<?php 
		echo anchor('catatan_bimb_mhs/dari_p1','Lihat Catatan dari Pembimbing 1',array('class'=> 'add')); 
		echo " ";
		echo anchor('catatan_bimb_mhs/dari_p2','Lihat Catatan dari Pembimbing 2',array('class'=> 'add'));
	?>
</div>
<br/>

<?php
/* End of file adm_afiliasi.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi.php */
?>