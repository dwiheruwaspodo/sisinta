<?php
$form = array(
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
					),
	'komen_dos' => array(
						'name' => 'komen_dos',
						'size' => '50',
						'class' => 'form_field',
						'value' => set_value('komen_dos',isset($form_value['komen_dos'])? $form_value['komen_dos'] : '')
					),
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	
	<p class = "fg-white">
        <?php echo form_label('Mahasiswa', 'idmmhsw'); ?>
        <?php echo form_dropdown('idmmhsw', $mhs_bimbingan, set_value('idmmhsw',isset($form_value['idmmhsw']) ? $form_value['idmmhsw'] : '')); ?>
	</p>
	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('catatan_bimb_dos','Batal',array('class'=>'cancel')); ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file adm_afiliasi_form.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi_form.php */
?>