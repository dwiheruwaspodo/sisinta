<!DOCTYPE>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/reset.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/style.css');?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/xxx.css');?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('asset/css/calendar.css');?>" />
    <script type="text/javascript" src="<?php echo base_url('asset/js/calendar.js'); ?>"></script>

    <title><?php echo isset($breadcrumb) ? $breadcrumb : ''; ?></title>
</head>
          
<body>
	<div id="content-wrapper">
            <div id="header">
                <div id="headerkiri">
				<img src="<?php echo base_url();?>asset/images/um.png"/>
                    <font size=4%>SiSinta : Sistem Informasi Skripsi dan Tugas Akhir</font>
                </div>
                
                <div id="headerkanan">
                    <font size=4%><?php echo $this->session->userdata('users_name');?> | </font>
					<font size=4%><?php echo $this->session->userdata('user_nama');?> | </font>
					<font size=4%><?php echo anchor('logout', 'LogOut', array('onclick' => "return confirm('Anda yakin akan logout?')"));?></font>
				</div>
				
            </div>
		    <div id="content-left">
                <div id="content-menu">
                    <?php $this->load->view('navdoskaj'); ?>
					<?php $this->load->view('navkaj'); ?>

                </div>
            </div>
            <div id="content-right">
                <div id="content-main">
                    <?php $this->load->view($main_view); ?>
                </div>
                <div id="footer">
                    <?php $this->load->view('footer'); ?>
                </div>
            </div>
         	<div id="headerkecil">
                <div id="headerkirikecil">
	                    <img src="<?php echo base_url();?>asset/images/nav.png"
                        onclick="if (this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display != '') 
                        { 
                            this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = ''; 
                            this.innerText = ''; 
                            this.value = ''; 
                        } 
                        else 
                        {
                            this.parentNode.parentNode.getElementsByTagName('div')[1].getElementsByTagName('div')[0].style.display = 'none'; 
                            this.innerText = ''; 
                            this.value = ''; 
                        }" />
                        <div> 
                            <div style="display: none; max-width:767px;">
							
								<div id="headermenukiri">
									<?php $this->load->view('navdoskaj'); ?>
								</div>
								
                                <div id="headermenukanan">
									<?php $this->load->view('navkaj'); ?>
								</div>
							</div> 
                        </div>
                    
                </div>
                <div id="headerkanankecil">
                    <font size=4%><?php echo $this->session->userdata('user_nama');?></font>
                </div>
                
                
		    </div>    
    </div>
</body>
</html>

