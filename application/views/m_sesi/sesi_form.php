<?php
$form = array(
    'sesi_mulai' => array(
        'name'=>'sesi_mulai',
        'size'=>'10',
        'class'=>'form_field',
        'value'=>set_value('sesi_mulai', isset($form_value['sesi_mulai']) ? $form_value['sesi_mulai'] : '')
    ),
    'sesi_selesai'    => array(
        'name'=>'sesi_selesai',
        'size'=>'10',
        'class'=>'form_field',
        'value'=>set_value('sesi_selesai', isset($form_value['sesi_selesai']) ? $form_value['sesi_selesai'] : '')
    ),    
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class="fg-white">
        <?php echo form_label('Jam Mulai', 'sesi_mulai'); ?>
        <?php echo form_input($form['sesi_mulai']); ?>
	</p>
	<p><?php echo form_error('sesi_mulai', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class="fg-white">
        <?php echo form_label('Jam Berakhir', 'sesi_selesai'); ?>
        <?php echo form_input($form['sesi_selesai']); ?>
	</p>
	<?php echo form_error('sesi_selesai', '<p class="field_error fg-red">', '</p>');?>	

	<p>
		<?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_sesi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form start -->

<?php
/* End of file siswa_form.php */
/* Location: ./application/views/kelas/siswa_form.php */
?>