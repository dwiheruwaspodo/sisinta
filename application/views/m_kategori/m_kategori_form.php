<?php
$form = array(
    'katg_nama'    => array(
                        'name'=>'katg_nama',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('katg_nama', isset($form_value['katg_nama']) ? $form_value['katg_nama'] : '')
                  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Kategori', 'katg_nama'); ?>
        <?php echo form_input($form['katg_nama']); ?>
	</p>
	<p><?php echo form_error('katg_nama', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_kategori','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>