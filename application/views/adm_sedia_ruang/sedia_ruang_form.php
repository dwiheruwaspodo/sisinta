<?php
$form = array(
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Hari', 'SediaRuang_hari'); ?>
		<select name="SediaRuang_hari">
        	<option value="senin">Senin
        	<option value="selasa">Selasa
        	<option value="rabu">Rabu
        	<option value="kamis">Kamis
        	<option value="jumat">Jumat
         </select>
	</p>
	<p><?php echo form_error('SediaRuang_hari', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Ruangan', 'idmRuang'); ?>
        <?php echo form_dropdown('idmRuang', $option_ruang, set_value('idmRuang',isset($form_value['idmRuang']) ? $form_value['idmRuang'] : '')); ?>
	</p>
	<p><?php echo form_error('idmRuang', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Jam Mulai', 'idmSesi_awal'); ?>
        <?php echo form_dropdown('idmSesi_awal', $option_mulai, set_value('idmSesi_awal',isset($form_value['idmSesi_awal']) ? $form_value['idmSesi_awal'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSesi_awal', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Jam Berakhir', 'idmSesi_akhir'); ?>
        <?php echo form_dropdown('idmSesi_akhir', $option_selesai, set_value('idmSesi_akhir',isset($form_value['idmSesi_akhir']) ? $form_value['idmSesi_akhir'] : '')); ?>
	</p>
	<p><?php echo form_error('idmSesi_akhir', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Status Ruangan', 'SediaRuang_stat'); ?>
        <select name="SediaRuang_stat">
        	<option value="tersedia">Tersedia
        	<option value="tidak_tersedia">Tidak Tersedia
         </select>
	</p>
    
	<p><?php echo form_error('SediaRuang_stat', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('adm_sediaruang','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>