<?php
$form = array(
    'users_passd'    => array(
                        'name'=>'users_passd',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('users_passd', isset($form_value['users_passd']) ? $form_value['users_passd'] : '')
                ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Password baru', 'users_passd'); ?>
        <?php echo form_password($form['users_passd']); ?>
	</p>
	<p><?php echo form_error('users_passd', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('login','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>