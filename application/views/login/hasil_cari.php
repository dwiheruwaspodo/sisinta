<!DOCTYPE HTML>
<head>
<title>SIS : Sistem Informasi Skripsi</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href="<?php echo base_url();?>asset/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>asset/css/polesan.css" rel="stylesheet" type="text/css"/> 

</head>
<body>
	<div id="warna_atas"></div>
	
	<div id="judul">
		<h1 align="center">Sistem Informasi 
		Skripsi dan Tugas Akhir</h1>
	</div>
	
	<div id="katalog-wrapper">
		<div id="isikatalog">
				<!-- pesan flash message start -->
				<?php $flash_pesan = $this->session->flashdata('pesan')?>
				<?php if (! empty($flash_pesan)) : ?>
					<div class="pesan">
						<?php echo $flash_pesan; ?>
					</div>
				<?php endif ?>
				<!-- pesan flash message end -->

				<!-- pesan start -->
				<?php if (! empty($pesan)) : ?>
					<div class="pesan">
						<?php echo $pesan; ?>
					</div>
				<?php endif ?>
				<!-- pesan end -->
				
				<form method="post" action="<?php echo base_url(); ?>index.php/semuabisalihat/cari_cari">
					<div id="login">
						<?php echo anchor('login','Login'); ?>
					</div>
					Kata Kunci: <input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan judul" />
					<input type="submit" value="Cari Judul" />
					
					<div id="katalog" style="margin-right:10px;">
						<?php echo anchor('semuabisalihat','Lihat Semua'); ?>
					</div>
				</form>
				
                <table id="zebra">
                <?php
                    echo 'Hasil pencarian dengan kata kunci <b>"'.$judul.'"</b>';
					echo '<tr> <th>Judul</th><th>Nim</th><th>Nama</th><th>Prodi</th></tr>';
					
					$att = array(
						'width' => '480',
						'height' => '480',
						'scroolbars' => 'no',
						'status' => 'yes',
						'resizable' => 'no',
						'screenx' => '500',
						'screeny' => '80',
						'class' => 'detail'
					);
					
                    if(count($katalog->result_array())>0)
                    {
                        foreach($katalog->result_array() as $nd)
                        {
                            echo '<tr class="zebra"><td>'.$nd['skrip_judul'].'</td>';
                            echo '<td>'.$nd['nim'].'</td>';
                            echo '<td>'.$nd['nama'].'</td>';
                            echo '<td>'.$nd['prodi'].'</td>';
							echo '<td>'.anchor_popup('semuabisalihat/detail/'.$nd['id'],'Detail',$att).'</td></tr>';
                        }
                    }
                    else
                    {
                        echo '<tr><td>Tidak ditemukan skripsi dengan kata kunci <b>"'.$judul.'"</b></td></tr>';
                    }
                ?>
                
                </table>

				<!-- pagination start -->
				<?php if (! empty($paginator)) : ?>
					<div id="pagination">
						<?php echo $paginator; ?>
					</div>
				<?php endif ?>
				<!-- paginatin end -->

			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>

