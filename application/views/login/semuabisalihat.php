<!DOCTYPE HTML>
<head>
<title>SIS : Sistem Informasi Skripsi</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href="<?php echo base_url();?>asset/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>asset/css/polesan.css" rel="stylesheet" type="text/css"/> 

</head>
<body>
	<div id="warna_atas"></div>
	
	<div id="judul">
		<h1 align="center">Sistem Informasi 
		Skripsi dan Tugas Akhir</h1>
	</div>
	
	<div id="katalog-wrapper">
		<div id="isikatalog">
				<?php echo form_open($form_action); ?>
				<div id="login">
					<?php echo anchor('login','Login'); ?>
				</div>
				Kata Kunci: <input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan judul" />
				<input type="submit" value="Cari Judul" />
				<?php echo form_close(); ?>
				
				<!-- pesan flash message start -->
				<?php $flash_pesan = $this->session->flashdata('pesan')?>
				<?php if (! empty($flash_pesan)) : ?>
					<div class="pesan">
						<?php echo $flash_pesan; ?>
					</div>
				<?php endif ?>
				<!-- pesan flash message end -->

				<!-- pesan start -->
				<?php if (! empty($pesan)) : ?>
					<div class="pesan">
						<?php echo $pesan; ?>
					</div>
				<?php endif ?>
				<!-- pesan end -->
				
				<!-- tabel data start -->
				<?php if (! empty($katalog)) : ?>
					<?php echo $katalog; ?>
				<?php endif ?>
				<!-- tabel data end -->

				<!-- pagination start -->
				<?php if (! empty($pagination)) : ?>
					<div id="pagination">
						<?php echo $pagination; ?>
					</div>
				<?php endif ?>
				<!-- paginatin end -->

			<div class="clear"></div>
		</div>
	</div>
	
</body>
</html>

