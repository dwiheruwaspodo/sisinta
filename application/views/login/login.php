<!DOCTYPE HTML>
<head>
<title>SIS : Sistem Informasi Skripsi</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

<link href="<?php echo base_url();?>asset/css/reset.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url();?>asset/css/polesan.css" rel="stylesheet" type="text/css"/> 

</head>
<body>
<?php
	$attributes = array('name' => 'login_form', 'id' => 'login_form');
	echo form_open('login', $attributes);
?> 
	<div id="warna_atas"></div>
	
	<div id="judul">
		<h1 align="center">Sistem Informasi 
		Skripsi dan Tugas Akhir</h1>
	</div>
	
	<div id="content-wrapper">
		<div id="content">
			<div id="form_login">
				<form>
					<h5>
						Masuk Akun Sisinta
					</h5>
					<div id="form_input">
						<!-- pesan start -->
						<?php if (! empty($pesan)) : ?>
						<p>
							<?php echo $pesan; ?>
						</p>
						<?php endif ?>
						<!-- pesan end -->
						<span>
							<input type="text" name="users_name" placeholder="Username" value="<?php echo set_value('users_name');?>">
							<small><?php echo form_error('users_name');?></small>
						</span>
						<span>
							<input type="password" name="users_passd" placeholder="Password" value="<?php echo set_value('users_passd');?>">
							<small><?php echo form_error('users_passd');?></small>
						</span>
						
						<button type="submit">Login</button>
					</div>
				</form>
				<div id="lupa">
					<?php echo anchor('login','Lupa Password?',array('onclick'=>"return confirm('Hubungi Administrasi!!')"));?>
				</div>
				
			</div>
			<div id="katalog" data-tip="Daftar katalog skripsi Jurusan Teknik Elektro Universitas Negeri Malang">
				<?php echo anchor('semuabisalihat','Katalog Skripsi'); ?>
			</div>
			<div class="clear"></div>
		</div>
		<div id="bawah">
			<img src="<?php echo base_url();?>asset/images/logo.png"/>
		</div>
		<div id="footer">
			<?php echo anchor('tentang','&copy; 2015 Tim Pengembang Sistem Informasi'); ?>
		</div>
	</div>
	
</body>
</html>

