<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<?php
	foreach($lihat as $row):
?>
<?php
	$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->tgl_reg));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->tgl_reg));
            $hr_tgl = "$hari, $tgl";
?>
<div>
	<table>
	<tr>
		<td colspan="2" align="center"><b><?php echo $row->skrip_judul;?></b></td>
	</tr>
	<tr></tr><tr></tr>
	<tr>
		<td><p>Tgl Registrasi </p></td>
		<td> : <?php echo $hr_tgl;?><br/></td>
	</tr>
	<tr>
		<td><p>Nama Mahasiswa </p></td>
		<td> : <?php echo $row->nama;?><br/></td>
	</tr>
	<tr>
		<td><p>NIM </p></td>
		<td> : <?php echo $row->nim;?><br/></td>
	</tr>
	<tr>
		<td><p>Prodi </p></td>
		<td> : <?php echo $row->prodi;?><br/></td>
	</tr>
	<tr>
		<td><p>Status Skripsi </p></td>
		<td> : <?php echo $row->sproses;?><br/></td>
	</tr>
	<tr>
		<td><p>Status Proses </p></td>
		<td> : <?php echo $row->sskripsi;?><br/></td>
	</tr>
	<tr>
		<td><p>Pembimbing 1 </p></td>
		<td> : <?php echo $row->pembimbing1;?><br/></td>
	</tr>
	<tr>
		<td><p>Pembimbing 2</p></td>
		<td> : <?php echo $row->pembimbing2;?><br/></td>
	</tr>
	<table>
	<div>
		<p>Abstrak Skripsi</p>
		<p><?php echo $row->abstrak;?></p>
	</div>
</div>
<?php
	endforeach;
?>

<?php
/* End of file adm_afiliasi.php */
/* Location: ./application/views/adm_afiliasi/adm_afiliasi.php */
?>