<?php
$form = array(
    'idmstatus_pros' => array(
        'name'=>'idmstatus_pros',
        'size'=>'5',
        'class'=>'form_field',
        'value'=>set_value('idmstatus_pros', isset($form_value['idmstatus_pros']) ? $form_value['idmstatus_pros'] : '')
    ),
    'statuspros_tipe'    => array(
        'name'=>'statuspros_tipe',
        'size'=>'30',
        'class'=>'form_field',
        'value'=>set_value('statuspros_tipe', isset($form_value['statuspros_tipe']) ? $form_value['statuspros_tipe'] : '')
    ),    
    'submit'   => array(
        'name'=>'submit',
        'id'=>'submit',
        'value'=>'Simpan'
    )
);
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class="fg-white">
        <?php echo form_label('ID', 'idmstatus_pros'); ?>
        <?php echo form_input($form['idmstatus_pros']); ?>
	</p>
	<p><?php echo form_error('idmstatus_pros', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class="fg-white">
        <?php echo form_label('Status Proses','statuspros_tipe'); ?>
        <?php echo form_input($form['statuspros_tipe']); ?>
	</p>
	<?php echo form_error('statuspros_tipe', '<p class="field_error fg-red">', '</p>');?>	

	<p>
		<?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_statusproses','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form start -->

<?php
/* End of file statusProses_form.php */
/* Location: ./application/views/status/statusProses_form.php */
?>