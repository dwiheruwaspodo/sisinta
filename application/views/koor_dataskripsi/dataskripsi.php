<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan flash message start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan flash message end -->

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<div>
	<?php echo anchor('koor_dataskripsi/barumasuk/','Skripsi Belum Terverifikasi',array('class'=> 'add'));?>
	<?php echo anchor('koor_dataskripsi/yangsudahvalid/','Skripsi Terverifikasi',array('class'=> 'add'));?>
</div>

<?php echo form_open($form_action); ?>
Cari Mahasiswa : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan nama" />
<button type="submit">Cari</button>


<!-- tabel data start -->
<?php if (! empty($tabel_data)) : ?>
    <?php echo $tabel_data; ?>
<?php endif ?>
<!-- tabel data end -->

<!-- pagination start -->
<?php if (! empty($pagination)) : ?>
    <div id="pagination" class="fg-white">
        <?php echo $pagination; ?>
    </div>
<?php endif ?>
<!-- paginatin end -->


<?php
/* End of file dataskripsi.php */
/* Location: ./application/views/koor_dataskripsi.php */
?>