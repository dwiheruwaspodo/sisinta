<script src="<?php echo base_url();?>asset/js/jquery-1.9.1.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>asset/js/jquery-ui.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url();?>asset/js/jquery-ui.css" type="text/css" media="all"/>
<?php
$form = array(
    
	'judul'	=> array(
			'name' => 'judul',
			'rows'=>'3',
			'cols' =>'50',
			'class' => 'form_field',
			'value' => set_value('judul',isset($form_value['judul']) ? $form_value['judul']:'')
	),
	'nim' => array(
			'name' => 'nim',
			'size' => '30',
			'class' => 'form_field',
			'value' => set_value('nim',isset($form_value['nim']) ? $form_value['nim'] : '')
	),
	'skrip_tgl_reg'	=> array(
						'name'	=>'skrip_tgl_reg',
						'size'	=>'30',
						'class'	=>'form_field',
						'value'	=>set_value('skrip_tgl_reg',isset($form_value['skrip_tgl_reg'])? $form_value['skrip_tgl_reg']:''),
						'onclick' =>"displayDatePicker('skrip_tgl_reg')"
	),
	
	'submit'   	=> array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php if (! empty($pesan)) : ?>
    <div class="pesan">
        <?php echo $pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p>
    	<?php echo form_label('Tanggal Registrasi','skrip_tgl_reg');?>
        <?php echo form_input($form['skrip_tgl_reg']);?>
        <a href="javascript:void(0);" onclick="displayDatePicker('skrip_tgl_reg');"><img src="<?php echo base_url('asset/images/icon_calendar.png'); ?>" alt="calendar" border="0"></a>
    </p>
	<?php echo form_error('skrip_tgl_reg', '<p class="field_error">', '</p>');?>
	
	<p class = "fg-white">
        <?php echo form_label('Nama Mahasiswa', 'idmMhsw'); ?>
        <?php echo form_dropdown('idmMhsw',$opsi_mhs,set_value('idmMhsw',isset($form_value['idmMhsw']) ? $form_value['idmMhsw'] : '')); ?>
	</p>
	<p><?php echo form_error('idmMhsw', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
		<?php echo form_label('NIM','nim');?>
		<?php echo form_input($form['nim']);?>
	<p>
	<?php form_error('nim','<p class="field_error">','</p>');?>
	
	<p>
		<?php echo form_label('Judul Skripsi','judul'); ?>
		<?php echo form_textarea($form['judul']);?>
	</p><?php form_error('judul','<p class="field_error">','</p>');?>
	
	<p class = "fg-white">
        <?php echo form_label('Pembimbing 1', 'idmDosen_Bi1'); ?>
        <?php echo form_dropdown('idmDosen_Bi1', $opsi_p1, set_value('idmDosen_Bi1',isset($form_value['idmDosen_Bi1']) ? $form_value['idmDosen_Bi1'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi1', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Pembimbing 2', 'idmDosen_Bi2'); ?>
        <?php echo form_dropdown('idmDosen_Bi2', $opsi_p2, set_value('idmDosen_Bi2',isset($form_value['idmDosen_Bi2']) ? $form_value['idmDosen_Bi2'] : '')); ?>
	</p>
	<p><?php echo form_error('idmDosen_Bi2', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Kategori 1', 'idmKategori1'); ?>
        <?php echo form_dropdown('idmKategori1', $opsi_k1, set_value('idmKategori1',isset($form_value['idmKategori1']) ? $form_value['idmKategori1'] : '')); ?>
	</p>
	<p><?php echo form_error('idmKategori1', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Kategori 2', 'idmKategori2'); ?>
        <?php echo form_dropdown('idmKategori2', $opsi_k2, set_value('idmKategori2',isset($form_value['idmKategori2']) ? $form_value['idmKategori2'] : '')); ?>
	</p>
	<p><?php echo form_error('idmKategori2', '<p class="field_error fg-red">', '</p>');?></p>

	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('koor_dataskripsi','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->

<?php
/* End of file prodi_form.php */
/* Location: ./application/views/prodi/prodi_form.php */
?>