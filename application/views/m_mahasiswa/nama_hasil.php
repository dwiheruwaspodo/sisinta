<!DOCTYPE>
<body>

<?php echo form_open($form_action); ?>
<div id="katalog">
	<?php echo anchor('master_mahasiswa','Lihat Semua');?>
</div>

Cari Mahasiswa : 
<br />
<input type="text" name="cari" size="50" autocomplete="off" placeholder="ketikkan nama" />
<button type="submit">Cari</button>

<table id="zebra">
<?php
	$banyak = count($identitas->result_array());
	echo '<tr><td>Ditemukan <b>'.$banyak.'</b> hasil pencarian dengan kata <b>"'.$nama.'"</b></td></tr>';
	echo '<tr> <th>Nama</th><th>NIM</th><th>Prodi</th><th>No.Tlp</th></tr>';
	
	if(count($identitas->result_array())>0)
	{
		foreach($identitas->result_array() as $nd)
		{
			echo '<tr class="zebra"><td>'.$nd['mhsw_nama'].'</td>';
			echo '<td>'.$nd['mhsw_nim'].'</td>';
			echo '<td>'.$nd['prodi_nama'].'</td>';
			echo '<td>'.$nd['mhsw_tlp'].'</td>';
			echo '<td>'.anchor('master_mahasiswa/edit/'.$nd['idmmhsw'],'Edit',array('class' => 'edit')).' '.
                anchor('master_mahasiswa/hapus/'.$nd['idmmhsw'],'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')")).'</td>';
		}
	}
	else
	{
		echo '<tr><td>Tidak ditemukan mahasiswa dengan kata kunci <b>"'.$nama.'"</b></td></tr>';
	}
?>

</table>
<div id="pagination"><?php echo $paginator; ?>
</div>
</body>