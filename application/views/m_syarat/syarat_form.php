<?php
$form = array(
    'doksyarat_nama'    => array(
                        'name'=>'doksyarat_nama',
                        'size'=>'30',
                        'class'=>'form_field',
                        'value'=>set_value('doksyarat_nama', isset($form_value['doksyarat_nama']) ? $form_value['doksyarat_nama'] : '')
                  ),
	'format_file_upload' => array(
				  		'name' => 'format_file_upload',
						'size' => '30',
						'class' => 'form_field',
						'value' => set_value('format_file_upload', isset($form_value['format_file_upload'])? $form_value['format_file_upload'] : '')
				  ),
	'keterangan' => array(
						'name' => 'keterangan',
						'size' => '30',
						'class'=> 'form_field',
						'value'=> set_value('keterangan', isset($form_value['keterangan']) ? $form_value['keterangan'] : '')
				  ),
	'kode_ss' => array(
						'name' => 'kode_ss',
						'size' => '30',
						'class' => 'form_field',
						'value' => set_value('kode_ss', isset($form_value['kode_ss'])? $form_value['kode_ss'] : '')
				  ),
    'submit'   => array(
                        'name'=>'submit',
                        'id'=>'submit',
						'value'=>'Simpan'
                  )
    );
?>

<h2 class="fg-white"><?php echo $breadcrumb ?></h2>

<!-- pesan start -->
<?php $flash_pesan = $this->session->flashdata('pesan')?>
<?php if (! empty($flash_pesan)) : ?>
    <div class="pesan fg-white">
        <?php echo $flash_pesan; ?>
    </div>
<?php endif ?>
<!-- pesan end -->

<!-- form start -->
<?php echo form_open($form_action); ?>
	<p class = "fg-white">
        <?php echo form_label('Nama Dokumen', 'doksyarat_nama'); ?>
        <?php echo form_input($form['doksyarat_nama']); ?>
	</p>
	<p><?php echo form_error('doksyarat_nama', '<p class="field_error fg-red">', '</p>');?></p>
	
	<p class = "fg-white">
        <?php echo form_label('Format File Upload', 'format_file_upload'); ?>
        <?php echo form_input($form['format_file_upload']); ?>
	</p>
	<p><?php echo form_error('format_file_upload', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Keterangan', 'keterangan'); ?>
        <?php echo form_input($form['keterangan']); ?>
	</p>
	<p><?php echo form_error('keterangan', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Cek Dosen', 'cekdosen'); ?>
		<select name="cekdosen">
        	<option value="">--Pilih--
        	<option value="1">Yes
            <option value="0">No
        </select>
	</p>
	<p><?php echo form_error('cekdosen', '<p class="field_error fg-red">', '</p>');?></p>

	<p class = "fg-white">
        <?php echo form_label('Kode Syarat', 'kode_ss'); ?>
        <?php echo form_input($form['kode_ss']); ?>
	</p>
	<p><?php echo form_error('kode_ss', '<p class="field_error fg-red">', '</p>');?></p>

	
	
	<p>
        <?php echo form_submit($form['submit']); ?>
        <?php echo anchor('master_syarat','Batal', array('class' => 'cancel')) ?>
	</p>
<?php echo form_close(); ?>
<!-- form end -->


<?php
/* End of file syarat.php */
/* Location: ./application/views/m_syarat/syarat.php */
?>