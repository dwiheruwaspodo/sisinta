<?php if (!defined('BASEPATH')) exit ('No direct scriptacces allowed');

class Model_admsediaruang extends CI_Model
{
	public $db_tabel		= 'sediaruanghari';
	public $per_halaman		= 10;
	public $offset			= 0;
	
	public function load_form_rules_tambah()
	{
		$form = array(
					array(
						'field' => 'SediaRuang_hari',
						'label' => 'Hari',
						'rules' => 'required'
					),
					array(
						'field' => 'idmRuang',
						'label' => 'Ruangan',
						'rules' => 'required'
					),
					array(
						'field' => 'idmSesi_awal',
						'label' => 'Jam Mulai',
						'rules' => 'required'
					),
					array(
						'field' => 'SediaRuang_stat',
						'label' => 'Status Ruangan',
						'rules' => 'required'
					),
					array(
						'field' => 'idmSesi_akhir',
						'label' => 'Jam Berakhir',
						'rules' => 'required'
					)
		);
		return $form;
	}
	
	public function load_form_rules_edit()
	{
		$form = array(
					array(
						'field' => 'SediaRuang_hari',
						'label' => 'Hari',
						'rules' => 'required'
					),
					array(
						'field' => 'idmRuang',
						'label' => 'Ruangan',
						'rules' => 'required'
					),
					array(
						'field' => 'idmSesi_awal',
						'label' => 'Jam Mulai',
						'rules' => 'required'
					),
					array(
						'field' => 'SediaRuang_stat',
						'label' => 'Status Ruangan',
						'rules' => 'required'
					),
					array(
						'field' => 'idmSesi_akhir',
						'label' => 'Jam Berakhir',
						'rules' => 'required'
					)
		);
		return $form;	
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$form = $this->load_form_rules_edit();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_semua($offset)
	{
		if(is_null($offset)|| empty ($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('mruang.ruang_nama,a.sesi_mulai,b.sesi_selesai,SediaRuang_stat, SediaRuang_hari, idSediaRuang')
						->from('sediaruanghari, msesi a, msesi b, mruang')
						->where('sediaruanghari.idmRuang = mruang.idmRuang')
						->where('sediaruanghari.idmSesi_awal = a.idmSesi')
						->where('sediaruanghari.idmSesi_akhir = b.idmSesi')
						->limit($this->per_halaman, $this->offset)
						->order_by('SediaRuang_hari','asc')
						->get()
						->result();
						
	}
	
	public function cari($idSediaRuang)
	{
		return $this->db->where('idSediaRuang', $idSediaRuang)
			->limit(1)
			->get($this->db_tabel)
			->row();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');

		$this->table->set_heading('Hari','Ruang','Jam Mulai','Jam Selesai', 'Status');
		
		foreach($data as $row)
		{
			$this->table->add_row(
			$row->SediaRuang_hari,
			$row->ruang_nama,
			$row->sesi_mulai,
			$row->sesi_selesai,
			$row->SediaRuang_stat,
			anchor('adm_sediaruang/edit/'.$row->idSediaRuang,'Edit',array('class' => 'edit')).''.
			anchor('adm_sediaruang/hapus/'.$row->idSediaRuang,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
			
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	public function tambah()
	{
		$ruang = array(
			'SediaRuang_hari' => $this->input->post('SediaRuang_hari'),
			'idmRuang' => $this->input->post('idmRuang'),
			'idmSesi_awal' => $this->input->post('idmSesi_awal'),
			'SediaRuang_stat' => $this->input->post('SediaRuang_stat'),
			'idmSesi_akhir' => $this->input->post('idmSesi_akhir')	
		);
		
		$this->db->insert($this->db_tabel,$ruang);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idSediaRuang)
	{
		$ruang = array(
			'SediaRuang_hari' => $this->input->post('SediaRuang_hari'),
			'idmRuang' => $this->input->post('idmRuang'),
			'idmSesi_awal' => $this->input->post('idmSesi_awal'),
			'SediaRuang_stat' => $this->input->post('SediaRuang_stat'),
			'idmSesi_akhir' => $this->input->post('idmSesi_akhir')	
		);
		
		$this->db->where('idSediaRuang',$idSediaRuang)->update($this->db_tabel, $ruang);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idSediaRuang)
	{
		$this->db->where('idSediaRuang',$idSediaRuang)->delete($this->db_tabel);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	
}