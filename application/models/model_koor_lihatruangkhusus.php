<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_koor_lihatruangkhusus extends CI_Model
{
	public $offset = 0;
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Tanggal','Ruang','Jam Mulai','Jam Selesai');
		
		foreach($data as $row)
		{
			
			// Konversi hari dan tanggal ke dalam format Indonesia (dd-mm-yyyy)
            $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->srt_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->srt_tgl));
            $hr_tgl = "$hari, $tgl";
		
			$this->table->add_row(
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				$row->sesi_selesai
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
}
/* End of file model_koor_lihatruangkhusus.php */
/* Location: ./application/models/model_koor_lihatruangkhusus.php */