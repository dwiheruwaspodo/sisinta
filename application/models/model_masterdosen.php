<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_masterdosen extends CI_Model 
{
	public $db_tabel = 'mdosen';
	public $db_tabel1 = 'users';
	public $per_halaman = 10;
	public $offset = 0;
	
	public function __construct()
	{
		parent::__construct();
	}

	public function load_form_rules_tambah()
	{
		$form = array (
			array(
				'field' => 'dos_nama',
				'label' => 'Nama',
				'rules' => 'required|max_length[50]'
			),
			array(
				'field' => 'dos_nip',
				'label' => 'NIP',
				'rules' => "required|numeric|exact_length[18]|is_unique[$this->db_tabel.dos_nip]"
			),
			array(
				'field' => 'dos_sandi',
				'label' => 'Sandi Dosen',
				'rules' => "required|numeric|exact_length[4]|is_unique[$this->db_tabel.dos_sandi]"
			),
			array(
				'field' => 'dos_prodi',
				'label' => 'Prodi Dosen',
				'rules' => 'required'
			),
		);	
		return $form;
	}
	
	public function load_form_rules_edit()
	{
		$form = array(
			array(
				'field' => 'dos_nip',
				'label' => 'NIP',
				'rules' => "required|numeric|exact_length[18]"
			),
			array(
				'field' => 'dos_nama',
				'label' => 'Nama',
				'rules' => 'required|max_length[50]'
			),
			array(
				'field' => 'dos_sandi',
				'label' => 'Sandi Dosen',
				'rules' => "required|numeric|exact_length[4]"
			),
			array(
				'field' => 'dos_prodi',
				'label' => 'Prodi Dosen',
				'rules' => 'required'
			),
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if ($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$form = $this->load_form_rules_edit();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function dd_dos()
	{
		return $this->db->order_by('idmDosen','ASC')
						->get($this->db_tabel)
						->result();
	}
	
	public function cari_semua($offset)
	{
		if (is_null($offset) || empty ($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('mdosen.dos_nama,mdosen.idmDosen,mdosen.dos_nip,mdosen.dos_sandi,mprodi.prodi_nama')
						->from('mdosen,mprodi')
						->where('mdosen.dos_prodi = mprodi.idmProdi')
						->limit($this->per_halaman, $this->offset)
						->order_by('idmDosen','ASC')
						->get()
						->result();
	}
	
	public function cari($idmDosen)
	{
		return $this->db->where('idmDosen',$idmDosen)
					->limit(1)
					->get($this->db_tabel)
					->row();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		$this->table->set_heading('No','Nama','NIP','Sandi','Prodi Dosen','Aksi');
		
		$no = 0 + $this->offset;
		
		foreach($data as $row)
		{
			$this->table->add_row(
			++$no,
			$row->dos_nama,
			$row->dos_nip,
			$row->dos_sandi,
			$row->prodi_nama,
			anchor('master_dosen/edit/'.$row->idmDosen,'Edit',array('class' => 'edit')).' '.
			anchor('master_dosen/hapus/'.$row->idmDosen,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'			=> $base_url,
			'total_rows'		=> $this->hitung_semua(),
			'per_page'			=> $this->per_halaman,
			'num_links'			=> 2,
			'use_page_numbers'	=> TRUE,
			'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	public function tambah_user()
	{
		$user = array(
			'users_name' => $this->input->post('dos_nip'),
			'users_passd' => md5($this->input->post('dos_sandi')),
			'ugrup_idugrup' => 5,
			'user_nama' => $this->input->post('dos_nama'),
		);
		$this->db->insert($this->db_tabel1,$user);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function tambah()
	{
		$dosen = array(
			'dos_nama'  => $this->input->post('dos_nama'),
			'dos_nip'   => $this->input->post('dos_nip'),
			'dos_sandi' => $this->input->post('dos_sandi'),
			'dos_prodi' => $this->input->post('dos_prodi'),
		);
		$this->db->insert($this->db_tabel, $dosen);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idmDosen)
	{
		$dosen = array(
			'dos_nama'	 => $this->input->post('dos_nama'),
			'dos_nip'	 => $this->input->post('dos_nip'),
			'dos_sandi'	 => $this->input->post('dos_sandi'),
			'dos_prodi' => $this->input->post('dos_prodi'),
		);
		
		$this->db->where('idmDosen',$idmDosen);
		$this->db->update($this->db_tabel, $dosen);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idmDosen)
	{
		$this->db->where('idmDosen',$idmDosen)->delete($this->db_tabel);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* End of file model_MasterDosen*/
/* Location: ./application/models/model_MasterDosen*/