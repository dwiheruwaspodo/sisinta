<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_koor_lihatbeban extends CI_Model
{
	public $db_tabel = 'vbeban';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function lihat_beban()
	{
		return $this->db->select('*')
						->from($this->db_tabel)
						->order_by('dos_nama','ASC')
						->get()
						->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		
		$this->table->set_heading('Nama Dosen','NIP','Sandi Dosen','Pembimbing 1', 'Pembimbing 2', 'Dosen Penguji 1','Dosen Penguji 2');
		
		foreach ($data as $row)
		{
			$this->table->add_row(
				$row->dos_nama,
				$row->dos_nip,
				$row->dos_sandi,
				$row->jml_bim1,
				$row->jml_bim2,
				$row->jml_uji,
				$row->jml_uji2
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
}