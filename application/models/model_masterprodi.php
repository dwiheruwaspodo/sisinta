<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_masterprodi extends CI_Model {

    private $db_tabel = 'mprodi';

	public function __construct()
	{
        parent::__construct();
	}

    public function load_form_rules_tambah()
    {
        $form_rules = array(
            array(
                'field' => 'idmProdi',
                'label' => 'Kode Prodi',
                'rules' => "required|numeric|is_unique[$this->db_tabel.idmProdi]"
            ),
            array(
                'field' => 'prodi_nama',
                'label' => 'Nama Prodi',
                'rules' => "required|max_length[32]|is_unique[$this->db_tabel.prodi_nama]"
            ),
        );
        return $form_rules;
    }

    public function load_form_rules_edit()
    {
        $form_rules = array(
            array(
                'field' => 'idmProdi',
                'label' => 'Kode Prodi',
                'rules' => "required|numeric|callback_is_id_kelas_exist"
            ),
            array(
                'field' => 'prodi_nama',
                'label' => 'Nama Prodi',
                'rules' => "required|max_length[32]|callback_is_kelas_exist"
            ),
        );
        return $form_rules;
    }

    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function cari_semua()
    {
        return $this->db->order_by('idmProdi', 'ASC')
                        ->get($this->db_tabel)
                        ->result();
    }

    public function cari($idmProdi)
    {
        return $this->db->where('idmProdi', $idmProdi)
                        ->limit(1)
                        ->get($this->db_tabel)
                        ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

        /// heading tabel
		$this->table->set_heading('Kode Prodi', 'Nama Prodi', 'Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
                $row->idmProdi,
                $row->prodi_nama,
                anchor('master_prodi/edit/'.$row->idmProdi,'Edit',array('class' => 'edit')).' '.
                anchor('master_prodi/hapus/'.$row->idmProdi,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function tambah()
    {
        $kelas = array(
                      'idmProdi' => $this->input->post('idmProdi'),
                      'prodi_nama' => $this->input->post('prodi_nama')
                      );
        $this->db->insert($this->db_tabel, $kelas);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmProdi)
    {
        $kelas = array(
            'idmProdi'=>$this->input->post('idmProdi'),
            'prodi_nama'=>$this->input->post('prodi_nama'),
        );

        // update db
        $this->db->where('idmProdi', $idmProdi);
		$this->db->update($this->db_tabel, $kelas);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmProdi)
    {
        $this->db->where('idmProdi', $idmProdi)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterProdi.php */
/* Location: ./application/models/model_MasterProdi.php */