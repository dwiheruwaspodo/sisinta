<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_all_katalogskripsi extends CI_Controller
{
	public $per_halaman = 10;
	public $offset 		= 0;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function lihat_katalog($offset)
	{
		if (is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 1')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->limit ($this->per_halaman, $this->offset)
						->order_by('skrip_tgl_reg','desc')
						->get()
						->result();
	}
	
	public function detail($detail)
	{
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.idmSkripsi',$detail)
						->get()
						->result();
	}
	
	
	public function buat_tabel($data)
	{
		$att = array(
			'width' => '480',
			'height' => '480',
			'scroolbars' => 'no',
			'status' => 'yes',
			'resizable' => 'no',
			'screenx' => '500',
			'screeny' => '80',
			'class' => 'detail'
		);
		
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		$this->table->set_heading('Judul Skripsi','NIM','Mahasiswa','Jurusan','Tgl Registrasi');

		foreach($data as $row)
		{
			$this->table->add_row(
				$row->skrip_judul,
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->prodi_nama,
				$row->skrip_tgl_reg,
				anchor_popup('all_katalogskripsi/detail/'.$row->idmSkripsi,'Detail',$att)
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
		
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->select('skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 1')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->get()
						->num_rows();
	}
	
	public function cari_judul($limit,$offset,$judul)
	{
		$cari_judul = $this->db->query("select skripsi.idmSkripsi,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2
						from skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b
						where skripsi.idmMhsw = mmhsw.idmmhsw
						AND skripsi.status_judul = 1
						AND skripsi.idmDosen_Bi1 = a.idmDosen
						AND skripsi.idmDosen_Bi2 = b.idmDosen
						AND mmhsw.idmProdi = mprodi.idmProdi
						AND skripsi.idmStatuspros = mstatuspros.idmstatus_pros
						AND skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip
						AND skripsi.skrip_judul like '%$judul%'
						order by skrip_tgl_reg DESC
						limit $offset,$limit						
						");
		return $cari_judul;
	}
	
	public function total($judul)
	{
		$cari_judul = $this->db->query("select skripsi.idmSkripsi,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2
						from skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b
						where skripsi.idmMhsw = mmhsw.idmmhsw
						AND skripsi.status_judul = 1
						AND skripsi.idmDosen_Bi1 = a.idmDosen
						AND skripsi.idmDosen_Bi2 = b.idmDosen
						AND mmhsw.idmProdi = mprodi.idmProdi
						AND skripsi.idmStatuspros = mstatuspros.idmstatus_pros
						AND skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip
						AND skripsi.skrip_judul like '%$judul%'
						");
		return $cari_judul;
	}
}