<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Msemuabisalihat extends CI_Model
{
	public $per_halaman = 10;
	public $offset = 0;
	public $db_tabel = 'view_katalog';
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function katalog($offset)
	{
		if(is_null ($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset *$this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('status_judul = 1')
						->limit($this->per_halaman,$this->offset)										
						->order_by('tgl_reg','desc')
						->get()
						->result();
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->select('*')
						->from($this->db_tabel)
						->get()
						->num_rows();
	}
	
	public function tabel($data)
	{
		$att = array(
			'width' => '480',
			'height' => '480',
			'scroolbars' => 'no',
			'status' => 'yes',
			'resizable' => 'no',
			'screenx' => '500',
			'screeny' => '80',
			'class' => 'detail'
		);
		$this->load->library('table');
		$this->table->set_heading('Judul','NIM','Nama','Prodi');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->skrip_judul,
				$row->nim,
				$row->nama,
				$row->prodi,
				anchor_popup('semuabisalihat/detail/'.$row->id,'Detail',$att)
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function detail($id)
	{
		return $this->db->select('*')
						->from($this->db_tabel)
						->where('id',$id)
						->where('status_judul = 1')
						->order_by('tgl_reg','desc')
						->get()
						->result();
	}
	
	public function cari_cari($limit,$offset,$judul)
	{
		$katalog = $this->db->query("select * 
		from view_katalog 
		where skrip_judul like '%$judul%' 
		AND status_judul = 1 
		order by tgl_reg DESC
		LIMIT $offset,$limit");
		
		return $katalog;
	}
	
	public function tot_hal($judul)
	{
		$hitung = $this->db->query("select * 
		from view_katalog 
		where skrip_judul like '%$judul%'
		AND status_judul = 1 
		");
		return $hitung;
	}
}