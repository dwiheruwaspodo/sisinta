<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_masterruang extends CI_Model {

    public $db_tabel = 'mruang';
	public $per_halaman = 10;
	public $offset = 0;

	public function __construct()
	{
        parent::__construct();
	}

    public function load_form_rules_tambah()
    {
        $form_rules = array(
            array(
                'field' => 'idmRuang',
                'label' => 'Kode Ruang',
                'rules' => "required|numeric|is_unique[$this->db_tabel.idmRuang]"
            ),
            array(
                'field' => 'ruang_nama',
                'label' => 'Nama Ruangan',
                'rules' => "required|max_length[32]|is_unique[$this->db_tabel.ruang_nama]"
            ),
        );
        return $form_rules;
    }

    public function load_form_rules_edit()
    {
        $form_rules = array(
            array(
                'field' => 'idmRuang',
                'label' => 'Kode Ruang',
                'rules' => "required|numeric|callback_is_id_ruang_exist"
            ),
            array(
                'field' => 'ruang_nama',
                'label' => 'Nama Ruangan',
                'rules' => "required|max_length[32]|callback_is_ruang_exist"
            ),
        );
        return $form_rules;
    }

    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function cari_semua($offset)
    {
        if(is_null($offset) || empty($offset))
		{
			$this->offset=0;
		}
		else
		{
			$this->offset = ($offset*$this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->limit($this->per_halaman, $this->offset)
						->order_by('idmRuang', 'ASC')
                        ->get()
                        ->result();
    }

	public function dd_ruang()
	{
		return $this->db->order_by('idmRuang','ASC')
						->get($this->db_tabel)
						->result();
	}

    public function cari($idmRuang)
    {
        return $this->db->where('idmRuang', $idmRuang)
                        ->limit(1)
                        ->get($this->db_tabel)
                        ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        // buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

        /// heading tabel
        $this->table->set_heading('Kode Ruang', 'Nama Ruangan', 'Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
                $row->idmRuang,
                $row->ruang_nama,
                anchor('master_ruang/edit/'.$row->idmRuang,'Edit',array('class' => 'edit')).' '.
                anchor('master_ruang/hapus/'.$row->idmRuang,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
				'base_url'			=> $base_url,
				'total_rows'		=> $this->hitung_semua(),
				'per_page'			=> $this->per_halaman,
				'num_links'			=> 2,
				'use_page_numbers'	=> TRUE,
	            'first_link'       => '&#124;&lt; First',
            	'last_link'        => 'Last &gt;&#124;',
	            'next_link'        => 'Next &gt;',
    	        'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	
    public function tambah()
    {
        $ruang = array(
                      'idmRuang' => $this->input->post('idmRuang'),
                      'ruang_nama' => $this->input->post('ruang_nama')
                      );
        $this->db->insert($this->db_tabel, $ruang);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmRuang)
    {
        $ruang = array(
            'idmRuang'=>$this->input->post('idmRuang'),
            'ruang_nama'=>$this->input->post('ruang_nama'),
        );

        // update db
        $this->db->where('idmRuang', $idmRuang);
		$this->db->update($this->db_tabel, $ruang);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmRuang)
    {
        $this->db->where('idmRuang', $idmRuang)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterRuang.php */
/* Location: ./application/models/model_MasterRuang.php */