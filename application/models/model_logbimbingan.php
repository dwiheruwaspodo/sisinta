<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_logbimbingan extends CI_Model
{
	public $db_tabel = 'logbook';
	public $db_dos = 'mdosen';
	public $db_mhs = 'mmhsw';
	public $per_halaman = 10;
	public $offset = 0;
	
	public function caricaricari($idLogBook)
	{
		return $this->db->where('idLogBook',$idLogBook)
						->get($this->db_tabel)
						->result();
	}
	
	public function cari($idLogBook)
	{
		return $this->db->where('idLogBook',$idLogBook)
						->limit(1)
						->get($this->db_tabel)
						->row();
	}
	
	public function id_dos()
	{
		$dos = $this->session->userdata('users_name');
		return $this->db->select('idmDosen')
						->from($this->db_dos)
						->where('dos_nip',$dos)
						->limit(1)
						->get()
						->row()->idmDosen;
	}
	
	public function id_mhs()
	{
		$mhs = $this->session->userdata('users_name');
		return $this->db->select('idmmhsw')
						->from($this->db_mhs)
						->where('mhsw_nim',$mhs)
						->limit(1)
						->get()
						->row()->idmmhsw;
	}
	
	public function rules_ins()
	{
		$form = array(
			array(
				'field' => 'idmmhsw',
				'label' => 'Mahasiswa Bimbingan',
				'rules' => 'required'
			)
		);
		return $form;
	}
	
	public function rules_catatan_dos()
	{
		$form = array(
			array(
			'field' => 'komen_dos',
			'label' => 'Masukan Pembimbing',
			//'rules' => 'required'
			),
			array(
			'field' => 'log_verif',
			'label' => 'Verifikasi',
			'rules' => 'required'
			)
		);
		return $form;
	}
	
	public function rules_catatan_mhs()
	{
		$form = array(
				'field' => 'logbook_mhs',
				'label' => 'Konsultasi',
				'rules' => 'required'
			);
		return $form;
	}
	
	public function validasi_ins()
	{
		$form = $this->rules_ins();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_catatan_dos()
	{
		$form = $this->rules_catatan_dos();
		$this->form_validation->set_rules($form);
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_catatan_mhs()
	{
		$form = $this->rules_catatan_mhs();
		$this->form_validation->set_rules($form);
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function mhs_bimb1()
	{
		$dos = $this->id_dos($this->session->userdata('users_name'));
		
		return $this->db->select('mmhsw.mhsw_nama,mmhsw.idmmhsw')
						->from('skripsi,mmhsw')
						->where('skripsi.idmDosen_Bi1',$dos)
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.idmStatusSkrip < 6')
						->get()
						->result();
	}
	
	public function mhs_bimb2()
	{
		$dos = $this->id_dos($this->session->userdata('users_name'));
		
		return $this->db->select('mmhsw.mhsw_nama,mmhsw.idmmhsw')
						->from('skripsi,mmhsw')
						->where('skripsi.idmDosen_Bi2',$dos)
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.idmStatusSkrip < 6')
						->get()
						->result();
	}
	
	public function ins()
	{
		$tambah = array(
			'logbook_tgl' => date('Y-m-d'),
			'idmdosen' => $this->id_dos($this->session->userdata('users_name')),
			'idmmhsw' => $this->input->post('idmmhsw'),
			'log_verif' => 0,
		);
		$this->db->insert($this->db_tabel,$tambah);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function catatan_mhs($idLogBook)
	{
		$catatan = array(
			'logbook_mhs' => $this->input->post('logbook_mhs')
		);
		
		$this->db->where('idLogBook',$idLogBook)->update($this->db_tabel, $catatan);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function catatan_dos($idLogBook)
	{
		$catatan = array(
			'komen_dos' => $this->input->post('komen_dos'),
			'log_verif' => $this->input->post('log_verif')
		);
		$this->db->where('idLogBook',$idLogBook)->update($this->db_tabel,$catatan);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function lihat_catatan_p1()
	{
		$id = $this->id_mhs($this->session->userdata('users_name'));
		return $this->db->select('skripsi.idmDosen_Bi1')
						->from('skripsi,mmhsw,mdosen')
						->where('skripsi.idmMhsw',$id)
						->where('mdosen.idmDosen = skripsi.idmDosen_Bi1')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->get()
						->row()->idmDosen_Bi1;
	}
	
	public function lihat_catatan_p2()
	{
		$id = $this->id_mhs($this->session->userdata('users_name'));
		return $this->db->select('skripsi.idmDosen_Bi2')
						->from('skripsi,mmhsw,mdosen')
						->where('skripsi.idmMhsw',$id)
						->where('mdosen.idmDosen = skripsi.idmDosen_Bi2')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->get()
						->row()->idmDosen_Bi2;
	}
	
	public function logbook_mhs_darip1()
	{
		$idmhs = $this->id_mhs($this->session->userdata('users_name'));
		$iddos = $this->lihat_catatan_p1();
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mdosen.dos_nama,logbook.komen_dos,logbook.log_verif')
						->from('logbook,mdosen,mmhsw')
						->where('logbook.idmdosen',$iddos)
						->where('logbook.idmdosen = mdosen.idmDosen')
						->where('logbook.idmmhsw',$idmhs)
						->where('logbook.idmmhsw = mmhsw.idmmhsw')
						->order_by('logbook_tgl','desc')
						->get()
						->result();
	}
	
	public function logbook_mhs_darip2()
	{
		$idmhs = $this->id_mhs($this->session->userdata('users_name'));
		$iddos = $this->lihat_catatan_p2();
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mdosen.dos_nama,logbook.komen_dos,logbook.log_verif')
						->from('logbook,mdosen,mmhsw')
						->where('logbook.idmdosen',$iddos)
						->where('logbook.idmdosen = mdosen.idmDosen')
						->where('logbook.idmmhsw',$idmhs)
						->where('logbook.idmmhsw = mmhsw.idmmhsw')
						->order_by('logbook_tgl','desc')
						->get()
						->result();
	}
	
	public function logbook_mhs_dilist($idmhs)
	{
		$iddos = $this->id_dos($this->session->userdata('users_name'));
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mdosen.dos_nama,logbook.komen_dos,logbook.log_verif,mmhsw.mhsw_nama')
						->from('logbook,mdosen,mmhsw')
						->where('logbook.idmdosen = mdosen.idmDosen')
						->where('logbook.idmdosen',$iddos)
						->where('logbook.idmmhsw',$idmhs)
						->where('logbook.idmmhsw = mmhsw.idmmhsw')
						->order_by('logbook_tgl','desc')
						->get()
						->result();
	}
	
	public function logbook_dos($offset)
	{
		$idmdos = $this->id_dos($this->session->userdata('users_name'));
		
		if(is_null($offset) || empty($offset))
		{
			$offset = 0;
		}
		else
		{
			$offset = ($this->per_halaman * $this->offset) - $this->per_halaman;
		}
		
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mmhsw.mhsw_nama,logbook.komen_dos,logbook.log_verif')
						->from('logbook,mmhsw')
						->where('logbook.idmmhsw = mmhsw.idmmhsw')
						->where('logbook.idmdosen',$idmdos)
						->where('logbook.log_verif = 0')
						->order_by('logbook_tgl','desc')
						->limit($this->per_halaman,$this->offset)
						->get()
						->result();
		
	}
	
	
	public function logbook_dos_semua($offset)
	{
		$idmdos = $this->id_dos($this->session->userdata('users_name'));
		
		if(is_null($offset) || empty($offset))
		{
			$offset = 0;
		}
		else
		{
			$offset = ($this->per_halaman * $this->offset) - $this->per_halaman;
		}
		
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mmhsw.mhsw_nama,logbook.komen_dos,logbook.log_verif')
						->from('logbook,mmhsw')
						->where('logbook.idmmhsw = mmhsw.idmmhsw')
						->where('logbook.idmdosen',$idmdos)
						->order_by('logbook_tgl','desc')
						->limit($this->per_halaman,$this->offset)
						->get()
						->result();
		
	}
	
	public function tabel_dos($data)
	{
		$this->load->library('table');
		$this->table->set_heading('tanggal','Konsultasi','Nama Mahasiswa','Masukkan Pembimbing','Status Logbook');
  		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->logbook_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->logbook_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$verifikasi = $this->verifikasi_mhs($row->log_verif);
			$this->table->add_row(
				$hr_tgl,
				$row->logbook_mhs,
				$row->mhsw_nama,
				$row->komen_dos,
				$verifikasi,
				anchor('catatan_bimb_dos/catatan_dosen/'.$row->idLogBook,'Beri Masukan',array('class' => 'baru'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_dos_koor($data)
	{
		$this->load->library('table');
		$this->table->set_heading('tanggal','Konsultasi','Nama Mahasiswa','Masukkan Pembimbing','Status Logbook');
  		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->logbook_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->logbook_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$verifikasi = $this->verifikasi_mhs($row->log_verif);
			$this->table->add_row(
				$hr_tgl,
				$row->logbook_mhs,
				$row->mhsw_nama,
				$row->komen_dos,
				$verifikasi,
				anchor('catatan_bimb_koor/catatan_dosen/'.$row->idLogBook,'Beri Masukan',array('class' => 'baru'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_dos_kaj($data)
	{
		$this->load->library('table');
		$this->table->set_heading('tanggal','Konsultasi','Nama Mahasiswa','Masukkan Pembimbing','Status Logbook');
  		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->logbook_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->logbook_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$verifikasi = $this->verifikasi_mhs($row->log_verif);
			$this->table->add_row(
				$hr_tgl,
				$row->logbook_mhs,
				$row->mhsw_nama,
				$row->komen_dos,
				$verifikasi,
				anchor('catatan_bimb_kaj/catatan_dosen/'.$row->idLogBook,'Beri Masukan',array('class' => 'baru'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_dos_semua($data)
	{
		$this->load->library('table');
		$this->table->set_heading('tanggal','Konsultasi','Nama Mahasiswa','Masukkan Pembimbing','Status Logbook');
  		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->logbook_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->logbook_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$verifikasi = $this->verifikasi_mhs($row->log_verif);
			$this->table->add_row(
				$hr_tgl,
				$row->logbook_mhs,
				$row->mhsw_nama,
				$row->komen_dos,
				$verifikasi
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging_dos($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitungs_dos(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitungs_dos()
	{
		$idmdos = $this->id_dos($this->session->userdata('users_name'));
		return $this->db->select('logbook.idLogBook,logbook.logbook_tgl,logbook.logbook_mhs,mmhsw.mhsw_nama,logbook.komen_dos')
					->from('logbook,mmhsw')
					->where('logbook.idmmhsw = mmhsw.idmmhsw')
					->where('logbook.idmdosen',$idmdos)
					->get()
					->num_rows();
	}
	
	public function verifikasi_mhs($ver)
	{
		if($ver == '1')
		{
			$a = 'Verifikasi';
			return $a;
		}
		else
		{
			$b = 'Belum di Verifikasi';
			return $b;
		}
	}
}

/* End of file model_logbimbingan.php */
/* Location: ./application/models/model_logbimbingan.php */