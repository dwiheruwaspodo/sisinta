<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_koor_dataskripsi extends CI_Model
{
	public $db_tabel1 = 'skripsi';
	public $db_tabel3 = 'mmhsw';
	public $db_tabel4 = 'mdosen';
	
	public $offset = 0;
	public $per_halaman = 10;
	
	
	public function iya($id)
	{
		$diterima = array(
			'status_judul' => 1,
		);
		$this->db->where('idmSkripsi',$id)->update($this->db_tabel1,$diterima);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
		
	public function tidak($id)
	{
		$ditolak = array(
			'status_judul' => 0,
		);
		$this->db->where('idmSkripsi',$id)->update($this->db_tabel1,$ditolak);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function skripsi_baru($offset)
	{
		if (is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		$ses = $this->session->userdata('DP');
		
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 0')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('mmhsw.idmProdi',$ses)
						->limit ($this->per_halaman, $this->offset)
						->order_by('skrip_tgl_reg','desc')
						->get()
						->result();
	}
	
	public function skripsi_oke($offset)
	{
		if (is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		$ses = $this->session->userdata('DP');
		
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 1')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('mmhsw.idmProdi',$ses)
						->limit ($this->per_halaman, $this->offset)
						->order_by('skrip_tgl_reg','desc')
						->get()
						->result();
	}
	
	public function paging_oke($base_url)
	{
		$this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->semua_oke(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	
	public function paging_baru($base_url)
	{
		$this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->semua_baru(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
	}
	
	public function semua_oke()
	{
		$ses = $this->session->userdata('DP');
		
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 1')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('mmhsw.idmProdi',$ses)
						->order_by('skrip_tgl_reg','desc')
						->get()
						->num_rows();
	}
	
	public function semua_baru()
	{
		$ses = $this->session->userdata('DP');
		
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2')
						->from ('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.status_judul = 0')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('mmhsw.idmProdi',$ses)
						->order_by('skrip_tgl_reg','desc')
						->get()
						->num_rows();
	}
	
	public function buat_tabel($data)
	{
		$att = array(
			'width' => '480',
			'height' => '480',
			'scroolbars' => 'no',
			'status' => 'yes',
			'resizable' => 'no',
			'screenx' => '500',
			'screeny' => '80',
			'class' => 'setting'
		);
		
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		$this->table->set_heading('Judul Skripsi','NIM','Mahasiswa','Jurusan','Pembimbing 1', 'Pembimbing 2');

		foreach($data as $row)
		{
			$this->table->add_row(
				$row->skrip_judul,
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->prodi_nama,
				$row->p1,
				$row->p2,
				anchor('koor_dataskripsi/detail/'.$row->idmSkripsi,'proses',$att)
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
		
	}
	
	public function detail($detail)
	{
		return $this->db->select('skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2,skripsi.status_judul')
						->from('skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b')
						->where('skripsi.idmDosen_Bi1 = a.idmDosen')
						->where('skripsi.idmDosen_Bi2 = b.idmDosen')
						->where('mmhsw.idmProdi = mprodi.idmProdi')
						->where('skripsi.idmStatuspros = mstatuspros.idmstatus_pros')
						->where('skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip')
						->where('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where('skripsi.idmSkripsi',$detail)
						->get()
						->result();
	}
	
	public function dd_dosen()
	{
		return $this->db->order_by('idmDosen','ASC')
						->get($this->db_tabel4)
						->result();
	}
	
	public function rules_edit()
	{
		$form = array(
			array(
				'field' => 'idmDosen_Bi1',
				'label' => 'dosen pembimbing 1',
				'rules' => 'required'
			),
			array(
				'field' => 'idmDosen_Bi2',
				'label' => 'dosen pembimbing 2',
				'rules' => 'required'
			),
			array(
				'field' => 'idmKategori1',
				'label' => 'Kategori 1',
				'rules' => 'required'
			),
			array(
				'field' => 'idmKategori2',
				'label' => 'Kategori 2',
				'rules' => 'required'
			),
		);
		return $form;
	}
	
	public function validasi_edit()
	{
		$form = $this->rules_edit();
		$this->form_validation->set_rules($form);

		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idmSkripsi)
	{
		$edit = array(
			'idmDosen_Bi1' => $this->input->post('idmDosen_Bi1'),
			'idmDosen_Bi2' => $this->input->post('idmDosen_Bi2'),
			'idmKategori1' => $this->input->post('idmKategori1'),
			'idmKategori2' => $this->input->post('idmKategori2')
		);
		$this->db->where('idmSkripsi',$idmSkripsi)->update($this->db_tabel1,$edit);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari($idmSkripsi)
	{
		return $this->db->where('idmSkripsi',$idmSkripsi)
						->limit(1)
						->get($this->db_tabel1)
						->row();
	}
	
	public function hapus($id)
	{
		$this->db->where('idmSkripsi',$id)->delete($this->db_tabel1);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_nama($limit,$offset,$nama)
	{
		$ses = $this->session->userdata('DP');
		$cari_nama = $this->db->query("SELECT skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2
						FROM skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b
						WHERE skripsi.idmMhsw = mmhsw.idmmhsw
						AND skripsi.idmDosen_Bi1 = a.idmDosen
						AND skripsi.idmDosen_Bi2 = b.idmDosen
						AND mmhsw.idmProdi = mprodi.idmProdi
						AND skripsi.idmStatuspros = mstatuspros.idmstatus_pros
						AND skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip
						AND mmhsw.mhsw_nama LIKE '%$nama%'
						AND mmhsw.idmProdi = $ses
						limit $offset,$limit						
						");
		return $cari_nama;
	}
	
	public function total($nama)
	{
		$ses = $this->session->userdata('DP');
		$cari_judul = $this->db->query("SELECT skripsi.idmSkripsi,skripsi.skrip_abstrak,skripsi.skrip_judul,mmhsw.mhsw_nim, mmhsw.mhsw_nama, mstatusskrip.StatusSkrip_tipe,mprodi.prodi_nama, mstatuspros.statuspros_tipe, skripsi.skrip_tgl_reg, a.dos_nama AS p1,b.dos_nama AS p2
						FROM skripsi,mmhsw,mprodi,mstatuspros, mstatusskrip, mdosen a, mdosen b
						WHERE skripsi.idmMhsw = mmhsw.idmmhsw
						AND skripsi.idmDosen_Bi1 = a.idmDosen
						AND skripsi.idmDosen_Bi2 = b.idmDosen
						AND mmhsw.idmProdi = mprodi.idmProdi
						AND skripsi.idmStatuspros = mstatuspros.idmstatus_pros
						AND skripsi.idmStatusSkrip = mstatusskrip.idmStatusSkrip
						AND mmhsw.mhsw_nama LIKE '%$nama%'
						AND mmhsw.idmProdi = $ses
						");
		return $cari_judul;
	}
}

/* 
End of file model_koor_dataskripsi.php */
/* Location: ./application/models/model_koor_dataskripsi.php */