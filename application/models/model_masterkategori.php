<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_masterkategori extends CI_Model {

    public $db_tabel    = 'mkategori';
    public $per_halaman = 10;
    public $offset      = 0;

    // rules form validasi, proses TAMBAH
    public function load_form_rules_tambah()
    {
        $form = array(
                       
                            'field' => 'katg_nama',
                            'label' => 'kategori',
                            'rules' => 'required'
                        
        );
        return $form;
    }

    // rules form validasi, proses EDIT
    public function load_form_rules_edit()
    {
        $form = array(
                       
                            'field' => 'katg_nama',
                            'label' => 'kategori',
                            'rules' => 'required'
                        
        );
        return $form;
    }

    // jalankan proses validasi, untuk operasi TAMBAH
    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // jalankan proses validasi, untuk operasi EDIT
    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

	public function dd_kat()
	{
		return $this->db->order_by('idmKategori','ASC')
                        ->get($this->db_tabel)
                        ->result();
	}
	
    public function cari_semua($offset)
	{
        if (is_null($offset) || empty($offset))
        {
            $this->offset = 0;
        }
        else
        {
            $this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
        }		
        // $offset end

        return $this->db->order_by('idmKategori','ASC')
                        ->get($this->db_tabel)
                        ->result();
	}

    public function cari($idmKategori)
    {
        return $this->db->where('idmKategori', $idmKategori)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        // buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

        // heading tabel
        $this->table->set_heading('ID', 'Kategori', 'Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
				$row->idmKategori,
                $row->katg_nama,
                anchor('master_kategori/edit/'.$row->idmKategori,'Edit',array('class' => 'edit')).' '.
                anchor('master_kategori/hapus/'.$row->idmKategori,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function paging($base_url)
    {
        $this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    public function hitung_semua()
    {
        return $this->db->count_all($this->db_tabel);
    }

    public function tambah()
    {
        $ket = array(
            'katg_nama' => $this->input->post('katg_nama'),
        );
        $this->db->insert($this->db_tabel, $ket);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmKategori)
    {
        $ket = array(
            'katg_nama'=>$this->input->post('katg_nama'),
        );

        // update db
        $this->db->where('idmKategori', $idmKategori);
        $this->db->update($this->db_tabel, $ket);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmKategori)
    {
        $this->db->where('idmKategori', $idmKategori)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterKategori.php */
/* Location: ./application/models/model_MasterKategori.php */