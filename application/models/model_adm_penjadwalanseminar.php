<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_adm_penjadwalanseminar extends CI_Model
{
	public $db_tabel = 'tsemsid';
	public $db_tabel2 = 'skripsi';
	public $offset = 0;
	public $per_halaman = 10;
	
	public function rules_tambah()
	{
		$form = array(
			array(
				'field' => 'semsid_tgl',
				'label' => 'tanggal',
				'rules' => 'required|callback_is_format_tanggal'
			),
			array(
				'field' => 'idmSkripsi',
				'label' => 'Nama',
				'rules' => 'required'
			),
			array(
				'field' => 'idmSesi',
				'label' => 'Jam Mulai',
				'rules' => 'required'
			),
			array(
				'field' => 'idmRuang',
				'label' => 'Ruangan',
				'rules' => 'required'
			),
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$form = $this->rules_tambah();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function tambah()
	{
		$tambah = array(
			'semsid_tgl' => date('Y-m-d', strtotime($this->input->post('semsid_tgl'))),
			'idmSkripsi' => $this->input->post('idmSkripsi'),
			'idmKgtn'	=> 1,
			'lockedit'	=> 0,
			'idmSesi'	=> $this->input->post('idmSesi'),
			'idmRuang'	=> $this->input->post('idmRuang'),
		);
		
		$this->db->insert($this->db_tabel, $tambah);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit_skripsi($dor)
	{
		$update = array(
			'idmStatusPros'=> 3,
		);
		$this->db->where('idmSkripsi',$dor)->update($this->db_tabel2,$update);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function get_idSk($id)
	{
		return $this->db->select('idmSkripsi')
						->from($this->db_tabel)
						->where('idtSemSid',$id)
						->get()
						->row()->idmSkripsi;
	}
	
	public function batal_seminar($dor)
	{
		$update = array(
			'idmStatusPros'=> 2,
		);
		$this->db->where('idmSkripsi',$dor)->update($this->db_tabel2,$update);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function rules_edit()
	{
		$form = array(
			array(
				'field' => 'semsid_tgl',
				'label' => 'tanggal',
				'rules' => 'required|callback_is_format_tanggal'
			),
			array(
				'field' => 'idmSesi',
				'label' => 'Jam Mulai',
				'rules' => 'required'
			),
			array(
				'field' => 'idmRuang',
				'label' => 'Ruangan',
				'rules' => 'required'
			),
		);
		return $form;
	}
	
	public function validasi_edit()
	{
		$edit = $this->rules_edit();
		$this->form_validation->set_rules($edit);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idtSemSid)
	{
		$edit = array(
			'semsid_tgl' => date('Y-m-d',strtotime($this->input->post('semsid_tgl'))),
			'idmSesi'	=> $this->input->post('idmSesi'),
			'idmRuang'	=> $this->input->post('idmRuang'),
		);
		$this->db->where('idtSemSid', $idtSemSid)->update($this->db_tabel, $edit);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_semua($offset)
	{
		if(is_null ($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}

		return $this->db->select('tsemsid.idtSemSid, mmhsw.mhsw_nim, mmhsw.mhsw_nama, tsemsid.semsid_tgl, skripsi.skrip_judul, mkgtn.kgtn_jenis, mruang.ruang_nama, msesi.sesi_mulai, a.dos_nama AS P1,b.dos_nama AS P2')
						->from('mmhsw, msesi, skripsi, tsemsid, mkgtn, mruang, mdosen a, mdosen b')
						->where('tsemsid.idmSkripsi = skripsi.idmSkripsi')
						->where('a.idmDosen = skripsi.idmDosen_Bi1')
						->where('b.idmDosen = skripsi.idmDosen_Bi2')
						->where('tsemsid.idmSesi = msesi.idmSesi')
						->where('tsemsid.idmKgtn = mkgtn.idmKgtn')
						->where('tsemsid.idmRuang = mruang.idmRuang')
						->where('mmhsw.idmmhsw = skripsi.idmMhsw')
						->where('tsemsid.idmKgtn = 1')
						->where('tsemsid.semsid_tgl > now()')
						->limit($this->per_halaman, $this->offset)
						->order_by('semsid_tgl','DESC')
						->get()
						->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Nim','Nama','Judul','Pembimbing 1','Pembimbing 2','Tanggal','Ruangan','Jam Mulai','Kegiatan');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->skrip_judul,
				$row->P1,
				$row->P2,
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				anchor('adm_penjadwalanseminar/edit/'.$row->idtSemSid, 'Edit',array('class' => 'edit')).''.
				anchor('adm_penjadwalanseminar/hapus/'.$row->idtSemSid, 'Hapus', array('class' => 'delete','onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabel_k($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Nim','Nama','Judul','Pembimbing 1','Pembimbing 2','Tanggal','Ruangan','Jam Mulai','Kegiatan');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->skrip_judul,
				$row->P1,
				$row->P2,
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				$row->kgtn_jenis,
				anchor('koor_penjadwalanseminar/edit/'.$row->idtSemSid, 'Edit',array('class' => 'edit')).''.
				anchor('koor_penjadwalanseminar/hapus/'.$row->idtSemSid, 'Hapus', array('class' => 'delete','onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{

		return $this->db->select('tsemsid.idtSemSid, mmhsw.mhsw_nim, mmhsw.mhsw_nama, tsemsid.semsid_tgl, skripsi.skrip_judul, mkgtn.kgtn_jenis, mruang.ruang_nama, msesi.sesi_mulai, a.dos_nama AS P1,b.dos_nama AS P2')
						->from('mmhsw, msesi, skripsi, tsemsid, mkgtn, mruang, mdosen a, mdosen b')
						->where('tsemsid.idmSkripsi = skripsi.idmSkripsi')
						->where('a.idmDosen = skripsi.idmDosen_Bi1')
						->where('b.idmDosen = skripsi.idmDosen_Bi2')
						->where('tsemsid.idmSesi = msesi.idmSesi')
						->where('tsemsid.idmKgtn = mkgtn.idmKgtn')
						->where('tsemsid.idmRuang = mruang.idmRuang')
						->where('mmhsw.idmmhsw = skripsi.idmMhsw')
						->where('tsemsid.idmKgtn = 1')
						->where('tsemsid.semsid_tgl > now()')
						->get()
						->num_rows();
	}
	
	public function cari($idtSemSid)
	{
		return $this->db->select('idtSemSid',$idtSemSid)
						->limit(1)
						->get($this->db_tabel)
						->row();
	}
	
	public function idr()
	{
		$id = $this->input->post('idmRuang');
		
		return $this->db->select('idSediaRuang')
						->where('idSediaRuang',$id)
						->get()
						->row()->idSediaRuang;
	}
	
		public function dd_mhs()
	{
		return $this->db->select('skripsi.idmSkripsi,
								mmhsw.mhsw_nama
								')
						->from ('mmhsw,
								skripsi')
						->where ('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where ('skripsi.idmStatusPros = 2')
						->get()
						->result();
	}
	
	public function dd_ruang()
	{
		$t = 'tersedia';
		return $this->db->select('mruang.idmRuang,mruang.ruang_nama,msesi.sesi_mulai')
						->from('sediaruanghari,mruang,msesi')
						->where('sediaruanghari.idmRuang = mruang.idmRuang')
						->where('sediaruanghari.SediaRuang_stat',$t)
						->where('sediaruanghari.idmSesi_awal = msesi.idmSesi')
						->get()
						->result();
	}
	
	public function ruang_khusus()
	{
		return $this->db->select('mruang.idmRuang,mruang.ruang_nama')
						->from('sediaruangtanggal, mruang')
						->where('sediaruangtanggal.idmRuang = mruang.idmRuang')
						->get()
						->result();
	}
	
	public function hapus($id)
	{
		$this->db->where('idtSemSid',$id)->delete($this->db_tabel);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
/* 
End of file model_admpenjadwalanseminar.php */
/* Location: ./application/models/model_admpenjadwalan.php */