<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_adm_hakakses extends CI_Model
{
	public $db_1 = 'users';
	public $offset = 0;
	public $per_halaman = 10;
	
	public function tampil_koor()
	{
		return $this->db->select('users_name,user_nama,ugrup.ugrup_nama')
						->from('users,ugrup')
						->where('ugrup.idugrup = users.ugrup_idugrup')
						->where('users.ugrup_idugrup = 3')
						->get()
						->result();
	}
	
	public function tampil_kajur()
	{
		return $this->db->select('users_name,user_nama,ugrup.ugrup_nama')
						->from('users,ugrup')
						->where('ugrup.idugrup = users.ugrup_idugrup')
						->where('users.ugrup_idugrup = 2')
						->get()
						->result();
	}
	
	public function para_admin()
	{
		return $this->db->select('users_name,user_nama,ugrup.ugrup_nama')
						->from('users,ugrup')
						->where('ugrup.idugrup = users.ugrup_idugrup')
						->where('users.ugrup_idugrup = 4')
						->get()
						->result();
	}
	
	public function tabel_admin($admin)
	{
		$this->load->library('table');
		$this->table->set_heading('No Pegawai','Nama admin');
		foreach($admin as $row)
		{
			$this->table->add_row(
			$row->users_name,
			$row->user_nama,
			anchor('adm_admintandingan/hapus/'.$row->users_name,'Hapus',array('class' => 'gajadi'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function form_add_adm()
	{
		$form = array(
					array(
						'field' => 'users_name',
						'rules' => 'required',
						'label' => 'username'
					),
					array(
						'field' => 'user_nama',
						'rules' => 'required',
						'label' => 'nama',
					),
		);
		return $form;
	}

	public function validasi_adm()
	{
		$form = $this->form_add_adm();
		$this->form_validation->set_rules($form);
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function adm_tandingan()
	{
		$adm = array(
			'users_name' => $this->input->post('users_name'),
			'users_passd' => md5($this->input->post('users_name')),
			'user_nama' => $this->input->post('user_nama'),
			'ugrup_idugrup' => 4,
		);
		$this->db->insert($this->db_1,$adm);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus_adm($adm)
	{
		$this->db->where('users_name',$adm)->delete($this->db_1);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function dosen($offset)
	{
		if(is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('users_name,user_nama')
						->from($this->db_1)
						->where('ugrup_idugrup = 5')
						->limit($this->per_halaman,$this->offset)
						->get()
						->result();
	}
	
	public function cari($users_name)
	{
		return $this->db->where('users_name',$users_name)
						->limit(1)
						->get($this->db_1)
						->row();
	}
	
	public function tabel_inginjadikoordinator($data)
	{
		$this->load->library('table');
		$this->table->set_heading('NIP','Nama Dosen','Aksi');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('adm_hakakses/jadikan_koor/'.$row->users_name,'aktivasi',array('class' => 'jadi'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_inginjadikajur($data)
	{
		$this->load->library('table');
		$this->table->set_heading('NIP','Nama Dosen','Aksi');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('adm_hakakses_kajur/jadikan_kajur/'.$row->users_name,'aktivasi',array('class' => 'jadi'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{	
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_dosen(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_dosen()
	{
		return $this->db->select('users_name,user_nama')
						->from($this->db_1)
						->where('ugrup_idugrup = 5')
						->get()
						->num_rows();
	}
	
	public function tabel_koor($data)
	{
		$this->load->library('table');
		$this->table->set_heading('NIP','Nama Koordinator');
		
		foreach($data as $row)
		{
			$this->table->add_row
			(
				$row->users_name,
				$row->user_nama,
				anchor('adm_hakakses/jadikan_dosen/'.$row->users_name,'non-aktif',array('class' => 'gajadi'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function tabel_kajur($data)
	{
		$this->load->library('table');
		$this->table->set_heading('NIP','Nama Ketua Jurusan');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('adm_hakakses_kajur/jadikan_dosen/'.$row->users_name,'non-aktif',array('class' => 'gajadi'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function rules_koor()
	{
		$form = array(
				'field' => 'users_name',
				'label' => 'koordinator',
				'rules' => 'required'
		);
		return $form;
	}
	
	public function validasi()
	{
		$form = $this->rules_koor();
		$this->form_validation->set_rules($form);
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function koorbaru($users_name)
	{
		$baru = array(
			'ugrup_idugrup' => '3'
		);
		$this->db->where('users_name',$users_name)->update($this->db_1,$baru);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function kajurbaru($users_name)
	{
		$baru = array(
			'ugrup_idugrup' => '2'
		);
		$this->db->where('users_name',$users_name)->update($this->db_1,$baru);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function koorlama($users_name)
	{
		$lama = array(
			'ugrup_idugrup' => '5'
		);
		$this->db->where('users_name',$users_name)->update($this->db_1,$lama);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}