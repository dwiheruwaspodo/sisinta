<?php
class Model_koor_v_nilaisidang extends CI_Model {

    public $db_tabel    = 'tsemsid';
    public $per_halaman = 10;
    public $offset      = 0;

   
	public function cari_semua($offset)
	{
        if (is_null($offset) || empty($offset))
        {
            $this->offset = 0;
        }
        else
        {
            $this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
        }
        // $offset end
		$ses = $this->session->userdata('DP');
		return $this->db->select('mmhsw.mhsw_nama,
									skripsi.skrip_judul,
									tsemsid.nilai_bimb1,
									tsemsid.nilai_bimb2,
									tsemsid.nilai_uji,
									tsemsid.nilai_uji2,
									tsemsid.lockedit,
									tsemsid.idtSemSid,
									tsemsid.semsid_tgl')
								->from('
								mmhsw,
								skripsi,
								tsemsid,
								mkgtn
								')
								->where('mmhsw.idmmhsw = skripsi.idmMhsw')
								->where('skripsi.idmSkripsi = tsemsid.idmSkripsi')
								->where('mmhsw.idmProdi',$ses)
								->where('mkgtn.idmKgtn = tsemsid.idmKgtn')
								->where('tsemsid.lockedit = 0')
								->where('tsemsid.idmKgtn` = 3')
                        ->order_by('tsemsid.semsid_tgl', 'desc')
                        ->limit($this->per_halaman, $this->offset)
                        ->get()->result();
	}

    public function cari($idtSemSid)
    {
        return $this->db->where('idtSemSid', $idtSemSid)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($absen)
    {
        $this->load->library('table');

        // Buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

        /// Buat heading tabel
        $this->table->set_heading('Nama', 'Judul Skripsi', 'tanggal','Nilai Pembimbing 1', 'Nilai Pembimbing 2','Nilai Penguji 1','Nilai Penguji 2');

        foreach ($absen as $row)
        {
             $hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";

			$this->table->add_row(
				$row->mhsw_nama,
				$row->skrip_judul,
				$hr_tgl,
				$row->nilai_bimb1,
				$row->nilai_bimb2,
				$row->nilai_uji,
				$row->nilai_uji2,
				anchor('koor_v_nilaisidang/edit/'.$row->idtSemSid,'Oke',array('class'=> 'oyi', 'onclick'=>"return confirm('Anda yakin akan memverifikasi nilai sidang ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function paging($base_url)
    {
        $this->load->library('pagination');
        $config = array(
            'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
        );
        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

	public function hitung_semua()
	{
		$ses = $this->session->userdata('DP');
		return $this->db->select('mmhsw.mhsw_nama,
									skripsi.skrip_judul,
									tsemsid.nilai_bimb1,
									tsemsid.nilai_bimb2,
									tsemsid.nilai_uji,
									tsemsid.lockedit,
									tsemsid.idtSemSid,
									tsemsid.semsid_tgl')
								->from('
								mmhsw,
								skripsi,
								tsemsid,
								mkgtn
								')
								->where('mmhsw.idmProdi',$ses)
								->where('mmhsw.idmmhsw = skripsi.idmMhsw')
								->where('skripsi.idmSkripsi = tsemsid.idmSkripsi')
								->where('mkgtn.idmKgtn = tsemsid.idmKgtn')
								->where('tsemsid.lockedit = 0')
								->where('tsemsid.idmKgtn` = 3')
							->order_by('tsemsid.semsid_tgl', 'desc')
                            ->get()->num_rows();
	}

	public function edit($idtSemSid)
    {
        $nilai = array(
            'lockedit'=>1
        );

        // update db
        $this->db->where('idtSemSid', $idtSemSid)->update($this->db_tabel, $nilai);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

}
/* End of file absen_model.php */
/* Location: ./application/models/absen_model.php */