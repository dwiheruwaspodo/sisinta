<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_all_jadwal_seminar extends CI_model
{
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('NIM','Nama','Judul','Tanggal','Ruangan','Jam Mulai','Kegiatan');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->skrip_judul,
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				$row->kgtn_jenis
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
}

/* End of file model_all_jadwalan_seminar.php */
/* Location: ./application/model/model_all_jadwalan_seminar.php */