<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_adm_lupapasword extends CI_Model
{
	public $db_1 = 'users';
	public $offset = 0;
	public $per_halaman = 10;
	
	public function hapus_adm($adm)
	{
		$this->db->where('users_name',$adm)->delete($this->db_1);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function semua($offset)
	{
		if(is_null($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('users_name,user_nama,ugrup.ugrup_nama')
						->from('users,ugrup')
						->where('ugrup.idugrup = users.ugrup_idugrup')
						->limit($this->per_halaman,$this->offset)
						->get()
						->result();
	}
	
	public function cari($users_name)
	{
		return $this->db->where('users_name',$users_name)
						->limit(1)
						->get($this->db_1)
						->row();
	}
	
	public function tabel_lupapass($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Username','Nama Pengguna','Aksi');
		
		foreach($data as $row)
		{
			$this->table->add_row(
				$row->users_name,
				$row->user_nama,
				anchor('adm_lupapasword/normal/'.$row->users_name.'/'.$row->users_name,'reset',array('class' => 'pass','onclick'=>"return confirm('Anda yakin akan mengubah password user ini?')")).' '.
				anchor('adm_lupapasword/hapus/'.$row->users_name,'hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus user ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{	
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->select('*')
						->from($this->db_1)
						->get()
						->num_rows();
	}
	
	public function passbaru($users_name,$baru)
	{
		$baru = array(
			'users_passd' => md5($baru)
		);
		$this->db->where('users_name',$users_name)->update($this->db_1,$baru);
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function cari_user($limit,$offset,$user)
	{
		$cari_user = $this->db->query("select users_name,user_nama,ugrup.ugrup_nama
						from users,ugrup
						where ugrup.idugrup = users.ugrup_idugrup
						AND users.users_name like '%$user%'
						limit $offset,$limit						
						");
		return $cari_user;
	}
	
	public function total($user)
	{
		$cari_user = $this->db->query("select users_name,user_nama,ugrup.ugrup_nama
						from users,ugrup
						where ugrup.idugrup = users.ugrup_idugrup
						AND users.users_name like '%$user%'
						");
		return $cari_user;
	}
	
}