<?php if(!defined ('BASEPATH')) exit ('No direct script acces allowed');

class Model_MasterAfiliasi extends CI_Model
{
	public $db_tabel		= 'mafiliasi';
	public $per_halaman		= 10;
	public $offset			= 0;
	
	private function load_form_rules_tambah()
	{
		$form = array(
					array(
					'label'	=> 'Nama Instansi',
					'field'	=> 'mAfiliasi_nama',
					'rules'	=> 'required|max_length[45]'
					),
					
					array(
					'label'	=> 'Alamat',
					'field'	=> 'afiliasi_alamat',
					'rules'	=> 'required|max_length[200]'
					)
		);
		return $form;
	}
	
	private function load_form_edit()
	{
		$form = array(
					array(
					'label' => 'Nama Instansi',
					'field'	=> 'mAfiliasi_nama',
					'rules'	=> 'required|max_length[45]'
					),
					
					array(
					'label'	=> 'Alamat',
					'field'	=> 'afiliasi_alamat',
					'rules'	=> 'required|max_length[200]'
					)
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$form = $this->load_form_rules_tambah();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$form = $this->load_form_edit();
		$this->form_validation->set_rules($form);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function dd_afiliasi()
	{
		return $this->db->select('*')
						->from($this->db_tabel)
						->get()
						->result();
	}
	
	public function cari_semua($offset)
	{
		if (is_null($offset) || empty ($offset))
		{
			$this->offset=0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
						->from($this->db_tabel)
						->limit($this->per_halaman, $this->offset)
						->order_by('idmAfiliasi','ASC')
						->get()
						->result();
	}
	
	public function cari($idmAfiliasi)
	{
		return $this->db->where('idmAfiliasi', $idmAfiliasi)
				->limit(1)
				->get($this->db_tabel)
				->row();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		 $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

		$this->table->set_heading('ID','Nama Instansi','Alamat');
		foreach($data as $row)
		{
			$this->table->add_row(
			$row->idmAfiliasi,
			$row->mAfiliasi_nama,
			$row->afiliasi_alamat,
			anchor('master_afiliasi/edit/'.$row->idmAfiliasi,'Edit',array('class' => 'edit')).' '.
			anchor('master_afiliasi/hapus/'.$row->idmAfiliasi,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
				'base_url'			=> $base_url,
				'total_rows'		=> $this->hitung_semua(),
				'per_page'			=> $this->per_halaman,
				'num_links'			=> 2,
				'use_page_numbers'	=> TRUE,
	            'first_link'       => '&#124;&lt; First',
            	'last_link'        => 'Last &gt;&#124;',
	            'next_link'        => 'Next &gt;',
    	        'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitung_semua()
	{
		return $this->db->count_all($this->db_tabel);
	}
	
	public function tambah()
	{
		$afi = array(
			'mAfiliasi_nama'		=> $this->input->post('mAfiliasi_nama'),
			'afiliasi_alamat'	=> $this->input->post('afiliasi_alamat')
		);
		$this->db->insert($this->db_tabel, $afi);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idmAfiliasi)
	{
		$afi = array(
			'mAfiliasi_nama'	=> $this->input->post('mAfiliasi_nama'),
			'afiliasi_alamat'	=> $this->input->post('afiliasi_alamat')
		);
		
		$this->db->where('idmAfiliasi', $idmAfiliasi);
		$this->db->update($this->db_tabel, $afi);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idmAfiliasi)
	{
		$this->db->where('idmAfiliasi', $idmAfiliasi)->delete($this->db_tabel);
		
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}
/* End of file model_MasterAfiliasi.php */
/* Location: ./application/models/model_MasterAfiliasi.php */