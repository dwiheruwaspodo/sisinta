<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_koor_lihatruang extends CI_Controller
{
	public $offset = 0;
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('No','Hari','Ruang','Jam Mulai','Jam Selesai', 'Status');
		$no = 0 + $this->offset;
		foreach($data as $row)
		{
			$this->table->add_row(
			++$no,
			$row->SediaRuang_hari,
			$row->ruang_nama,
			$row->sesi_mulai,
			$row->sesi_selesai,
			$row->SediaRuang_stat
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
}

/* End of file model_koor_lihatruang.php */
/* Location: ./application/models/model_koor_lihatruang.php */