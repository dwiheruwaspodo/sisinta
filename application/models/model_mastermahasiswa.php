<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_mastermahasiswa extends CI_Model {

    public $db_tabel    = 'mmhsw';
	public $db_tabel2   = 'users';
    public $per_halaman = 10;
    public $offset      = 0;

    // rules form validasi, proses TAMBAH
    public function load_form_rules_tambah()
    {
        $form = array(
                        array(
                            'field' => 'mhsw_nim',
                            'label' => 'NIM',
                            'rules' => "required|exact_length[12]|numeric|is_unique[$this->db_tabel.mhsw_nim]"
                        ),
                        array(
                            'field' => 'mhsw_nama',
                            'label' => 'Nama',
                            'rules' => 'required|max_length[50]'
                        ),
                        array(
                            'field' => 'idmProdi',
                            'label' => 'prodi',
                            'rules' => 'required'
                        ),
						array(
                            'field' => 'mhsw_tlp',
                            'label' => 'Nama',
							'rules' => "numeric|max_length[12]"
                            
                        ),
        );
        return $form;
    }

    // rules form validasi, proses EDIT
    public function load_form_rules_edit()
    {
        $form = array(
            array(
                'field' => 'mhsw_nim',
                'label' => 'NIM',
                'rules' => 'required|numeric'
            ),
            array(
                'field' => 'mhsw_nama',
                'label' => 'Nama',
                'rules' => 'required|max_length[50]'
            ),
            array(
                'field' => 'idmProdi',
                'label' => 'Prodi',
                'rules' => 'required'
            ),
			array(
				'field' => 'mhsw_tlp',
				'label' => 'No.Tlp',
				'rules' => 'numeric|max_length[12]',
			),
			
        );
        return $form;
    }

    // jalankan proses validasi, untuk operasi TAMBAH
    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // jalankan proses validasi, untuk operasi EDIT
    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

	public function dd_mhs()
	{
		return $this->db->select('idmmhsw,mhsw_nama')
						->order_by('idmmhsw','DESC')
						->from($this->db_tabel)
						->get()
						->result();
	}
	
    public function cari_semua($offset)
	{
		if (is_null($offset) || empty($offset))
        {
            $this->offset = 0;
        }
        else
        {
            $this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
        }		
        // $offset end

        return $this->db->select('mmhsw.idmmhsw, mmhsw.mhsw_nim, mmhsw.mhsw_nama,mprodi.prodi_nama,mmhsw.mhsw_tlp')
                        ->from($this->db_tabel)
                        ->join('mprodi', 'mprodi.idmProdi = mmhsw.idmProdi')
                        ->limit($this->per_halaman, $this->offset)
                        ->order_by('idmmhsw', 'ASC')
                        ->get()
                        ->result();
	}

    public function cari($idmmhsw)
    {
        return $this->db->where('idmmhsw', $idmmhsw)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');
        $tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);

        // heading tabel
        $this->table->set_heading('No', 'Nama', 'NIM', 'Prodi', 'No Tlp','Aksi');

        // no urut data
        $no = 0 + $this->offset;

        foreach ($data as $row)
        {
            $this->table->add_row(
                ++$no,
                $row->mhsw_nama,
                $row->mhsw_nim,
                $row->prodi_nama,
				$row->mhsw_tlp,
                anchor('master_mahasiswa/edit/'.$row->idmmhsw,'Edit',array('class' => 'edit')).' '.
                anchor('master_mahasiswa/hapus/'.$row->idmmhsw,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'			=> $base_url,
			'total_rows'		=> $this->hitung_semua(),
			'per_page'			=> $this->per_halaman,
			'num_links'			=> 2,
			'use_page_numbers'	=> TRUE,
			'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}

    public function hitung_semua()
    {
        return $this->db->count_all($this->db_tabel);
    }

    public function tambah()
    {
        $siswa = array(
            'mhsw_nama' => $this->input->post('mhsw_nama'),
            'mhsw_nim' => $this->input->post('mhsw_nim'),
            'idmProdi' => $this->input->post('idmProdi'),
			'mhsw_tlp'	=> $this->input->post('mhsw_tlp'),
        );
        $this->db->insert($this->db_tabel, $siswa);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function tambah_user()
	{
		$yuser = array(
			'users_name' => $this->input->post('mhsw_nim'),
			'user_nama' => $this->input->post('mhsw_nama'),
			'users_passd' =>  md5($this->input->post('mhsw_nim')),
			'ugrup_idugrup' => 6
		);
		
		$this->db->insert($this->db_tabel2,$yuser);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

    public function edit($idmmhsw)
    {
        $siswa = array(
            'mhsw_nama'=>$this->input->post('mhsw_nama'),
            'mhsw_nim'=>$this->input->post('mhsw_nim'),
            'idmProdi' => $this->input->post('idmProdi'),
			'mhsw_tlp'	=> $this->input->post('mhsw_tlp'),
        );

        // update db
        $this->db->where('idmmhsw', $idmmhsw);
        $this->db->update($this->db_tabel, $siswa);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmmhsw)
    {
        $this->db->where('idmmhsw', $idmmhsw)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
	
	public function cari_mhs($limit,$offset,$nama)
	{
		$mhs = $this->db->query("
		select mmhsw.idmmhsw, mmhsw.mhsw_nim, mmhsw.mhsw_nama,mprodi.prodi_nama,mmhsw.mhsw_tlp
		from mmhsw,mprodi
		where mprodi.idmProdi = mmhsw.idmProdi
		AND mmhsw.mhsw_nama like '%$nama%'
		limit $offset,$limit
		");
		return $mhs;
	}
	
	public function total($nama)
	{
		$mhs = $this->db->query("
		select mmhsw.idmmhsw, mmhsw.mhsw_nim, mmhsw.mhsw_nama,mprodi.prodi_nama,mmhsw.mhsw_tlp
		from mmhsw,mprodi
		where mprodi.idmProdi = mmhsw.idmProdi
		AND mmhsw.mhsw_nama like '%$nama%'
		");
		return $mhs;
	}
}
/* End of file siswa_model.php */
/* Location: ./application/models/siswa_model.php */