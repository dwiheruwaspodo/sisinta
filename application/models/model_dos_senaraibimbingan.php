<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_dos_senaraibimbingan extends CI_Model
{
	public $db_tabel = 'view_katalog';
	public $offset = 0;
	public $per_halaman = 10;
	
	public function p1($offset)
	{
		$dos = $this->session->userdata('users_name');
		
		if(is_null($offset)|| empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
					->from($this->db_tabel)
					->where('nip_p1',$dos)
					->limit($this->per_halaman,$this->offset)
					->order_by('tgl_reg','desc')
					->get()
					->result();
	}
	
	public function p2($offset)
	{
		$dos = $this->session->userdata('users_name');
		
		if(is_null($offset)|| empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}
		
		return $this->db->select('*')
					->from($this->db_tabel)
					->where('nip_p2',$dos)
					->limit($this->per_halaman,$this->offset)
					->order_by('tgl_reg','desc')
					->get()
					->result();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		$this->table->set_heading('NIM','Nama','Prodi','Judul','Tahapan','Status Skripsi');

		foreach($data as $row)
		{
			$this->table->add_row(
				$row->nim,
				$row->nama,
				$row->prodi,
				$row->skrip_judul,
				$row->sproses,
				$row->sskripsi,
				anchor('dos_senaraibimbingan/lihat_catatan_anak/'.$row->id,'Logbook',array('class'=> 'buku'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabelkaj($data)
	{
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		$this->table->set_heading('NIM','Nama','Prodi','Judul','Tahapan','Status Skripsi');

		foreach($data as $row)
		{
			$this->table->add_row(
				$row->nim,
				$row->nama,
				$row->prodi,
				$row->skrip_judul,
				$row->sproses,
				$row->sskripsi,
				anchor('doskaj_senaraibimbingan/lihat_catatan_anak/'.$row->id,'Logbook',array('class'=> 'buku'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function buat_tabelkoor($data)
	{
		$this->load->library('table');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		$this->table->set_heading('NIM','Nama','Prodi','Judul','Tahapan','Status Skripsi');

		foreach($data as $row)
		{
			$this->table->add_row(
				$row->nim,
				$row->nama,
				$row->prodi,
				$row->skrip_judul,
				$row->sproses,
				$row->sskripsi,
				anchor('doskoor_senaraibimbingan/lihat_catatan_anak/'.$row->id,'Logbook',array('class'=> 'buku'))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	
	public function paging1($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitungs_semua1(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitungs_semua1()
	{
	$dos = $this->session->userdata('users_name');
		return $this->db->select('*')
					->from($this->db_tabel)
					->where('nip_p1',$dos)
					->get()
					->num_rows();
	}
	
	public function paging2($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitungs_semua2(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 2,
            'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function hitungs_semua2()
	{
	$dos = $this->session->userdata('users_name');
		return $this->db->select('*')
					->from($this->db_tabel)
					->where('nip_p2',$dos)
					->get()
					->num_rows();
	}
	
	public function cari_mhs1($limit,$offset,$nama)
	{
		$dos = $this->session->userdata('users_name');
		$cari_mhs1 = $this->db->query("select *
						from view_katalog
						where nip_p1 = $dos
						AND nama like '%$nama%'
						limit $offset,$limit						
						");
		return $cari_mhs1;
	}
	
	public function total($nama)
	{
		$dos = $this->session->userdata('users_name');
		$cari_mhs1 = $this->db->query("select *
						from view_katalog
						where nip_p1 = $dos
						AND nama like '%$nama%'
						");
		return $cari_mhs1;
	}
	
	public function cari_mhs2($limit,$offset,$nama)
	{
		$dos = $this->session->userdata('users_name');
		$cari_mhs1 = $this->db->query("select *
						from view_katalog
						where nip_p2 = $dos
						AND nama like '%$nama%'
						limit $offset,$limit						
						");
		return $cari_mhs1;
	}
	
	public function total2($nama)
	{
		$dos = $this->session->userdata('users_name');
		$cari_mhs1 = $this->db->query("select *
						from view_katalog
						where nip_p2 = $dos
						AND nama like '%$nama%'
						");
		return $cari_mhs1;
	}
	
}


/* End of file model_dos_senaraibimbingan.php */
/* Location: ./application/models/model_dos_senaraibimbingan.php */