<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Model_koor_penjadwalansidang extends CI_Model
{
	public $db_tabel = 'tsemsid';
	public $db_tabel2 = 'skripsi';
	public $offset = 0;
	public $per_halaman = 10;
	
	public function cari_semua($offset)
	{
		if(is_null ($offset) || empty($offset))
		{
			$this->offset = 0;
		}
		else
		{
			$this->offset = ($offset * $this->per_halaman) - $this->per_halaman;
		}

		return $this->db->select('tsemsid.idtSemSid,mmhsw.mhsw_nim, mmhsw.mhsw_nama, tsemsid.semsid_tgl, skripsi.skrip_judul, mkgtn.kgtn_jenis, mruang.ruang_nama, msesi.sesi_mulai,a.dos_nama AS P1,b.dos_nama AS P2,c.dos_nama AS PU,d.dos_nama as PU2')
						->from('mmhsw, msesi, skripsi, tsemsid, mkgtn, mruang, mdosen a, mdosen b, mdosen c, mdosen d')
						->where('tsemsid.idmSkripsi = skripsi.idmSkripsi')
						->where('a.idmDosen = skripsi.idmDosen_Bi1')
						->where('b.idmDosen = skripsi.idmDosen_Bi2')
						->where('c.idmDosen = tsemsid.idmdosen_uji')
						->where('d.idmDosen = tsemsid.idmdosen_uji2')
						->where('tsemsid.idmSesi = msesi.idmSesi')
						->where('tsemsid.idmKgtn = mkgtn.idmKgtn')
						->where('tsemsid.idmRuang = mruang.idmRuang')
						->where('mmhsw.idmmhsw = skripsi.idmMhsw')
						->where('tsemsid.idmKgtn = 3')
						->where('tsemsid.semsid_tgl > now()')
						->limit($this->per_halaman, $this->offset)
						->order_by('semsid_tgl','DESC')
						->get()
						->result();
	}
	
	public function hitung_semua()
	{

		return $this->db->select('tsemsid.idtSemSid,mmhsw.mhsw_nim, mmhsw.mhsw_nama, tsemsid.semsid_tgl, skripsi.skrip_judul, mkgtn.kgtn_jenis, mruang.ruang_nama, msesi.sesi_mulai,a.dos_nama AS P1,b.dos_nama AS P2,c.dos_nama AS PU,d.dos_nama as PU2')
						->from('mmhsw, msesi, skripsi, tsemsid, mkgtn, mruang, mdosen a, mdosen b, mdosen c, mdosen d')
						->where('tsemsid.idmSkripsi = skripsi.idmSkripsi')
						->where('a.idmDosen = skripsi.idmDosen_Bi1')
						->where('b.idmDosen = skripsi.idmDosen_Bi2')
						->where('c.idmDosen = tsemsid.idmdosen_uji')
						->where('d.idmDosen = tsemsid.idmdosen_uji2')												
						->where('tsemsid.idmSesi = msesi.idmSesi')
						->where('tsemsid.idmKgtn = mkgtn.idmKgtn')
						->where('tsemsid.idmRuang = mruang.idmRuang')
						->where('mmhsw.idmmhsw = skripsi.idmMhsw')
						->where('tsemsid.idmKgtn = 3')
						->where('tsemsid.semsid_tgl > now()')
						->get()
						->num_rows();
	}
	
	public function buat_tabel($data)
	{
		$this->load->library('table');
		$this->table->set_heading('Nim','Nama','Judul','Pembimbing 1','Pembimbing 2','Tanggal','Ruang','Jam','Penguji','Penguji 2');
		$tmpl = array('row_alt_start'  => '<tr class="zebra">');
        $this->table->set_template($tmpl);
		
		foreach($data as $row)
		{
			$hari_array = array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu');
            $hr = date('w', strtotime($row->semsid_tgl));
            $hari = $hari_array[$hr];
            $tgl = date('d-m-Y', strtotime($row->semsid_tgl));
            $hr_tgl = "$hari, $tgl";
			
			$this->table->add_row(
				$row->mhsw_nim,
				$row->mhsw_nama,
				$row->skrip_judul,
				$row->P1,
				$row->P2,
				$hr_tgl,
				$row->ruang_nama,
				$row->sesi_mulai,
				$row->PU,
				$row->PU2,
				anchor('koor_penjadwalansidang/edit/'.$row->idtSemSid, 'Edit',array('class' => 'edit')).''.
				anchor('koor_penjadwalansidang/hapus/'.$row->idtSemSid, 'Hapus', array('class' => 'delete','onclick' => "return confirm('Anda yakin akan menghapus data ini?')"))
			);
		}
		$tabel = $this->table->generate();
		return $tabel;
	}
	
	public function paging($base_url)
	{
		$this->load->library('pagination');
		$config = array(
			'base_url'         => $base_url,
            'total_rows'       => $this->hitung_semua(),
            'per_page'         => $this->per_halaman,
            'num_links'        => 4,			
			'use_page_numbers' => TRUE,
            'first_link'       => '&#124;&lt; First',
            'last_link'        => 'Last &gt;&#124;',
            'next_link'        => 'Next &gt;',
            'prev_link'        => '&lt; Prev',
		);
		$this->pagination->initialize($config);
		return $this->pagination->create_links();
	}
	
	public function cari($idtSemSid)
	{
		return $this->db->select('tsemsid.idtSemSid, 
							mmhsw.mhsw_nama,
							tsemsid.semsid_tgl,
							skripsi.skrip_judul,
							mkgtn.kgtn_jenis,
							mruang.ruang_nama,
							msesi.sesi_mulai')
						->from('mmhsw, msesi, skripsi, tsemsid, mkgtn, mruang')
						->where('tsemsid.idmSkripsi = skripsi.idmSkripsi')
						->where('tsemsid.idmSesi = msesi.idmSesi')
						->where('tsemsid.idmKgtn = mkgtn.idmKgtn')
						->where('tsemsid.idmRuang = mruang.idmRuang')
						->where('mmhsw.idmmhsw = skripsi.idmMhsw')
						->where('tsemsid.idmKgtn = 3')
						->get()
						->row();
	}
	
	public function load_rules_tambah()
	{
		$form = array(
			array(
				'field' => 'semsid_tgl',
				'label' => 'tanggal',
				'rules' => 'required|callback_is_format_tanggal'
			),
			array(
				'field' => 'idmSkripsi',
				'label' => 'Nama',
				'rules' => 'required'
			),
			array(
				'field' => 'idmSesi',
				'label' => 'Jam Mulai',
				'rules' => 'required'
			),
			array(
				'field' => 'idmRuang',
				'label' => 'Ruangan',
				'rules' => 'required'
			),
			array(
				'field' => 'idmdosen_uji',
				'label' => 'Penguji 1',
				'rules' => 'required'
			),
			array(
				'field' => 'idmdosen_uji2',
				'label' => 'Penguji 2',
				'rules' => 'required'
			)
		);
		return $form;
	}
	
	public function load_rules_edit()
	{
		$form = array(
			array(
				'field' => 'semsid_tgl',
				'label' => 'tanggal',
				'rules' => 'required|callback_is_format_tanggal'
			),
			array(
				'field' => 'idmSesi',
				'label' => 'Jam Mulai',
				'rules' => 'required'
			),
			array(
				'field' => 'idmRuang',
				'label' => 'Ruangan',
				'rules' => 'required'
			),
			array(
				'field' => 'idmdosen_uji',
				'label' => 'Penguji 1',
				'rules' => 'required'
			),
			array(
				'field' => 'idmdosen_uji2',
				'label' => 'Penguji 2',
			)
		);
		return $form;
	}
	
	public function validasi_tambah()
	{
		$tambah = $this->load_rules_tambah();
		$this->form_validation->set_rules($tambah);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function validasi_edit()
	{
		$edit = $this->load_rules_edit();
		$this->form_validation->set_rules($edit);
		
		if($this->form_validation->run())
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function tambah()
	{
		$tambah = array(
			'semsid_tgl' => date('Y-m-d', strtotime($this->input->post('semsid_tgl'))),
			'idmSkripsi' => $this->input->post('idmSkripsi'),
			'idmKgtn'	=> 3,
			'idmSesi'	=> $this->input->post('idmSesi'),
			'idmRuang'	=> $this->input->post('idmRuang'),
			'lockedit'	=> 0,
			'idmdosen_uji' => $this->input->post('idmdosen_uji'),
			'idmdosen_uji2' => $this->input->post('idmdosen_uji2'),
		);
		
		$this->db->insert($this->db_tabel, $tambah);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit_skripsi($dor)
	{
		$update = array(
			'idmStatusPros' => 6
		);
		$this->db->where('idmSkripsi',$dor)->update($this->db_tabel2,$update);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function edit($idtSemSid)
	{
		$edit = array(
			'semsid_tgl' => date('Y-m-d',strtotime($this->input->post('semsid_tgl'))),
			'idmSesi'	=> $this->input->post('idmSesi'),
			'idmRuang'	=> $this->input->post('idmRuang'),
			'idmdosen_uji' => $this->input->post('idmdosen_uji'),
			'idmdosen_uji2' => $this->input->post('idmdosen_uji2'),
		);
		
		$this->db->where('idtSemSid', $idtSemSid)->update($this->db_tabel, $edit);
		
		if($this->db->affected_rows() > 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function hapus($idtSemSid)
	{
		$this->db->where('idtSemSid', $idtSemSid)->delete($this->db_tabel);
		
		if($this->db->affected_rows()> 0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
	public function dd_mhs()
	{
		return $this->db->select('skripsi.idmSkripsi,
								mmhsw.mhsw_nama
								')
						->from ('mmhsw,
								skripsi')
						->where ('skripsi.idmMhsw = mmhsw.idmmhsw')
						->where ('skripsi.idmStatuspros = 5')
						->get()
						->result();
	}
	
	public function dd_ruang()
	{
		$t = 'tersedia';
		return $this->db->select('mruang.idmRuang,mruang.ruang_nama,msesi.sesi_mulai')
						->from('sediaruanghari,mruang,msesi')
						->where('sediaruanghari.idmRuang = mruang.idmRuang')
						->where('sediaruanghari.SediaRuang_stat',$t)
						->where('sediaruanghari.idmSesi_awal = msesi.idmSesi')
						->get()
						->result();
	}
	
	public function ruang_khusus()
	{
		return $this->db->select('mruang.idmRuang,mruang.ruang_nama')
						->from('sediaruangtanggal, mruang')
						->where('sediaruangtanggal.idmRuang = mruang.idmRuang')
						->get()
						->result();
	}
	
	public function get_idSk($id)
	{
		return $this->db->select('idmSkripsi')
						->from($this->db_tabel)
						->where('idtSemSid',$id)
						->get()
						->row()->idmSkripsi;
	}
	
	public function batal($dor)
	{
		$update = array(
			'idmStatusPros'=> 5,
		);
		$this->db->where('idmSkripsi',$dor)->update($this->db_tabel2,$update);
		if($this->db->affected_rows()>0)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
	
}
/* End of file model_admpenjadwalansidang.php */
/* Location: ./application/models/model_admpenjadwalansidang.php */