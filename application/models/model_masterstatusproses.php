<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Model_masterstatusproses extends CI_Model {

    public $db_tabel    = 'mstatuspros';
    
	public function __construct()
	{
		parent::__construct();
	}

    // rules form validasi, proses TAMBAH
    private function load_form_rules_tambah()
    {
        $form = array(
                        array(
                            'field' => 'idmstatus_pros',
                            'label' => 'ID',
                            'rules' => "required|numeric|is_unique[$this->db_tabel.idmstatus_pros]"
                        ),
                        array(
                            'field' => 'statuspros_tipe',
                            'label' => 'Status Proses',
                            'rules' => 'required|max_length[50]'
                        ),
        );
        return $form;
    }

    // rules form validasi, proses EDIT
    private function load_form_rules_edit()
    {
        $form = array(
			
            array(
                'field' => 'idmstatus_pros',
                'label' => 'ID',
                'rules' => "required|numeric"
            ),
            array(
                'field' => 'statuspros_tipe',
                'label' => 'Status Proses',
                'rules' => 'required|max_length[50]'
            ),
        );
        return $form;
    }

    // jalankan proses validasi, untuk operasi TAMBAH
    public function validasi_tambah()
    {
        $form = $this->load_form_rules_tambah();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    // jalankan proses validasi, untuk operasi EDIT
    public function validasi_edit()
    {
        $form = $this->load_form_rules_edit();
        $this->form_validation->set_rules($form);

        if ($this->form_validation->run())
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

     public function cari_semua()
    {
        return $this->db->select('*')
						->from($this->db_tabel)
                        ->get()
                        ->result();
    }


    public function cari($idmstatus_pros)
    {
        return $this->db->where('idmstatus_pros', $idmstatus_pros)
            ->limit(1)
            ->get($this->db_tabel)
            ->row();
    }

    public function buat_tabel($data)
    {
        $this->load->library('table');

        // buat class zebra di <tr>,untuk warna selang-seling
        $tmpl = array('row_alt_start'  => '<tr class="bg-yellow">');
        $this->table->set_template($tmpl);

        // heading tabel
        $this->table->set_heading('ID','Status Proses', 'Aksi');

        foreach ($data as $row)
        {
            $this->table->add_row(
				$row->idmstatus_pros,
                $row->statuspros_tipe,
                anchor('master_statusproses/edit/'.$row->idmstatus_pros, 'Edit',array('class' => 'edit')).' '.
                anchor('master_statusproses/hapus/'.$row->idmstatus_pros,'Hapus',array('class' => 'delete','onclick'=>"return confirm('Anda yakin akan menghapus data ini?')"))
            );
        }
        $tabel = $this->table->generate();

        return $tabel;
    }

    public function tambah()
    {
        $st = array(
			'idmstatus_pros' => $this->input->post('idmstatus_pros'),
            'statuspros_tipe' => $this->input->post('statuspros_tipe'),
        );
        $this->db->insert($this->db_tabel, $st);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function edit($idmstatus_pros)
    {
        $st = array(
			'idmstatus_pros' => $this->input->post('idmstatus_pros'),
            'statuspros_tipe' => $this->input->post('statuspros_tipe'),
        );
        // update db
        $this->db->where('idmstatus_pros', $idmstatus_pros);
        $this->db->update($this->db_tabel, $st);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function hapus($idmstatus_pros)
    {
        $this->db->where('idmstatus_pros', $idmstatus_pros)->delete($this->db_tabel);

        if($this->db->affected_rows() > 0)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}
/* End of file model_MasterStatusProses.php */
/* Location: ./application/models/model_MasterStatusProses.php */