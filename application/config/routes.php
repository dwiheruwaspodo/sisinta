<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/


$route['default_controller'] = "login";

$route['katalog/halaman'] = "semuabisalihat/index";
$route['katalog/halaman/(:num)'] = "semuabisalihat/index/$1";

$route['404_override'] = 'error404';

$route['mmahasiswa/halaman'] = "master_mahasiswa/index";
$route['mmahasiswa/halaman/(:num)'] = "master_mahasiswa/index/$1";

$route['mdosen/halaman'] = "master_dosen/index";
$route['mdosen/halaman/(:num)'] = "master_dosen/index/$1";

$route['mafiliasi/halaman'] = "master_afiliasi/index";
$route['mafiliasi/halaman/(:num)'] = "master_afiliasi/index/$1";

$route['ruangan/halaman'] = "master_ruang/index";
$route['ruangan/halaman/(:num)'] = "master_ruang/index/$1";

$route['adm_sediaruang/halaman'] = "adm_sediaruang/index";
$route['adm_sediaruang/halaman/(:num)'] = "adm_sediaruang/index/$1";

$route['adm_sediaruangkhusus/halaman'] = "adm_sediaruangkhusus/index";
$route['adm_sediaruangkhusus/halaman/(:num)'] = "adm_sediaruangkhusus/index/$1";

$route['beban_dosen/halaman'] = "koor_lihatbeban/index";
$route['beban_dosen/halaman/(:num)'] = "koor_lihatbeban/index/$1";

$route['seminar/halaman'] = "koor_penjadwalanseminar/index";
$route['seminar/halaman/(:num)'] = "koor_penjadwalanseminar/index/$1";

$route['sidang/halaman'] = "koor_penjadwalansidang/index";
$route['sidang/halaman/(:num)'] = "koor_penjadwalansidang/index/$1";

$route['adm_seminar/halaman'] = "adm_penjadwalanseminar/index";
$route['adm_seminar/halaman/(:num)'] = "adm_penjadwalanseminar/index/$1";

$route['ketersediaan_ruangan/halaman'] = "koor_lihatruang/index";
$route['ketersediaan_ruangan/halaman/(:num)'] = "koor_lihatruang/index/$1";

$route['ruangkhusus/halaman'] = "koor_lihatruangkhusus/index";
$route['ruangkhusus/halaman/(:num)'] = "koor_lihatruangkhusus/index/$1";

$route['agendadosen/halaman'] = "koor_lihatagendadosen/index";
$route['agendadosen/halaman/(:num)'] = "koor_lihatagendadosen/index/$1";

$route['dataskripsivalid/halaman'] = "koor_dataskripsi/yangsudahvalid";
$route['dataskripsivalid/halaman/(:num)'] = "koor_dataskripsi/yangsudahvalid/$1";

$route['dataskripsibaru/halaman'] = "koor_dataskripsi/barumasuk";
$route['dataskripsibaru/halaman/(:num)'] = "koor_dataskripsi/barumasuk/$1";

$route['mahasiswa_bimbingan/halaman'] = "dos_senaraibimbingan/p1";
$route['mahasiswa_bimbingan/halaman/(:num)'] = "dos_senaraibimbingan/p1/$1";

$route['mahasiswa_bimbingan2/halaman'] = "dos_senaraibimbingan/p2";
$route['mahasiswa_bimbingan2/halaman/(:num)'] = "dos_senaraibimbingan/p2/$1";

$route['mahasiswa_bimbingankj/halaman'] = "doskaj_senaraibimbingan/p1";
$route['mahasiswa_bimbingankj/halaman/(:num)'] = "doskaj_senaraibimbingan/p1/$1";

$route['mahasiswa_bimbingankj2/halaman'] = "doskaj_senaraibimbingan/p2";
$route['mahasiswa_bimbingankj2/halaman/(:num)'] = "doskaj_senaraibimbingan/p2/$1";

$route['mahasiswa_bimbingank1/halaman'] = "doskoor_senaraibimbingan/p1";
$route['mahasiswa_bimbingank1/halaman/(:num)'] = "doskoor_senaraibimbingan/p1/$1";

$route['mahasiswa_bimbingank2/halaman'] = "doskoor_senaraibimbingan/p2";
$route['mahasiswa_bimbingank2/halaman/(:num)'] = "doskoor_senaraibimbingan/p2/$1";

$route['agenda/halaman'] = "dos_lihatagenda/index";
$route['agenda/halaman/(:num)'] = "dos_lihatagenda/index/$1";

$route['agenda_k/halaman'] = "dos_lihatagenda_koor/index";
$route['agenda_k/halaman/(:num)'] = "dos_lihatagenda_koor/index/$1";

$route['agenda_kj/halaman'] = "dos_lihatagenda_kaj/index";
$route['agenda_kj/halaman/(:num)'] = "dos_lihatagenda_kaj/index/$1";

$route['afiliasi_mahasiswa/halaman'] = "adm_afiliasi/index";
$route['afiliasi_mahasiswa/halaman/(:num)'] = "adm_afiliasi/index/$1";

$route['ubah_status/halaman'] = "adm_ubahstatus/index";
$route['ubah_status/halaman/(:num)'] = "adm_ubahstatus/index/$1";

$route['nilaisidang/halaman'] = "adm_nilaisidang/index";
$route['nilaisidang/halaman/(:num)'] = "adm_nilaisidang/index/$1";

$route['nilaiseminar/halaman'] = "adm_nilaiseminar/index";
$route['nilaiseminar/halaman/(:num)'] = "adm_nilaiseminar/index/$1";

$route['vnilaiseminar/halaman'] = "koor_v_nilaiseminar/index";
$route['vnilaiseminar/halaman/(:num)'] = "koor_v_nilaiseminar/index/$1";

$route['vnilaisidang/halaman'] = "koor_v_nilaisidang/index";
$route['vnilaisidang/halaman/(:num)'] = "koor_v_nilaisidang/index/$1";

$route['koorbaru/halaman'] = "adm_hakakses/index";
$route['koorbaru/halaman/(:num)'] = "adm_hakakses/index/$1";

$route['tambah_koorbaru/halaman'] = "adm_hakakses/kandidat_koor";
$route['tambah_koorbaru/halaman/(:num)'] = "adm_hakakses/kandidat_koor/$1";

$route['kajurbaru/halaman'] = "adm_hakakses_kajur/index";
$route['kajurbaru/halaman/(:num)'] = "adm_hakakses_kajur/index/$1";

$route['logbook_dosen/halaman'] = "catatan_bimb_dos/index";
$route['logbook_dosen/halaman/(:num)'] = "catatan_bimb_dos/index/$1";

$route['logbook_dosenkj/halaman'] = "catatan_bimb_kaj/index";
$route['logbook_dosenkj/halaman/(:num)'] = "catatan_bimb_kaj/index/$1";

$route['logbook_dosenkoo/halaman'] = "catatan_bimb_koor/index";
$route['logbook_dosenkoo/halaman/(:num)'] = "catatan_bimb_koor/index/index/$1";

$route['all_katalogskripsi/halaman'] = "all_katalogskripsi/index";
$route['all_katalogskripsi/halaman/(:num)'] = "all_katalogskripsi/index/$1";

$route['pencarianjudul1/halaman'] = "all_katalogskripsi_adm/index";
$route['pencarianjudul1/halaman/(:num)'] = "all_katalogskripsi_adm/index/$1";

$route['all_katalogskripsi1/halaman'] = "all_katalogskripsi_adm/index";
$route['all_katalogskripsi1/halaman/(:num)'] = "all_katalogskripsi_adm/index/$1";

$route['all_katalogskripsi2/halaman'] = "all_katalogskripsi_koor/index";
$route['all_katalogskripsi2/halaman/(:num)'] = "all_katalogskripsi_koor/index/$1";

$route['all_katalogskripsi3/halaman'] = "all_katalogskripsi_dos/index";
$route['all_katalogskripsi3/halaman/(:num)'] = "all_katalogskripsi_dos/index/$1";

$route['all_katalogskripsi4/halaman'] = "all_katalogskripsi_kaj/index";
$route['all_katalogskripsi4/halaman/(:num)'] = "all_katalogskripsi_kaj/index/$1";

$route['jadwal_seminar/halaman'] = "all_jadwal_seminar/index";
$route['jadwal_seminar/halaman/(:num)'] = "all_jadwal_seminar/index/$1";

$route['jadwal_seminar_a/halaman'] = "all_jadwal_seminar_adm/index";
$route['jadwal_seminar_a/halaman/(:num)'] = "all_jadwal_seminar_adm/index/$1";

$route['jadwal_seminar_kj/halaman'] = "all_jadwal_seminar_kaj/index";
$route['jadwal_seminar_kj/halaman/(:num)'] = "all_jadwal_seminar_kaj/index/$1";

$route['jadwal_seminar_k/halaman'] = "all_jadwal_seminar_koor/index";
$route['jadwal_seminar_k/halaman/(:num)'] = "all_jadwal_seminar_koor/index/$1";

$route['jadwal_seminar_d/halaman'] = "all_jadwal_seminar_dos/index";
$route['jadwal_seminar_d/halaman/(:num)'] = "all_jadwal_seminar_dos/index/$1";

$route['jadwal_sidang/halaman'] = "all_jadwal_sidang/index";
$route['jadwal_sidang/halaman/(:num)'] = "all_jadwal_sidang/index/$1";

$route['jadwal_sidang_a/halaman'] = "all_jadwal_sidang_adm/index";
$route['jadwal_sidang_a/halaman/(:num)'] = "all_jadwal_sidang_adm/index/$1";

$route['jadwal_sidang_kj/halaman'] = "all_jadwal_sidang_kaj/index";
$route['jadwal_sidang_kj/halaman/(:num)'] = "all_jadwal_sidang_kaj/index/$1";

$route['jadwal_sidang_k/halaman'] = "all_jadwal_sidang_koor/index";
$route['jadwal_sidang_k/halaman/(:num)'] = "all_jadwal_sidang_koor/index/$1";

$route['jadwal_sidang_d/halaman'] = "all_jadwal_sidang_dos/index";
$route['jadwal_sidang_d/halaman/(:num)'] = "all_jadwal_sidang_dos/index/$1";

$route['lupapass/halaman'] = "adm_lupapasword/index";
$route['lupapass/halaman/(:num)'] = "adm_lupapasword/index/$1";


$route['logout'] = "login/logout";

/* End of file routes.php */
/* Location: ./application/config/routes.php */