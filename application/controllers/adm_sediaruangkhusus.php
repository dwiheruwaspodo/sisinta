<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class Adm_sediaruangkhusus extends MY_Controller
{
	public $data = array(
			 'breadcrumb'    => 'Ruang Khusus',
             'pesan'         => '',
             'pagination'    => '',
             'tabel_data'    => '',
             'main_view'     => 'adm_sedia_ruang_khusus/sedia_ruang',
             'form_action'   => '',
             'form_value'    => '',
             'option_mulai'  => '',
			 'option_selesai'=> '',
			 'option_ruang'	 => '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_admsediaruangkhusus','sedia',TRUE);
		$this->load->model('model_mastersesi','sesi',TRUE);
		$this->load->model('model_masterruang','ruang',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('tanggal_sekarang','');
		
		$ruang = $this->sedia->cari_semua($offset);
		
		if($ruang)
		{
			$tabel = $this->sedia->buat_tabel($ruang);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->sedia->paging(site_url('adm_sediaruangkhusus/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data tersedia';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb']	= 'Ruangan Khusus > Tambah';
		$this->data['main_view']	= 'adm_sedia_ruang_khusus/sedia_ruang_form';
		$this->data['form_action']	= 'adm_sediaruangkhusus/tambah';
		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['option_ruang'][$row->idmRuang] = $row->ruang_nama;
			}
		}
		else
		{
			$this->data['option_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}
////////////////////////////////////////
		
		$mulai = $this->sesi->cari_semua();
		if($mulai)		
		{
			foreach($mulai as $row)
			{
				$this->data['option_mulai'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['option_mulai'][00]='-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
////////////////////////////////////////
		
		$selesai = $this->sesi->cari_semua();
		if($selesai)
		{
			foreach($selesai as $row)
			{
				$this->data['option_selesai'][$row->idmSesi] = $row->sesi_selesai;
			}
		}
		else
		{
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
			$this->data['option_selesai'][00] = '-';
		}
/////////////////////////////////////////
		if($this->input->post('submit'))
		{
			if($this->sedia->validasi_tambah())
			{
				if($this->sedia->tambah())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil');
					redirect('adm_sediaruangkhusus');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm', $this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function edit($idSediaRuangTanggal = NULL)
	{
		$this->data['breadcrumb']	= 'Ruang Khusus > Edit';
		$this->data['main_view']	= 'adm_sedia_ruang_khusus/sedia_ruang_form';
		$this->data['form_action']	= 'adm_sediaruangkhusus/edit/'. $idSediaRuangTanggal;
		
		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['option_ruang'][$row->idmRuang] = $row->ruang_nama;
			}
		}
		else
		{
			$this->data['option_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}
////////////////////////////////////////
		
		$mulai = $this->sesi->cari_semua();
		if($mulai)		
		{
			foreach($mulai as $row)
			{
				$this->data['option_mulai'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['option_mulai'][00]='-';
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
		}
////////////////////////////////////////
		
		$selesai = $this->sesi->cari_semua();
		if($selesai)
		{
			foreach($selesai as $row)
			{
				$this->data['option_selesai'][$row->idmSesi] = $row->sesi_selesai;
			}
		}
		else
		{
			$this->data['pesan'] = 'Data sesi tidak tersedia. Silahkan isi dahulu data sesi';
			$this->data['option_selesai'][00] = '-';
		}
/////////////////////////////////////////
		
		if(!empty ($idSediaRuangTanggal))
		{
			if($this->input->post('submit'))
			{
				if($this->sedia->validasi_edit() === TRUE)
				{
					$this->sedia->edit($idSediaRuangTanggal);
					$this->session->set_flashdata('pesan','Proses update data berhasil');
					redirect('adm_sediaruangkhusus');
					
				}
				else
				{
					$this->load->view('template_adm', $this->data);
				}
			}
			else
			{
				$update = $this->sedia->cari($idSediaRuangTanggal);
				foreach($update as $key =>$value)
				{
					$this->data['form_value'][$key] = $value;
				}
				
				$tgl = $this->data['form_value']['srt_tgl'];
                 $this->data['form_value']['srt_tgl'] = date('d-m-Y', strtotime($tgl));

                // set temporary data for edit
                $this->session->set_userdata('tanggal_sekarang', $update->srt_tgl);

				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			redirect('adm_sediaruangkhusus');
		}
	}
	
	public function hapus($idSediaRuangTanggal = NULL)
	{
		if(!empty($idSediaRuangTanggal))
		{
			if($this->sedia->hapus($idSediaRuangTanggal))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil');
				redirect('adm_sediaruangkhusus');
			}
			else
			{
				$this->session->set_flasdata('pesan','Proses hapus data gagal');
				redirect('adm_sediaruangkhusus');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses hapus data gagal');
			redirect('adm_sediaruangkhusus');
		}
	}
	 
	 public function is_format_tanggal($str)
    {
        if( ! preg_match('/(0[1-9]|1[0-9]|2[0-9]|3[01])-(0[1-9]|1[012])-([0-9]{4})/', $str))
        {
            $this->form_validation->set_message('is_format_tanggal', 'Format tanggal tidak valid. (dd-mm-yyyy)');
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

	
}

/* End of file admsediaruangkhusus.php */
/* Location: ./application/controller/admsediaruangkhusus.php */