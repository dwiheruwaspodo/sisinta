<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_download extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array("html","form","url","text"));
		$this->load->model('model_upload','model',TRUE);
	}
	
	public function index()
	{
		$data['breadcrumb'] = "Periksa Syarat Seminar";
		$data['main_view'] = 'download/v_download';
		$untuk_adm = $this->model->baru_masuk();
		if($untuk_adm)
		{
			$data['tabel_data'] = $this->model->tabel_adm($untuk_adm);
		}
		else
		{
			$data['pesan'] = 'Belum ada syarat yang terkumpul';
		}
		
		$this->load->view("template_koor",$data);
	}
	
	public function yg_sudahdidonlod()
	{
		$data['breadcrumb'] = "Verifikasi Berkas";
		$data['main_view'] = 'download/v_download';
		$untuk_adm = $this->model->sudah_didonload();
		if($untuk_adm)
		{
			$data['tabel_data'] = $this->model->tabelsudahdonlod_adm($untuk_adm);
		}
		else
		{
			$data['pesan'] = 'Belum ada yang di donload';
		}
		
		$this->load->view("template_koor",$data);
	}
	
	public function yg_sudahvalid()
	{
		$data['breadcrumb'] = "Berkas yang sudah valid";
		$data['main_view'] = 'download/v_download';
		$untuk_adm = $this->model->sudah_diver();
		if($untuk_adm)
		{
			$data['tabel_data'] = $this->model->tabel_adm_ygvalid($untuk_adm);
		}
		else
		{
			$data['pesan'] = 'Belum ada syarat yang valid';
		}
		
		$this->load->view("template_koor",$data);
	}
	
	
	public function download($namafile)
	{
		$this->model->tandai_sudahdonlod($namafile);
		$this->session->set_flashdata('pesan', 'Berkas sudah di download');
		
		$this->load->helper("download");
		$data = file_get_contents(base_url()."./files/".$namafile);
		force_download($namafile,$data);
		redirect('koor_download');
	}
	
	public function verifikasi($id)
	{
		$this->model->diterima($id);
		
		$mhs = $this->model->get_skripsi($id);
		$this->model->ubah_status_skripsi($mhs);
		
		redirect('koor_download/yg_sudahvalid');
	}
	
	public function ditolak($id)
	{
		$data['main_view'] = 'download/ket';
		$data['breadcrumb'] = 'Keterangan untuk berkas tidak valid';
		$data['form_action'] = 'koor_download/ditolak/'.$id;
		
		if($this->input->post('submit'))
		{
			if($this->model->validasi_ket() === TRUE)
			{
				if($this->model->ditolak($id))
				{
				
					$this->session->set_flashdata('pesan','Keterangan pada berkas tidak lengkap sudah dberikan');
					redirect('koor_download/yg_sudahdidonlod');
				}
				else
				{
					$data['pesan'] = 'Pemberitahuan gagal';
				}
			}
			else
			{
				$data['pesan'] = 'Pemberitahuan gagal';
				$this->load->view('template_koor',$data);
			}
		}
		else
		{
			$this->load->view('template_koor',$data);
		}
	}
}