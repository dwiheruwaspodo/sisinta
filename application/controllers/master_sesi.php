<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_Sesi extends MY_Controller {

    public $data = array(
                        'breadcrumb'    => 'Master Sesi',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_sesi/sesi',
                        'form_action'   => '',
                        'form_value'    => '',
                        );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_mastersesi', 'messi', TRUE);
    }

	public function index()
	{
        // hapus data temporary proses update
        $this->session->unset_userdata('sesi_mulai_sekarang', '');

        // Cari semua data kelas
        $messi = $this->messi->cari_semua();

        // data kelas ada, tampilkan
        if ($messi)
        {
            // buat tabel
            $tabel = $this->messi->buat_tabel($messi);
            $this->data['tabel_data'] = $tabel;
            $this->load->view('template_adm', $this->data);
        }
        // data kelas tidak ada
        else
        {
            $this->data['pesan'] = 'Tidak ada data Sesi.';
            $this->load->view('template_adm', $this->data);
        }
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Sesi > Tambah';
        $this->data['main_view']   = 'm_sesi/sesi_form';
        $this->data['form_action'] = 'master_sesi/tambah';

        // submit
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->messi->validasi_tambah())
            {
                if($this->messi->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_sesi');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmSesi = NULL)
    {
        $this->data['breadcrumb']  = 'Sesi > Edit';
        $this->data['main_view']   = 'm_sesi/sesi_form';
        $this->data['form_action'] = 'master_sesi/edit/' . $idmSesi;

        // pastikan id_kelas ada
        if( ! empty($idmSesi))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->messi->validasi_edit() === TRUE)
                {
                    //update db
                    $this->messi->edit($this->session->userdata('sesi_mulai_sekarang'));
					$this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('master_sesi');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }
            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai dafault form
                $messi = $this->messi->cari($idmSesi);
                foreach($messi as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data for edit
                $this->session->set_userdata('sesi_mulai_sekarang', $messi->idmSesi);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter id_kelas, kembalikan ke halaman kelas
        else
        {
            redirect('master_sesi');
        }
    }

    public function hapus($idmSesi = NULL)
    {
        // pastikan id_kelas yang akan dihapus
        if( ! empty($idmSesi))
        {
            if($this->messi->hapus($idmSesi))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_sesi');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_sesi');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_sesi');
        }
    }

}
/* End of file master_sesi.php */
/* Location: ./application/controllers/master_sesi.php */