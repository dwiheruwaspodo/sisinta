<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_v_nilaisidang extends MY_Controller
{
    public $data = array(
        'breadcrumb'    => 'Verifikasi Nilai Sidang',
        'pesan'         => '',
        'pagination'    => '',
        'tabel_data'    => '',
        'main_view'     => 'koor_v_nilaisidang/koor_v_nilaisidang',
        'form_action'   => '',
        'form_value'    => '',
    );

	public function __construct()
	{	
		parent::__construct();		
		$this->load->model('model_koor_v_nilaisidang', 'nilai', TRUE);
	}

	public function index($offset = 0)
    {
        $nilai = $this->nilai->cari_semua($offset);

        if ($nilai)
        {
            $tabel = $this->nilai->buat_tabel($nilai);
            $this->data['tabel_data'] = $tabel;

            // Paging
            // http://localhost/absensi2014/siswa/halaman/2
            $this->data['pagination'] = $this->nilai->paging(site_url('vnilaisidang/halaman'));
        }
        else
        {
            $this->data['pesan'] = 'Semua data sudah di verifikasi';
        }
        $this->load->view('template_koor', $this->data);
    }

    public function edit($idtSemSid = NULL)
    {
        $this->data['breadcrumb']  = 'Sidang > Verifikasi Nilai';
        $this->data['main_view']   = 'koor_v_nilaisidang/koor_v_nilaisidang';
        $this->data['form_action'] = 'koor_v_nilaisidang/edit/' . $idtSemSid;

        // pastikan parameter ada, mencegah error
        if( ! empty($idtSemSid))
        {
                //update db
                    if($this->nilai->edit($idtSemSid))
                    {
                        $this->session->set_flashdata('pesan', 'Proses verifikasi nilai sidang berhasil.');
                        redirect('koor_v_nilaisidang');
                    }
                    else
                    {
                        $this->session->set_flashdata('pesan', 'Ups! Entah mengapa proses verifikasi nilai sidang gagal.');
                        redirect('koor_v_nilaisidang');
                    }
            
        }
        // tidak ada parameter, kembalikan ke halaman absen
        else
        {
            redirect('koor_v_nilaisidang');
        }
    }
}
/* End of file koor_v_nilaisidang.php */
/* Location: ./application/controllers/koor_v_nilaisidang.php */