<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Catatan_bimb_dos extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Catatan Bimbingan',
		'main_view' => 'catatan_bimbingan/logbook_dos',
		'form_value' => '',
		'form_action' => '',
		'pesan' => '',
		'tabel_data' => '',
		'pagination' => '',
		'mhs_bimbingan' => '',
		'dosen' => '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_logbimbingan','bimbingan',TRUE);
	}
	
	public function index($offset = 0)
	{
		$catatan = $this->bimbingan->logbook_dos($offset);
		if($catatan)
		{
			$tabel = $this->bimbingan->tabel_dos($catatan);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->bimbingan->paging_dos(site_url('logbook_dosen/halaman'));
		}
		else
		{
			$catatan_semua = $this->bimbingan->logbook_dos_semua($offset);
			if($catatan_semua)
			{	
				$tabel = $this->bimbingan->tabel_dos_semua($catatan_semua);
				$this->data['breadcrumb'] = 'Daftar Logbook Semua Mahasiswa Bimbingan';
				$this->data['tabel_data'] = $tabel;
				$this->data['pagination'] = $this->bimbingan->paging_dos(site_url('logbook_dosen_/halaman'));
			}
			else
			{
				$this->data['pesan'] = 'Tidak ada catatan bimbingan';
			}
		}
		$this->load->view('template_dos',$this->data);
	}
	
	public function ins1()
	{
		$this->data['breadcrumb'] = 'Inisialisasi Catatan Bimbingan';
		$this->data['main_view'] = 'catatan_bimbingan/logbook_dos_ins';
		$this->data['form_action'] = 'catatan_bimb_dos/ins1';
		
		$mhs = $this->bimbingan->mhs_bimb1();
		if($mhs)
		{
			foreach ($mhs as $row)
			{
				$this->data['mhs_bimbingan'][$row->idmmhsw] = $row->mhsw_nama;
			}
		}
		else
		{
			$this->data['mhs_bimbingan']['00'] = '-';
			$this->data['pesan'] = 'Tidak ada mahasiswa bimbingan';
		}
///////////////////////////////////////////		
		if($this->input->post('submit'))
		{
			if($this->bimbingan->validasi_ins())
			{
				if($this->bimbingan->ins())
				{
					$this->session->set_flashdata('pesan','Inisialisasi berhasil');
					redirect('catatan_bimb_dos');
				}
				else
				{
					$this->data['pesan'] = 'Gagal Inisialisasi';
					$this->load->view('template_dos',$this->data);
				}
			}
			else
			{
				$this->load->view('template_dos',$this->data);
			}
		}
		else
		{
			$this->load->view('template_dos',$this->data);
		}
	}
	
	public function ins2()
	{
		$this->data['breadcrumb'] = 'Inisialisasi Catatan Bimbingan';
		$this->data['main_view'] = 'catatan_bimbingan/logbook_dos_ins';
		$this->data['form_action'] = 'catatan_bimb_dos/ins2';
		
		$mhs = $this->bimbingan->mhs_bimb2();
		if($mhs)
		{
			foreach ($mhs as $row)
			{
				$this->data['mhs_bimbingan'][$row->idmmhsw] = $row->mhsw_nama;
			}
		}
		else
		{
			$this->data['mhs_bimbingan']['00'] = '-';
			$this->data['pesan'] = 'Tidak ada mahasiswa bimbingan';
		}
///////////////////////////////////////////		
		if($this->input->post('submit'))
		{
			if($this->bimbingan->validasi_ins())
			{
				if($this->bimbingan->ins())
				{
					$this->session->set_flashdata('pesan','Inisialisasi berhasil');
					redirect('catatan_bimb_dos');
				}
				else
				{
					$this->data['pesan'] = 'Gagal Inisialisasi';
					$this->load->view('template_dos',$this->data);
				}
			}
			else
			{
				$this->load->view('template_dos',$this->data);
			}
		}
		else
		{
			$this->load->view('template_dos',$this->data);
		}
	}
	
	public function catatan_dosen($idLogBook = NULL)
	{
		$this->data['breadcrumb'] = 'Masukan dari dosen';
		$this->data['form_action'] = 'catatan_bimb_dos/catatan_dosen/'.$idLogBook;
		$this->data['main_view'] = 'catatan_bimbingan/logbook_dos_form';
		
		if(!empty($idLogBook))
		{
			if($this->input->post('submit'))
			{
				if($this->bimbingan->validasi_catatan_dos() === TRUE)
				{
					if($this->bimbingan->catatan_dos($idLogBook))
					{
						$this->session->set_flashdata('pesan','Masukan kepada mahasiswa berhasil!');
						redirect('catatan_bimb_dos');
					}
					else
					{
						$this->session->set_flashdata('pesan','Ups! Logbook tidak terverifikasi, Anda Harus Memberikan Masukan!');
						redirect('catatan_bimb_dos');
					}
				}
				else
				{
					$this->load->view('template_dos',$this->data);
				}
			}
			else
			{
				$catatan = $this->bimbingan->cari($idLogBook);
				foreach($catatan as $key=> $value)
				{
					$this->data['form_value'][$key]= $value;
				}
				$this->session->set_userdata('id_sekarang',$catatan->idLogBook);
				$this->load->view('template_dos',$this->data);
			}
		}
		else
		{
			redirect('catatan_bimb_dos');
		}
	}
	
	
}

/* End of file catatan_bimb_dos.php */
/* Location: ./application/controller/catatan_bimb_dos.php */