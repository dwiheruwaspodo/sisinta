<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kajur_lihatbeban extends MY_Controller
{
	public $data = array (
		'breadcrumb'		=> 'Beban Pembimbingan',
		'pesan'				=> '',
		'pagination'		=> '',
		'tabel_data'		=> '',
		'main_view'			=> 'koor_lihatbeban/lihatbeban',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_lihatbeban','beban',TRUE);
	}
	
	public function index($offset = 0)
	{
		$beban = $this->beban->lihat_beban($offset);
		
		if($beban)
		{
			$tabel = $this->beban->buat_tabel($beban);
			$this->data['tabel_data'] = $tabel;
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada beban dosen';
		}
		$this->load->view('template_kajur',$this->data);
	}
}