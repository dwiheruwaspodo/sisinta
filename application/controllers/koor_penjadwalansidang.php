<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Koor_penjadwalansidang extends MY_Controller
{
	public $data = array(
		'breadcrumb'	=> 'Penjadwalan Sidang',
		'main_view'		=> 'koor_penjadwalansidang/jadwal',
		'pagination'	=> '',
		'tabel_data'	=> '',
		'form_value'	=> '',
		'form_action'	=> '',
		'pesan'			=> '',
		'opsi_mhs'		=> '',
		'opsi_ruang'	=> '',
		'jam'			=> '',
		'opsi_dos'		=> '',
		'opsi_dos2'		=> '',
		
	);	
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_penjadwalansidang','sidang',TRUE);
		$this->load->model('model_mastersesi','sesi',TRUE);
		$this->load->model('model_masterruang','ruang',TRUE);
		$this->load->model('model_masterdosen','dosen',TRUE);
	}
	
	public function index($offset = 0)
	{
		$data = $this->sidang->cari_semua($offset);
		if($data)
		{
			$tabel = $this->sidang->buat_tabel($data);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination']	= $this->sidang->paging(site_url('sidang/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Belum ada sidang skripsi';
			
		}
		$this->load->view('template_koor',$this->data);
	}
	
	public function tambah()
	{
		$this->data['main_view'] = 'koor_penjadwalansidang/jadwal_form';
		$this->data['form_action']	= 'koor_penjadwalansidang/tambah';
		$this->data['breadcrumb'] 	= 'Sidang > Penjadwalan';
		
		$opsi_dos = $this->dosen->dd_dos();
		if($opsi_dos)
		{
			foreach($opsi_dos as $row)
			{
				$this->data['opsi_dos']['00'] = '-- Pilih Penguji 1 --';
				$this->data['opsi_dos'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data dosen. Isi dulu data master';
			$this->data['opsi_dos']['00'] = '-';
		}
///////////////////////////////////////		
		$opsi_dos2 = $this->dosen->dd_dos();
		if($opsi_dos2)
		{
			foreach($opsi_dos2 as $row)
			{
				$this->data['opsi_dos2']['00'] = '-- Pilih Penguji 2 --';
				$this->data['opsi_dos2'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data dosen. Isi dulu data master';
			$this->data['opsi_dos2']['00'] = '-';
		}
///////////////////////////////////////		
		$jam = $this->sesi->cari_semua();
		if($jam)
		{
			foreach($jam as $row)
			{
				$this->data['jam'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['jam']['00'] = '-';
			$this->data['pesan'] = 'data jam tidak ada';
		}
///////////////////////////////////////		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['opsi_ruang'][$row->idmRuang] = $row->ruang_nama;
				
			}
		}
		else
		{
			$this->data['opsi_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}

////////////////////////////////////////
		$mhs = $this->sidang->dd_mhs();
		if($mhs)
		{
			foreach($mhs as $row)
			{
				$this->data['opsi_mhs'][$row->idmSkripsi] = $row->mhsw_nama;
			}
		}
		else
		{
			$this->data['opsi_mhs'][00] = '-';
			$this->data['pesan'] = 'Data mahasiswa tidak tersedia';
		}
		
////////////////////////////////////////
		if($this->input->post('submit'))
		{
			if($this->sidang->validasi_tambah())
			{
				if($this->sidang->tambah() && $this->sidang->edit_skripsi($this->input->post('idmSkripsi')))
				{
					$this->session->set_flashdata('pesan','Proses Penjadwalan Berhasil');
					redirect('koor_penjadwalansidang');
				}
				else
				{
					$this->data['pesan'] = 'Proses Penjadwalan gagal! Pastikan semua field terisi!';
					$this->load->view('template_koor', $this->data);
				}
			}
			else
			{
				$this->data['pesan'] = 'Field harus terisi semua!';
				$this->load->view('template_koor', $this->data);
			}
		}
		else
		{
			$this->load->view('template_koor',$this->data);
		}
	}
	
	public function edit($idtSemSid = NULL)
	{
		$this->data['breadcrumb']	= 'Edit Jadwal Sidang';
		$this->data['main_view']	= 'koor_penjadwalansidang/jadwal_edit';
		$this->data['form_action']	= 'koor_penjadwalansidang/edit/'. $idtSemSid;

		$opsi_dos = $this->dosen->dd_dos();
		if($opsi_dos)
		{
			foreach($opsi_dos as $row)
			{
				$this->data['opsi_dos'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data dosen. Isi dulu data master';
			$this->data['opsi_dos']['00'] = '-';
		}
///////////////////////////////////////		
		$opsi_dos2 = $this->dosen->dd_dos();
		if($opsi_dos2)
		{
			foreach($opsi_dos2 as $row)
			{
				$this->data['opsi_dos2'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data dosen. Isi dulu data master';
			$this->data['opsi_dos2']['00'] = '-';
		}
///////////////////////////////////////		
		$jam = $this->sesi->cari_semua();
		if($jam)
		{
			foreach($jam as $row)
			{
				$this->data['jam'][$row->idmSesi] = $row->sesi_mulai;
			}
		}
		else
		{
			$this->data['jam']['00'] = '-';
			$this->data['pesan'] = 'data jam tidak ada';
		}
///////////////////////////////////////		
		$ruangan = $this->ruang->dd_ruang();
		if($ruangan)
		{
			foreach($ruangan as $row)
			{
				$this->data['opsi_ruang'][$row->idmRuang] = $row->ruang_nama;
				
			}
		}
		else
		{
			$this->data['opsi_ruang'][00]='-';
			$this->data['pesan'] = 'Data ruangan tidak tersedia. Silahkan isi dahulu data ruangan';
		}

////////////////////////////////////////

		if(!empty ($idtSemSid))
		{		
			if($this->input->post('submit'))
			{
				if($this->sidang->validasi_edit() === TRUE)
				{
					$this->sidang->edit($idtSemSid);
					$this->session->set_flashdata('pesan','Proses Edit Jadwal Seminar Berhasil');
					redirect('koor_penjadwalansidang');
				}
				else
				{
					$this->data['pesan'] = 'Proses Penjadwalan gagal! Pastikan semua field terisi!';
					$this->load->view('template_koor', $this->data);
				}
			}
			else
			{
				$sidang = $this->sidang->cari($idtSemSid);
				foreach($sidang as $key => $value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang',$sidang->idtSemSid);
				$this->load->view('template_koor',$this->data);
			}
		}
		else
		{
			redirect('koor_penjadwalansidang');
		}
	}
	
	public function hapus($idtSemSid = NULL)
	{
		$this->batal_sidang($idtSemSid);
		if($this->sidang->hapus($idtSemSid))
		{
			$this->session->set_flashdata('pesan','Proses Hapus Jadwal Seminar Berhasil');
			redirect('koor_penjadwalansidang');
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses Hapus jadwal seminar gagal');
			redirect('koor_penjadwalansidang');
		}
	
	}
	
	public function batal_sidang($id)
	{
		$batal = $this->sidang->get_idSk($id);
		$this->sidang->batal($batal);
	}
	
}
/* End of file adm_penjadwalanseminar.php */
/* Location: ./application/controller/adm_penjadwalanseminar.php */