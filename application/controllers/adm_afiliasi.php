<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_afiliasi extends MY_Controller
{
	
	public $data = array(
			'breadcrumb'	=> 'Data Afiliasi Mahasiswa',
			'main_view'		=> 'adm_afiliasi/adm_afiliasi',
			'pesan'			=> '',
			'tabel_data'	=> '',
			'pagination'	=> '',
			'option_mhs'	=> '',
			'option_tempat'	=> '',
			'form_value'	=> '',
			'form_action'	=> ''
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_afiliasi','aff',TRUE);
		$this->load->model('model_masterafiliasi','mff',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('id_sekarang','');
		$this->session->unset_userdata('tempat_sekarang','');
		
		$afi = $this->aff->cari_semua($offset);
		if($afi)
		{
			$tabel = $this->aff->buat_tabel($afi);
			$this->data['tabel_data'] = $tabel;
			
			$this->data['pagination'] = $this->aff->paging(site_url('afiliasi_mahasiswa/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data afiliasi mahasiswa';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb']	= 'Afiliasi > Tambah';
		$this->data['main_view']	= 'adm_afiliasi/adm_afiliasi_form';
		$this->data['form_action']	= 'adm_afiliasi/tambah';
////////////////////////////////////		
		$mhs = $this->aff->dd_mhs();
		if ($mhs)
		{
			foreach($mhs as $row)
			{
				$this->data['option_mhs'][$row->idmSkripsi] = $row->mhsw_nama;
			}
		}
		else
		{
			$this->data['option_mhs']['00'] = '-';
			$this->data['pesan'] = 'Data mahasiswa tidak tersedia';
		}
//////////////////////////////////////
		$tempat = $this->mff->dd_afiliasi();
		if($tempat)
		{
			foreach ($tempat as $row)
			{
				$this->data['option_tempat'][$row->idmAfiliasi] = $row->mAfiliasi_nama;
			}
		}
		else
		{
			$this->data['option_tempat']['00'] = '-';
			$this->data['pesan'] = 'Tidak ada data afiliasi';
		}

		
		if($this->input->post('submit'))
		{
			if($this->aff->validasi_tambah())
			{
				if($this->aff->tambah())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil');
					redirect('adm_afiliasi');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm',$this->data);
			}						
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	 public function edit($idtAfiliasi = NULL)
    {
        $this->data['breadcrumb']  = 'Afiliasi Mahasiswa > Edit';
        $this->data['main_view']   = 'adm_afiliasi/adm_afiliasi_form';
        $this->data['form_action'] = 'adm_afiliasi/edit/' . $idtAfiliasi;

        $mhs = $this->aff->dd_mhs();
		foreach($mhs as $row)
		{
			$this->data['option_mhs'][$row->idmSkripsi] = $row->mhsw_nama;
		}
//////////////////////////////////////
		$tempat = $this->mff->dd_afiliasi();
		foreach ($tempat as $row)
		{
			$this->data['option_tempat'][$row->idmAfiliasi] = $row->mAfiliasi_nama;
		}
		
		if( ! empty($idtAfiliasi))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->aff->validasi_edit() === TRUE)
                {
                    //update db
                    $this->aff->edit($this->session->userdata('id_sekarang'));
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('adm_afiliasi');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai default form
                $tempat = $this->aff->cari($idtAfiliasi);
                foreach($tempat as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                $this->session->set_userdata('id_sekarang', $tempat->idtAfiliasi);
				$this->load->view('template_adm', $this->data);
            }
        }
        
        else
        {
            redirect('adm_afiliasi');
        }
    }
	
	public function hapus($idtAfiliasi = NULL)
	{
		if(!empty($idtAfiliasi))
		{
			if($this->aff->hapus($idtAfiliasi))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil');
				redirect ('adm_afiliasi');
			}
			else
			{
				$this->session->set_flashdata('pesan','Proses hapus data gagal');
				redirect ('adm_afiliasi');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses hapus data gagal');
			redirect ('adm_afiliasi');
		}
	}
	
}

/* End of file adm_afiliasi.php */
/* Location: ./application/controller/adm_afiliasi.php */