<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_lihatbeban extends MY_Controller
{
	public $data = array (
		'breadcrumb'		=> 'Beban Pembimbingan',
		'pesan'				=> '',
		'tabel_data'		=> '',
		'main_view'			=> 'koor_lihatbeban/lihatbeban',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_lihatbeban','beban',TRUE);
	}
	
	public function index()
	{
		$beban = $this->beban->lihat_beban();
		
		if($beban)
		{
			$tabel = $this->beban->buat_tabel($beban);
			$this->data['tabel_data'] = $tabel;
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada beban dosen';
		}
		$this->load->view('template_koor',$this->data);
	}
}