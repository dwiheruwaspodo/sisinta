<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_hakakses extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Ganti Koordinator',
		'pesan' => '',
		'tabel_data' => '',
		'main_view' => 'adm_hakakses/adm_hakakses_koor',
		'form_action' => '',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_hakakses','hak',TRUE);
	}
	
	public function index($offset = 0)
	{
		$koor = $this->hak->tampil_koor();
		if($koor)
		{
			$this->data['tabel_data'] = $this->hak->tabel_koor($koor);
		}
		elseif(is_null($koor) || empty($koor))
		{
			$daftaran = $this->hak->dosen($offset);
			$this->data['tabel_data'] = $this->hak->tabel_inginjadikoordinator($daftaran);
			$this->data['pagination'] = $this->hak->paging(site_url('koorbaru/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada koordinator';
			echo anchor('adm_hakakses/jadikan_koor','Tambah Koordinator',array('class'=> 'add'));
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function kandidat_koor($offset = 0)
	{
		$daftaran = $this->hak->dosen($offset);
		$this->data['tabel_data'] = $this->hak->tabel_inginjadikoordinator($daftaran);
		$this->data['pagination'] = $this->hak->paging(site_url('tambah_koorbaru/halaman'));
		$this->load->view('template_adm',$this->data);
	}
	
	public function jadikan_dosen($users_name = NULL)
	{
		$turpang = $this->hak->koorlama($users_name);
		if($turpang)
		{
			$this->session->set_flashdata('pesan','Koordinator sudah menjadi dosen biasa');
			redirect('adm_hakakses');
		}
		else
		{
			$this->data['pesan'] = 'Koordinator belum ganti';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function jadikan_koor($users_name = NULL)
	{
		$napang = $this->hak->koorbaru($users_name);
		if($napang)
		{
			$this->session->set_flashdata('pesan','Koordinator baru telah terpilih');
			redirect('adm_hakakses');
		}
		else
		{
			$this->data['pesan'] = 'Koordinator belum terpilih';
		}
		$this->load->view('template_adm',$this->data);
	}
	
}