<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_katalog extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->library(array('pagination','session'));
		$this->load->model('model_all_katalogskripsi','katalog', TRUE);
	}
	
	function cari()
	{
		$data['main_view'] = 'all_katalog/judul_hasil';
		$data['form_action'] = 'search_katalog/cari';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul', $data['judul']);
		} 
		else 
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->katalog->cari_judul($batas,$offset,$data['judul']);
		$tot_hal = $this->katalog->total($data['judul']);
		
		$config['base_url'] = base_url() . 'index.php/search_katalog/cari/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_mhs',$data);
	}
	
	function cari_dos()
	{
		$data['main_view'] = 'all_katalog/judul_hasil_dos';
		$data['form_action'] = 'search_katalog/cari_dos';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul', $data['judul']);
		} 
		else 
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->katalog->cari_judul($batas,$offset,$data['judul']);
		$tot_hal = $this->katalog->total($data['judul']);
		
		$config['base_url'] = base_url() . 'index.php/search_katalog/cari_dos/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_dos',$data);
	}
	
	function cari_koor()
	{
		$data['main_view'] = 'all_katalog/judul_hasil_koor';
		$data['form_action'] = 'search_katalog/cari_koor';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul', $data['judul']);
		} 
		else 
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->katalog->cari_judul($batas,$offset,$data['judul']);
		$tot_hal = $this->katalog->total($data['judul']);
		
		$config['base_url'] = base_url() . 'index.php/search_katalog/cari_koor/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_koor',$data);
	}
	
	function cari_kaj()
	{
		$data['main_view'] = 'all_katalog/judul_hasil_kaj';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul', $data['judul']);
		} 
		else 
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->katalog->cari_judul($batas,$offset,$data['judul']);
		$tot_hal = $this->katalog->total($data['judul']);
		
		$config['base_url'] = base_url() . 'index.php/search_katalog/cari_kaj/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_kajur',$data);
	}
	
	function cari_adm()
	{
		$data['main_view'] = 'all_katalog/judul_hasil_adm';
		$data['form_action'] = 'search_katalog/cari_adm';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul', $data['judul']);
		} 
		else 
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->katalog->cari_judul($batas,$offset,$data['judul']);
		$tot_hal = $this->katalog->total($data['judul']);
		
		$config['base_url'] = base_url() . 'index.php/search_katalog/cari_adm/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_adm',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */