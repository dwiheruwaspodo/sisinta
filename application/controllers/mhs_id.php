<?php if (!defined('BASEPATH')) exit('No direct script allowed');

class Mhs_id extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_mhs_id','mhs',TRUE);
		$this->load->model('model_masterdosen','dosen',TRUE);
	}
	
	public $data = array(
			'main_view'		=> 'id_mhs/id_mhs',
			'breadcrumb' 	=> 'Data Skripsi',
			'form_action' 	=> '',
			'form_value'	=> '',
			'info' 			=> '',
			'opsi_p1' 		=> '',
			'pesan'			=> '',
			'tabel_data'	=> '',
	);
	
	public function index()
	{
		$db = $this->mhs->cek_data();
		if($db)
		{
			redirect('mhs_id/info');
		}
		else
		{
			redirect('mhs_id/registrasi');
		}
		
	}
	
	public function info()
	{
		$this->data['info'] = $this->mhs->info_sendiri($this->session->userdata('users_name'));
		$this->load->view('template_mhs',$this->data);
	}
	
	public function registrasi()
	{
		$this->data['main_view'] = 'id_mhs/dataskripsi_form';
		$this->data['form_action'] = 'mhs_id/registrasi';
		$this->data['breadcrumb'] = 'Registrasi Skripsi';
			
		$dos1 = $this->dosen->dd_dos();
		if($dos1)
		{
			foreach($dos1 as $row)
			{
				$this->data['opsi_p1'][0] = '-- Pilih Dosen Pembimbing 1 --';
				$this->data['opsi_p1'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p1']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
		
		$dos2 = $this->dosen->dd_dos();
		if($dos2)
		{
			foreach($dos1 as $row)
			{
				$this->data['opsi_p2'][0] = '-- Pilih Dosen Pembimbing 2 --';
				$this->data['opsi_p2'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p2']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
		
		if($this->input->post('submit'))
		{
			if($this->mhs->validasi_pengajuan())
			{
				if($this->mhs->setor_pengajuan())
				{
					$this->session->set_flashdata('pesan','Registrasi Berhasil. Tunggu Konfirmasi dari Koordinator');
					redirect('mhs_id/info');
				}
				else
				{
					$this->data['pesan'] = 'Proses registrasi skripsi gagal';
					$this->load->view('template_mhs',$this->data);
				}
			}
			else
			{
				$this->data['pesan'] = 'Field harus terisi semua!';
				$this->load->view('template_mhs',$this->data);
			}
		}
		else
		{
			$this->load->view('template_mhs',$this->data);
		}
	}
	
	public function edit($id)
	{
		$this->data['main_view'] = 'id_mhs/dataskripsi_form';
		$this->data['form_action'] = 'mhs_id/edit/'.$id;
		$this->data['breadcrumb'] = 'Edit Data Skripsi';
			
		$dos1 = $this->dosen->dd_dos();
		if($dos1)
		{
			foreach($dos1 as $row)
			{
				$this->data['opsi_p1'][0] = '-- Pilih Dosen Pembimbing 1 --';
				$this->data['opsi_p1'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p1']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
		
		$dos2 = $this->dosen->dd_dos();
		if($dos2)
		{
			foreach($dos1 as $row)
			{
				$this->data['opsi_p2'][0] = '-- Pilih Dosen Pembimbing 2 --';
				$this->data['opsi_p2'][$row->idmDosen] = $row->dos_nama;
			}
		}
		else
		{
			$this->data['opsi_p2']['00'] = '-';
			$this->data['pesan'] = 'Data Dosen tidak tersedia';
		}
		
		if($this->input->post('submit'))
		{
			if($this->mhs->validasi_edit())
			{
				if($this->mhs->setor_baru($id))
				{
					$this->session->set_flashdata('pesan','Data berhasil diperbarui. Tunggu Konfirmasi dari Koordinator');
					redirect('mhs_id/info');
				}
				else
				{
					$this->data['pesan'] = 'Proses registrasi skripsi gagal';
					$this->load->view('template_mhs',$this->data);
				}
			}
			else
			{
				$this->data['pesan'] = 'Field harus terisi semua!';
				$this->load->view('template_mhs',$this->data);
			}
		}
		else
		{
			$edit = $this->mhs->cari($id);
			foreach($edit as $key=>$value)
			{
				$this->data['form_value'][$key] = $value;
			}
			
			$this->load->view('template_mhs',$this->data);
		}
	}
	
}