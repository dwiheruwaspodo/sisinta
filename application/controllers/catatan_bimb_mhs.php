<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Catatan_bimb_mhs extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Logbook Skripsi Mahasiswa',
		'main_view' => 'catatan_bimbingan/logbook_pilihan',
		'form_value' => '',
		'form_action' => '',
		'pesan' => '',
		'pagination' => '',
		'mahasiswa' => '',
		'verif' => '',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_logbimbingan','bimbingan',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->load->view('template_mhs',$this->data);
	}
	
	public function dari_p1()
	{
		
		$this->data['mahasiswa'] = $this->bimbingan->logbook_mhs_darip1();
		$hasil = $this->data['mahasiswa'];
		if(empty($hasil) || is_null($hasil))
		{
			$this->data['pesan'] = 'Tidak ada catatan dari Pembimbing 1!';
		}
		$this->data['main_view'] = 'catatan_bimbingan/logbook_mhs';
		$this->load->view('template_mhs',$this->data);
	}
	
	public function dari_p2()
	{
		$this->data['mahasiswa'] = $this->bimbingan->logbook_mhs_darip2();
		$hasil = $this->data['mahasiswa'];
		if(empty($hasil) || is_null($hasil))
		{
			$this->data['pesan'] = 'Tidak ada catatan dari Pembimbing 2!';
		}
		$this->data['main_view'] = 'catatan_bimbingan/logbook_mhs';
		$this->load->view('template_mhs',$this->data);
	}
	
	public function konsultasi($idLogBook = NULL)
	{
		$this->data['breadcrumb'] = 'Konsultasi Skripsi';
		$this->data['main_view'] = 'catatan_bimbingan/logbook_mhs_form';
		$this->data['form_action'] = 'catatan_bimb_mhs/konsultasi/'.$idLogBook;
		
		if(!empty($idLogBook))
		{
			if($this->input->post('submit'))
			{
				if($this->bimbingan->validasi_catatan_mhs())
				{	
					if($this->bimbingan->catatan_mhs($idLogBook))
					{
						$this->session->set_flashdata('pesan','Konsultasi berhasil');
						redirect('catatan_bimb_mhs');
					}
					else
					{
						$this->data['pesan'] = 'Form Konsultasi Harus Diisi!';
						$this->load->view('template_mhs',$this->data);
					}
				}
				else
				{
					$this->load->view('template_mhs',$this->data);
				}
			}
			else
			{
				$buk = $this->bimbingan->cari($idLogBook);
				foreach($buk as $key=> $value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang',$buk->idLogBook);
				$this->load->view('template_mhs',$this->data);
			}
		}
		else
		{
			redirect('catatan_bimb_mhs');
		}
	}
}

/* End of file catatan_bimb_mhs.php */
/* Location: ./application/controller/catatan_bimb_mhs.php */