<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class All_katalogskripsi_kaj extends MY_Controller
{
	public $data = array (
		'breadcrumb'		=> 'katalog',
		'pesan'				=> '',
		'pagination'		=> '',
		'tabel_data'		=> '',
		'main_view'			=> 'all_katalog/all_katalogskripsi',
		'form_action'		=> 'search_katalog/cari_kaj',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_all_katalogskripsi','katalog', TRUE);
	}
	
	public function index($offset = 0)
	{
		if($this->input->post('submit'))
		{
			$hasil = $this->katalog->cari_katalog($offset);
			if($hasil)
			{
				$tabel = $this->katalog->buat_tabel($hasil);
				$this->data['tabel_data'] = $tabel;
				$this->data['pagination'] = $this->katalog->paging_cari(site_url('pencarianjudul4/halaman'));
			}
			else
			{
				$this->data['pesan'] = 'Data yang di cari tidak ada';
			}
		}
		else
		{
			$katalog = $this->katalog->lihat_katalog($offset);
			if($katalog)
			{
				$tabel = $this->katalog->buat_tabel($katalog);
				$this->data['tabel_data'] = $tabel;
				
				$this->data['pagination'] = $this->katalog->paging(site_url('all_katalogskripsi4/halaman'));
			}
			else
			{
				$this->data['pesan'] = 'Tidak ada katalog skripsi';
			}
		}
		$this->load->view('template_kajur',$this->data);
	}
	
}