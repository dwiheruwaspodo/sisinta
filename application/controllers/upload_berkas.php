<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Upload_berkas extends MY_Controller
{
	public $data = array(
		'main_view' => 'upload/v_upload',
		'pesan' => '',
		'breadcrumb' => 'Upload Syarat Seminar',
		'scriptaksi' => 'upload_berkas/upload',
		'tabel_data' => '',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('html','form','url','text'));
		$this->load->model('model_upload','up',TRUE);
	}
	
	public function index()
	{
		$daftar = $this->up->untuk_mhs_barumasuk();
		$diterima = $this->up->untuk_mhs_diterima();
		$didonlod = $this->up->untuk_mhs_didonlod();
		$ditolak = $this->up->untuk_mhs_ditolak();
		if($daftar)
		{
			$tabel = $this->up->tabel_mhs($daftar);
			$this->data['tabel_data'] = $tabel;
		}
		elseif($diterima)
		{
			$tabel = $this->up->tabel_mhs_diterima($diterima);
			$this->data['tabel_data'] = $tabel;
		}
		elseif($didonlod)
		{
			$tabel = $this->up->tabel_mhs($didonlod);
			$this->data['tabel_data'] = $tabel;
		}
		elseif($ditolak)
		{
			$tabel = $this->up->tabel_mhs_ditolak($ditolak);
			$this->data['tabel_data'] = $tabel;
		}
		else
		{
			$this->data['pesan'] = 'Anda belum mengumpulkan syarat seminar';
		}
		
		$this->load->view('template_mhs',$this->data);
	}
	
	public function cek_tipe($tipe)
	{
		if($tipe == 'application/zip')
		{
			return ".zip";
		}
		elseif($tipe == 'application/rar')
		{
			return ".rar";
		}
		else
		{
			return FALSE;
		}
	}
	
	public function upload()
	{
		$tipe = $this->cek_tipe($_FILES['userfile']['type']);
		$nama_file = "s_seminar_".$this->session->userdata('users_name').$tipe;
		
		$config['upload_path'] = './files';
		$config['allowed_types'] = 'rar|zip';
		$config['overwrite'] = 'true';
		$config['max_size'] = '2048';
		$config['overwrite'] = 'TRUE';
		$config['file_name'] = $nama_file;
		
		$this->load->library('upload',$config);
		$this->upload->initialize($config);
		$this->data['scriptaksi'] = 'upload_berkas/upload';
		if(! $this->upload->do_upload('userfile'))
		{
			$this->session->set_flashdata('pesan','File belum dipilih! Pilih File terlebih dahulu!');
			redirect('upload_berkas');
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$this->data['pesan'] = 'Upload File Berhasil';
			$this->up->upload_berkas($nama_file);
			
			redirect('upload_berkas');
		}
		$this->load->view('template_mhs',$this->data);
	}
	
	public function upload_lagi($id)
	{
		if($this->up->hapusdulu($id))
		{
			$this->session->set_flashdata('pesan','Silahkan upload lagi berkas seminar! Pastikan semua berkas terpenuhi!');
			redirect('upload_berkas');
		}
		else
		{
			$this->session->set_flashdata('pesan','Tidak dapat upload ulang.');
			redirect('upload_berkas');
		}
	}
}