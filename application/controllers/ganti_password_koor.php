<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ganti_password_koor extends CI_Controller
{
    public $data = array(
	'pesan'=> '',
	'breadcrumb' => 'Ganti Password',
	'tabel_data' => '',
	'main_view' => 'login/ganti_password'
	);

	public function __construct()
    {
		parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
		$this->load->model('model_login', 'login', TRUE);
	}

	public function index()
    {
		$data = $this->login->hasil();
		if($data)
		{
			$this->data['tabel_data'] = $this->login->buat_tabel_k($data);
		}
		else
		{
			$this->data['pesan'] = 'Kamu siapa?';
		}
		$this->load->view('template_koor',$this->data);
	}
	
	public function edit($users_name = NULL)
    {
        $this->data['breadcrumb']  = 'Ganti Password';
        $this->data['main_view']   = 'login/ganti_password_form';
        $this->data['form_action'] = 'ganti_password_koor/edit/' . $users_name;

        if( ! empty($users_name))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->login->validasi_gantipassword() === TRUE)
                {
                    //update db
                    $this->login->ganti_password($this->session->userdata('users_name'));
                    $this->session->set_flashdata('pesan', 'Proses ganti password berhasil.');

                    redirect('ganti_password_koor');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_koor', $this->data);
                }
            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai dafault form
                $ganti = $this->login->cari($users_name);
                
                // set temporary data for edit
                $this->session->set_userdata('users_name', $ganti->users_name);

                $this->load->view('template_koor', $this->data);
            }
        }
        // tidak ada parameter id_kelas, kembalikan ke halaman kelas
        else
        {
            redirect('login');
        }
    }
}
/* End of file login.php */
/* Location: ./application/controllers/login.php */