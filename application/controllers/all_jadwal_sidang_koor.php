<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class All_jadwal_sidang_koor extends MY_Controller
{
	public $data = array(
		'breadcrumb'	=> 'Jadwal Sidang',
		'main_view'		=> 'all_jadwal_sidang/all_jadwal_sidang',
		'pagination'	=> '',
		'tabel_data'	=> ''
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_koor_penjadwalansidang','sidang',TRUE);
		$this->load->model('model_all_jadwal_sidang','all',TRUE);
	}
	
	public function index($offset = 0)
	{
		$jadwal = $this->sidang->cari_semua($offset);
		if($jadwal)
		{
			$tabel = $this->all->buat_tabel($jadwal);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->sidang->paging(site_url('jadwal_sidang_k/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada jadwal sidang';
		}
		$this->load->view('template_koor',$this->data);
	}
}

/* End of file all_jadwal_sidang.php */
/* Location: ./application/controller/all_jadwal_sidang.php */