<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_statusskripsi extends MY_Controller
{
    public $data = array(
                        'breadcrumb'    => 'Master Status Skripsi',
                        'pesan'         => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_statusSkripsi/statusSkripsi',
                        'form_action'   => '',
                        'form_value'    => '',
                         );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_masterstatusskripsi', 'SS', TRUE);
	}

    public function index()
	{
        // hapus data temporary proses update
        $this->session->unset_userdata('status_sekarang', '');
  
        $SS = $this->SS->cari_semua();

        // ada data siswa, tampilkan
        if ($SS)
        {
            $tabel = $this->SS->buat_tabel($SS);
            $this->data['tabel_data'] = $tabel;

        }
        // tidak ada data siswa
        else
        {
            $this->data['pesan'] = 'Tidak ada data status.';
        }
        $this->load->view('template_adm', $this->data);
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Status > Tambah';
        $this->data['main_view']   = 'm_statusSkripsi/statusSkripsi_form';
        $this->data['form_action'] = 'master_statusskripsi/tambah';
		
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->SS->validasi_tambah())
            {
                if($this->SS->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_statusskripsi');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // if no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmStatusSkrip = NULL)
    {
        $this->data['breadcrumb']  = 'Status > Edit';
        $this->data['main_view']   = 'm_statusSkripsi/statusSkripsi_form';
        $this->data['form_action'] = 'master_statusskripsi/edit/' . $idmStatusSkrip;

        // Ada parameter
        if( ! empty($idmStatusSkrip))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->SS->validasi_edit() === TRUE)
                {
                    //update db
                    if($this->SS->edit($idmStatusSkrip))
					{
						$this->session->set_flashdata('pesan', 'Proses update data berhasil.');
						redirect('master_statusskripsi');
					}
					else
					{
						$this->session->set_flashdata('pesan', 'Proses update data gagal! Id digunakan pada informasi lain!');
						redirect('master_statusskripsi');
					}
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai default form
                $SS = $this->SS->cari($idmStatusSkrip);
                foreach($SS as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

        		$this->session->set_userdata('status_sekarang', $SS->idmStatusSkrip);
                $this->load->view('template_adm', $this->data);
            }
        }
        else
        {
            redirect('master_statusskripsi');
        }
    }

    public function hapus($idmStatusSkrip = NULL)
    {
        if( ! empty($idmStatusSkrip))
        {
            if($this->SS->hapus($idmStatusSkrip))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_statusskripsi');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_statusskripsi');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_statusskripsi');
        }
    }
}
/* End of file master_mahasiswa.php */
/* Location: ./application/controllers/master_mahasiswa.php */