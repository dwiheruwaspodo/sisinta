<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_input_nilai extends MY_Controller
{
    public $data = array(
        'breadcrumb'    => 'Pengisian Nilai Seminar dan Sidang',
        'pesan'         => '',
        'pagination'    => '',
        'tabel_data'    => '',
        'main_view'     => 'adm_nilaiseminar/adm_nilai',
        
    );

	public function __construct()
	{	
		parent::__construct();		
	}

	public function index($offset = 0)
    {
        $this->load->view('template_adm', $this->data);
    }

}

/* End of file model_adm_nilaisidang.php */
/* Location: ./application/models/model_adm_nilaisidang.php */