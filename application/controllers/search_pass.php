<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_pass extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->library(array('pagination','session'));
		$this->load->model('model_adm_lupapasword','hak',TRUE);
	}
	
	function cari()
	{
		$data['main_view'] = 'adm_hakakses/user_hasil';
		$data['form_action'] = 'search_pass/cari';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['user']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['user'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_user', $data['user']);
		} 
		else 
		{
			$data['user'] = $this->session->userdata('pencarian_user');
		}
		$data['pengguna'] = $this->hak->cari_user($batas,$offset,$data['user']);
		$tot_hal = $this->hak->total($data['user']);
		
		$config['base_url'] = base_url() . 'index.php/search_pass/cari/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_adm',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */