<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Semuabisalihat extends CI_Controller
{
	public $data = array(
		'katalog' =>  '',
		'form_action' => 'semuabisalihat/cari_cari',
		'pagination' => '',
		'pesan' => '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->library(array('pagination','session'));
		$this->load->model('msemuabisalihat','all',TRUE);
	}
	
	public function index($offset = 0)
	{
		$katalog = $this->all->katalog($offset);
		if($katalog)
		{
			$this->data['katalog'] = $this->all->tabel($katalog);
			$this->data['pagination'] = $this->all->paging(site_url('katalog/halaman/'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada katalog';
		}
		$this->load->view('login/semuabisalihat',$this->data);
	}
	
	public function detail($id)
	{
		$data['lihat'] = $this->all->detail($id);
		$this->load->view('login/detail',$data);
	}
	
	public function cari_cari()
	{
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['judul'] = "";
		$postkata = $this->input->post('cari');
		if($postkata)
		{
			$data['judul'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_judul',$data['judul']);
		}
		else
		{
			$data['judul'] = $this->session->userdata('pencarian_judul');
		}
		$data['katalog'] = $this->all->cari_cari($batas,$offset,$data['judul']);
		$tot_hal = $this->all->tot_hal($data['judul']);
		
			$config['base_url'] = base_url() . 'index.php/semuabisalihat/cari_cari/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
		$this->load->view('login/hasil_cari',$data);
	}
}