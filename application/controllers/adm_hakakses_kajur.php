<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Adm_hakakses_kajur extends MY_Controller
{
	public $data = array(
		'breadcrumb' => 'Ganti Ketua Jurusan',
		'pesan' => '',
		'tabel_data' => '',
		'main_view' => 'adm_hakakses/adm_hakakses',
		'form_action' => '',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_hakakses','hak',TRUE);
	}
	
	public function index($offset = 0)
	{
		$kajur = $this->hak->tampil_kajur();
		if($kajur)
		{
			$this->data['tabel_data'] = $this->hak->tabel_kajur($kajur);
		}
		elseif(is_null($kajur) || empty($kajur))
		{
			$daftaran = $this->hak->dosen($offset);
			$this->data['tabel_data'] = $this->hak->tabel_inginjadikajur($daftaran);
			$this->data['pagination'] = $this->hak->paging(site_url('kajurbaru/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada ketua jurusan';
			echo anchor('adm_hakakses_kajur/jadikan_kajur','Tambah Ketua Jurusan',array('class'=> 'add'));
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function jadikan_dosen($users_name = NULL)
	{
		$turpang = $this->hak->koorlama($users_name);
		if($turpang)
		{
			$this->session->set_flashdata('pesan','Ketua Jurusan sudah menjadi dosen biasa');
			redirect('adm_hakakses_kajur');
		}
		else
		{
			$this->data['pesan'] = 'Ketua Jurusan belum ganti';
			$this->load->view('template_adm',$this->data);
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function jadikan_kajur($users_name = NULL)
	{
		$napang = $this->hak->kajurbaru($users_name);
		if($napang)
		{
			$this->session->set_flashdata('pesan','Ketua Jurusan baru telah terpilih!');
			redirect('adm_hakakses_kajur');
		}
		else
		{
			$this->data['pesan'] = 'Ketua Jurusan belum terpilih';
			$this->load->view('template_adm',$this->data);
		}
		$this->load->view('template_adm',$this->data);
	}
	
}