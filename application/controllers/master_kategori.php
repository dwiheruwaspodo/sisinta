<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_kategori extends MY_Controller {

    public $data = array(
                        'breadcrumb'    => 'Master Kategori Skripsi',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_kategori/m_kategori',
                        'form_action'   => '',
                        'form_value'    => '',
                        );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_masterkategori', 'cat', TRUE);
    }

	public function index($offset= 0)
	{
        
        $kateg = $this->cat->cari_semua($offset);

        // data kelas ada, tampilkan
        if ($kateg)
        {
            // buat tabel
            $tabel = $this->cat->buat_tabel($kateg);
            $this->data['tabel_data'] = $tabel;
            $this->load->view('template_adm', $this->data);
        }
        // data kelas tidak ada
        else
        {
            $this->data['pesan'] = 'Tidak ada data kategori skripsi.';
            $this->load->view('template_adm', $this->data);
        }
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Kategori Skripsi > Tambah';
        $this->data['main_view']   = 'm_kategori/m_kategori_form';
        $this->data['form_action'] = 'master_kategori/tambah';

        // submit
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->cat->validasi_tambah())
            {
                if($this->cat->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_kategori');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmKategori = NULL)
    {
        $this->data['breadcrumb']  = 'Keterangan > Edit';
        $this->data['main_view']   = 'm_kategori/m_kategori_form';
        $this->data['form_action'] = 'master_kategori/edit/' . $idmKategori;

        // pastikan id_kelas ada
        if( ! empty($idmKategori))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->cat->validasi_edit() === TRUE)
                {
                    //update db
                    $this->cat->edit($idmKategori);
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('master_kategori');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }
            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai dafault form
                $categ = $this->cat->cari($idmKategori);
                foreach($categ as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data for edit
                $this->session->set_userdata('id_sekarang', $categ->idmKategori);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter id_kelas, kembalikan ke halaman kelas
        else
        {
            redirect('master_kategori');
        }
    }

    public function hapus($idmKategori = NULL)
    {
        // pastikan id_kelas yang akan dihapus
        if( ! empty($idmKategori))
        {
            if($this->cat->hapus($idmKategori))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_kategori');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_kategori');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_kategori');
        }
    }

}
/* End of file master_prodi.php */
/* Location: ./application/controllers/master_prodi.php */