<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class All_jadwal_seminar_dos extends MY_Controller
{
	public $data = array(
		'breadcrumb'	=> 'Jadwal Seminar',
		'main_view'		=> 'all_jadwal_seminar/all_jadwal_seminar',
		'pagination'	=> '',
		'tabel_data'	=> ''
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_adm_penjadwalanseminar','seminar',TRUE);
		$this->load->model('model_all_jadwal_seminar','all',TRUE);
	}
	
	public function index($offset = 0)
	{
		$jadwal = $this->seminar->cari_semua($offset);
		if($jadwal)
		{
			$tabel = $this->all->buat_tabel($jadwal);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->seminar->paging(site_url('jadwal_seminar_d/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada jadwal seminar';
		}
		$this->load->view('template_dos',$this->data);
	}
}

/* End of file all_jadwalan_seminar.php */
/* Location: ./application/controller/all_jadwalan_seminar.php */