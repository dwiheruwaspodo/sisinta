<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Master_syarat extends MY_Controller
{
	public $data = array (
			'breadcrumb'	=> 'Master Dokumen Syarat',
			'pesan' 		=> '',
			'paginatiom'  	=> '',
			'tabel_data' 	=> '',
			'main_view' 	=> 'm_syarat/syarat',
			'form_action' 	=> '',
			'form_value' 	=> '',
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_mastersyarat','syarat',TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('id_sekarang');
		$sy = $this->syarat->cari_semua($offset);
		
		if($sy)
		{
			$tabel = $this->syarat->buat_tabel($sy);
			$this->data['tabel_data'] = $tabel;
		}
		else
		{
			$this->data['pesan'] = 'tidak ada data';
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb']	= 'Syarat > Tambah';
		$this->data['main_view']	= 'm_syarat/syarat_form'	;
		$this->data['form_action']	= 'master_syarat/tambah';
		
		if($this->input->post('submit'))
		{
			if($this->afi->validasi_tambah())
			{
				if($this->afi->tambah())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil');
					redirect('master_syarat');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm',$this->data);
			}
		}
		else
		{
			$this->load->view('template_adm',$this->data);
		}
	}
	
	public function edit($idmDokSyarat = NULL)
	{
		$this->data['breadcrumb']		= 'Syarat > Edit';
		$this->data['main_view']		= 'm_syarat/syarat_form';
		$this->data['form_action']		= 'master_syarat/edit/' . $idmDokSyarat;
		if(!empty ($idmDokSyarat))
		{
			if($this->input->post('submit'))
			{
				if($this->syarat->validasi_edit() === TRUE)
				{
					$this->syarat->edit($this->session->userdata('id_sekarang'));
					$this->session->set_flashdata('pesan','Proses update data berhasil');
					redirect ('master_syarat');
				}
				else
				{
					$this->load->view('template_adm', $this->data);
				}
			}
			else
			{
				$sarat = $this->syarat->cari($idmDokSyarat);
				foreach($sarat as $key => $value)
				{
					$this->data['form_value'][$key] = $value;
				}
				$this->session->set_userdata('id_sekarang', $sarat->idmDokSyarat);
				$this->load->view('template_adm', $this->data);
			}
		}
		else
		{
			redirect ('master_syarat');
		}
	}
	
	public function hapus($idmDokSyarat = NULL)
	{
		if(!empty($idmDokSyarat))
		{
			if($this->syarat->hapus($idmDokSyarat))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil.');
				redirect('master_syarat');
			}
			else
			{
				$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
				redirect ('master_syarat');
			}
		}
		else
		{
			$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
			redirect ('master_syarat');
		}
	}
}