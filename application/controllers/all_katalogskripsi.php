<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class All_katalogskripsi extends MY_Controller
{
	public $data = array (
		'breadcrumb'		=> 'katalog',
		'pesan'				=> '',
		'pagination'		=> '',
		'tabel_data'		=> '',
		'main_view'			=> 'all_katalog/all_katalogskripsi',
		'form_value'		=> '',
		'form_action'		=> 'search_katalog/cari',
		
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_all_katalogskripsi','katalog', TRUE);
	}
	
	public function index($offset = 0)
	{
		$katalog = $this->katalog->lihat_katalog($offset);
			if($katalog)
			{
				$tabel = $this->katalog->buat_tabel($katalog);
				$this->data['tabel_data'] = $tabel;
				$this->data['pagination'] = $this->katalog->paging(site_url('all_katalogskripsi/halaman'));
			}
			else
			{
				$this->data['pesan'] = 'Tidak ada katalog skripsi';
			}
		$this->load->view('template_mhs',$this->data);
	}
	
	public function detail($detail)
	{
		$data['lihat'] = $this->katalog->detail($detail);
		$this->load->view('all_katalog/detail',$data);
	}
}