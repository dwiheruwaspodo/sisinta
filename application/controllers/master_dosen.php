<?php if(!defined('BASEPATH')) exit ('No direct script acces allowed');

class Master_dosen extends MY_Controller
{
	public $data = array (
					'breadcrumb' => 'Master Dosen',
					'pesan' => '',
					'pagination' => '',
					'tabel_data' => '',
					'main_view' => 'm_dosen/dosen',
					'form_action' => '',
					'form_value' => '',
					'prodi' => '',
				 );
				 
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_masterdosen','dosen', TRUE);
		$this->load->model('model_masterprodi','prodi', TRUE);
	}
	
	public function index($offset = 0)
	{
		$this->session->unset_userdata('id_sekarang');
		
		$masdos = $this->dosen->cari_semua($offset);
		if($masdos)
		{
			$tabel = $this->dosen->buat_tabel($masdos);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->dosen->paging(site_url('mdosen/halaman'));
			
		}
		else
		{
			$this->data['pesan'] = "Tidak ada data Dosen.";
		}
		$this->load->view('template_adm',$this->data);
	}
	
	public function tambah()
	{
		$this->data['breadcrumb'] = 'Dosen > Tambah';
		$this->data['main_view'] = 'm_dosen/dosen_form';
		$this->data['form_action'] = 'master_dosen/tambah';
		
		$prodi = $this->prodi->cari_semua();

        if($prodi)
        {
            foreach($prodi as $row)
            {
                $this->data['prodi'][$row->idmProdi] = $row->prodi_nama;
            }
        }
        
        else
        {
            $this->data['prodi']['00'] = '-';
            $this->data['pesan'] = 'Data prodi tidak tersedia.';
        }
		
		if($this->input->post('submit'))
		{
			if($this->dosen->validasi_tambah())
			{
				if($this->dosen->tambah() && $this->dosen->tambah_user())
				{
					$this->session->set_flashdata('pesan','Proses tambah data berhasil.');
					redirect('master_dosen');
				}
				else
				{
					$this->data['pesan'] = 'Proses tambah data gagal';
					$this->load->view('template_adm',$this->data);
				}
			}
			else
			{
				$this->load->view('template_adm', $this->data);
			}
		}
		else
		{
			$this->load->view('template_adm', $this->data);
		}
	}
	
	public function edit($idmDosen = NULL)
    {
        $this->data['breadcrumb']  = 'Dosen > Edit';
        $this->data['main_view']   = 'm_dosen/dosen_form';
        $this->data['form_action'] = 'master_dosen/edit/' . $idmDosen;
		
		$prodi = $this->prodi->cari_semua();
		
		if($prodi)
        {
            foreach($prodi as $row)
            {
                $this->data['prodi'][$row->idmProdi] = $row->prodi_nama;
            }
        }
        
        else
        {
            $this->data['prodi']['00'] = '-';
            $this->data['pesan'] = 'Data prodi tidak tersedia.';
        }
		
        // Ada parameter
        if( ! empty($idmDosen))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->dosen->validasi_edit() === TRUE)
                {
                    //update db
                    $this->dosen->edit($this->session->userdata('id_sekarang'));
                    $this->session->set_flashdata('pesan', 'Proses update data berhasil.');

                    redirect('master_dosen');
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }

            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai default form
                $dosen = $this->dosen->cari($idmDosen);
                foreach($dosen as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data untuk edit
                $this->session->set_userdata('id_sekarang', $dosen->idmDosen);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter $nis di URL, kembalikan ke halaman siswa
        else
        {
            redirect('master_dosen');
        }
    }
	
	public function hapus($idmDosen = NULL)
	{
		if(!empty ($idmDosen))
		{
			if($this->dosen->hapus($idmDosen))
			{
				$this->session->set_flashdata('pesan','Proses hapus data berhasil.');
				redirect('master_dosen');
			}
			else
			{
				$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
				redirect('master_dosen');
			}
		}
		else
		{
				$this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
				redirect('master_dosen');
		}
	}
	
	function nip_exist()
	{
		$nip_sekarang = $this->session->userdata('nip_sekarang');
		$nip_baru = $this->input->post('dos_nip');
		
		if ($nip_baru === $nip_sekarang)
		{
			return TRUE;
		}
		else
		{
			$query = $this->db->get_where('dos_nip',array ('idmDosen' => $nip_baru));
			if($query->num_rows() > 0)
			{
				$this->form_validation->set_message('nip_exist',"Dosen dengan NIP $nip_baru sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	function sandi_exist()
	{
		$sandi_sekarang = $this->session->userdata('sandi_sekarang');
		$sandi_baru = $this->input->post('dos_sandi');
		
		if($sandi_baru === $sandi_sekarang)
		{
			return TRUE;
		}
		else
		{
			$query = $this->db->get_where('dos_sandi', array('dos_sandi' => $sandi_baru));
			if($query->numb_rows() > 0)
			{
				$this->form_validation->set_message('sandi_exist',"Dosen dengan sandi $sandi_baru sudah terdaftar");
				return FALSE;
			}
			else
			{
				return TRUE;
			}
		}
	}
	
}

/*End of file master_dosen.php */
/* Location: ./application/controllers/master_dosen.php */