<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_v_nilaiseminar extends MY_Controller
{
    public $data = array(
        'breadcrumb'    => 'Verifikasi Nilai Seminar',
        'pesan'         => '',
        'pagination'    => '',
        'tabel_data'    => '',
        'main_view'     => 'koor_v_nilaiseminar/koor_v_nilaiseminar',
        'form_action'   => '',
        'form_value'    => '',
    );

	public function __construct()
	{	
		parent::__construct();		
		$this->load->model('model_koor_v_nilaiseminar', 'nilai', TRUE);
	}

	public function index($offset = 0)
    {
        $nilai = $this->nilai->cari_semua($offset);

        if ($nilai)
        {
            $tabel = $this->nilai->buat_tabel($nilai);
            $this->data['tabel_data'] = $tabel;

            // Paging
            // http://localhost/absensi2014/siswa/halaman/2
            $this->data['pagination'] = $this->nilai->paging(site_url('vnilaiseminar/halaman'));
        }
        else
        {
            $this->data['pesan'] = 'Semua data sudah di verifikasi';
        }
        $this->load->view('template_koor', $this->data);
    }

    public function edit($idtSemSid = NULL)
    {
        $this->data['breadcrumb']  = 'Seminar > Verifikasi Nilai';
        $this->data['main_view']   = 'koor_v_nilaiseminar/koor_v_nilaiseminar';
        $this->data['form_action'] = 'koor_v_nilaiseminar/edit/' . $idtSemSid;

        // pastikan parameter ada, mencegah error
        if( ! empty($idtSemSid))
        {
                   //update db
                   if($this->nilai->edit($idtSemSid))
                    {
                        $this->session->set_flashdata('pesan', 'Proses verifikasi berhasil.');
                        redirect('koor_v_nilaiseminar');
                    }
                    else
                    {
                        $this->session->set_flashdata('pesan', 'Ups! Entah mengapa verifikasi data gagal.');
                        redirect('koor_v_nilaiseminar');
                    }
        }
        else
        {
            redirect('koor_v_nilaiseminar');
        }
    }
}
/* End of file koor_v_nilaiseminar.php */
/* Location: ./application/controllers/koor_v_nilaiseminar.php */