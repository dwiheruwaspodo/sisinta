<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search_dataskripsi extends MY_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->database();
		$this->load->library(array('pagination','session'));
		$this->load->model('model_koor_dataskripsi','data', TRUE);
	}
	
	function cari()
	{
		$data['main_view'] = 'koor_dataskripsi/nama_hasil';
		$data['form_action'] = 'search_dataskripsi/cari';
		
		$page=$this->uri->segment(3);
      	$batas=10;
		if(!$page):
		$offset = 0;
		else:
		$offset = $page;
		endif;	
		
		$data['nama']="";
		$postkata = $this->input->post('cari');
		if(!empty($postkata))
		{
			$data['nama'] = $this->input->post('cari');
			$this->session->set_userdata('pencarian_nama', $data['nama']);
		} 
		else 
		{
			$data['nama'] = $this->session->userdata('pencarian_nama');
		}
		$data['nama_nama'] = $this->data->cari_nama($batas,$offset,$data['nama']);
		$tot_hal = $this->data->total($data['nama']);
		
		$config['base_url'] = base_url() . 'index.php/search_dataskripsi/cari/';
        	$config['total_rows'] = $tot_hal->num_rows();
        	$config['per_page'] = $batas;
			$config['uri_segment'] = 3;
	    	$config['first_link'] = '&#124;&lt; First';
			$config['last_link'] = 'Last &gt;&#124;';
			$config['next_link'] = 'Next &gt;';
			$config['prev_link'] = '&lt; Prev';
       		$this->pagination->initialize($config);
		$data["paginator"] = $this->pagination->create_links();
		
        $this->load->view('template_koor',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */