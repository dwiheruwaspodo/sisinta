<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Master_prodi extends MY_Controller {

    public $data = array(
                        'breadcrumb'    => 'Master Prodi',
                        'pesan'         => '',
                        'pagination'    => '',
                        'tabel_data'    => '',
                        'main_view'     => 'm_prodi/prodi',
                        'form_action'   => '',
                        'form_value'    => '',
                        );

    public function __construct()
	{
		parent::__construct();		
		$this->load->model('model_masterprodi', 'masprod', TRUE);
    }

	public function index()
	{
        // hapus data temporary proses update
        $this->session->unset_userdata('id_kelas_sekarang', '');
        $this->session->unset_userdata('kelas_sekarang', '');

        // Cari semua data kelas
        $masprod = $this->masprod->cari_semua();

        // data kelas ada, tampilkan
        if ($masprod)
        {
            // buat tabel
            $tabel = $this->masprod->buat_tabel($masprod);
            $this->data['tabel_data'] = $tabel;
            $this->load->view('template_adm', $this->data);
        }
        // data kelas tidak ada
        else
        {
            $this->data['pesan'] = 'Tidak ada data Prodi.';
            $this->load->view('template_adm', $this->data);
        }
	}

    public function tambah()
    {
        $this->data['breadcrumb']  = 'Prodi > Tambah';
        $this->data['main_view']   = 'm_prodi/prodi_form';
        $this->data['form_action'] = 'master_prodi/tambah';

        // submit
        if($this->input->post('submit'))
        {
            // validasi sukses
            if($this->masprod->validasi_tambah())
            {
                if($this->masprod->tambah())
                {
                    $this->session->set_flashdata('pesan', 'Proses tambah data berhasil.');
                    redirect('master_prodi');
                }
                else
                {
                    $this->data['pesan'] = 'Proses tambah data gagal.';
                    $this->load->view('template_adm', $this->data);
                }
            }
            // validasi gagal
            else
            {
                $this->load->view('template_adm', $this->data);
            }
        }
        // no submit
        else
        {
            $this->load->view('template_adm', $this->data);
        }
    }

    public function edit($idmProdi = NULL)
    {
        $this->data['breadcrumb']  = 'Prodi > Edit';
        $this->data['main_view']   = 'm_prodi/prodi_form';
        $this->data['form_action'] = 'master_prodi/edit/' . $idmProdi;

        // pastikan id_kelas ada
        if( ! empty($idmProdi))
        {
            // submit
            if($this->input->post('submit'))
            {
                // validasi berhasil
                if($this->masprod->validasi_edit() === TRUE)
                {
                    //update db
                    if($this->masprod->edit($idmProdi))
					{
						$this->session->set_flashdata('pesan', 'Proses update data berhasil.');
						redirect('master_prodi');
					}
					else
					{
						$this->session->set_flashdata('pesan', 'Proses update data gagal! Id digunakan pada informasi lain!');
						redirect('master_prodi');
					}
                }
                // validasi gagal
                else
                {
                    $this->load->view('template_adm', $this->data);
                }
            }
            // tidak disubmit, form pertama kali dimuat
            else
            {
                // ambil data dari database, $form_value sebagai nilai dafault form
                $prodi = $this->masprod->cari($idmProdi);
                foreach($prodi as $key => $value)
                {
                    $this->data['form_value'][$key] = $value;
                }

                // set temporary data for edit
                $this->session->set_userdata('id_kelas_sekarang', $prodi->idmProdi);
                $this->session->set_userdata('kelas_sekarang', $prodi->prodi_nama);

                $this->load->view('template_adm', $this->data);
            }
        }
        // tidak ada parameter id_kelas, kembalikan ke halaman kelas
        else
        {
            redirect('master_prodi');
        }
    }

    public function hapus($idmProdi = NULL)
    {
        // pastikan id_kelas yang akan dihapus
        if( ! empty($idmProdi))
        {
            if($this->masprod->hapus($idmProdi))
            {
                $this->session->set_flashdata('pesan', 'Proses hapus data berhasil.');
                redirect('master_prodi');
            }
            else
            {
                $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
                redirect('master_prodi');
            }
        }
        else
        {
            $this->session->set_flashdata('pesan','Proses hapus data gagal! Data terpilih digunakan untuk informasi lain!');
            redirect('master_prodi');
        }
    }

    // callback, apakah id_kelas sama? untuk proses edit
    function is_id_kelas_exist()
    {
        $id_kelas_sekarang 	= $this->session->userdata('id_kelas_sekarang');
        $id_kelas_baru		= $this->input->post('idmProdi');

        // jika id_kelas baru dan id_kelas yang sedang diedit sama biarkan
        // artinya id_kelas tidak diganti
        if ($id_kelas_baru === $id_kelas_sekarang)
        {
            return TRUE;
        }
        // jika id_kelas yang sedang diupdate (di session) dan yang baru (dari form) tidak sama,
        // artinya id_kelas mau diganti
        // cek di database apakah id_kelas sudah terpakai?
        else
        {
            // cek database untuk id_kelas yang sama
            $query = $this->db->get_where('mprodi', array('idmProdi' => $id_kelas_baru));

            // id_kelas sudah dipakai
            if($query->num_rows() > 0)
            {
                $this->form_validation->set_message('is_id_kelas_exist',
                                                    "Kelas dengan kode $id_kelas_baru sudah terdaftar");
                return FALSE;
            }
            // id_kelas belum dipakai, OK
            else
            {
                return TRUE;
            }
        }
    }

    // callback, apakah nama kelas sama? untuk proses edit
    // penjelasan kurang lebih sama dengan is_id_kelas_exist
    function is_kelas_exist()
    {
        $kelas_sekarang 	= $this->session->userdata('kelas_sekarang');
        $prodi_baru		= $this->input->post('prodi_nama');

        if ($prodi_baru === $kelas_sekarang)
        {
            return TRUE;
        }
        else
        {
            // cek database untuk nama kelas yang sama
            $query = $this->db->get_where('mprodi', array('prodi_nama' => $prodi_baru));
            if($query->num_rows() > 0)
            {
                $this->form_validation->set_message('is_kelas_exist',
                                                    "Kelas dengan nama $prodi_baru sudah terdaftar");
                return FALSE;
            }
            else
            {
                return TRUE;
            }
        }
    }
}
/* End of file master_prodi.php */
/* Location: ./application/controllers/master_prodi.php */