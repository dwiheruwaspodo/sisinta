<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Koor_lihatruangkhusus extends MY_Controller
{
	public $data = array(
		'breadcrumb'	=> 'Lihat Ruang Khusus',
		'pagination'	=> '',
		'tabel_data'	=> '',
		'main_view'		=> 'koor_ruangkhusus/ruang',
		'pesan'			=> '',
	
	);
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_admsediaruangkhusus', 'tersedia', TRUE);
		$this->load->model('model_koor_lihatruangkhusus','ruangk',TRUE);
	}

	public function index($offset = 0)
	{
		$ruang = $this->tersedia->cari_semua($offset);
		if ($ruang)
		{
			$tabel = $this->ruangk->buat_tabel($ruang);
			$this->data['tabel_data'] = $tabel;
			$this->data['pagination'] = $this->tersedia->paging(site_url('ruangkhusus/halaman'));
		}
		else
		{
			$this->data['pesan'] = 'Tidak ada data ruangan khusus yang tersedia';
		}
		$this->load->view('template_koor',$this->data);
		
	}
}